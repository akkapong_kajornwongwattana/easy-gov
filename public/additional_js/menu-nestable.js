var UINestable = function () {
	"use strict";
    //function to initiate jquery.nestable
    var updateOutput = function (e) {

        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));
            //, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };
    var runNestable = function () {
        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1
        }).on('change', updateOutput);
        
        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $('#nestable-menu').on('click', function (e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
        
    };
    return {
        //main function to initiate template pages
        init: function () {
            runNestable();
        },
        updateData: function(e) {
            updateOutput(e);
        }
    };
}();

var editModalMenuName = "modal-edit-menu-name";
var editModalMenuUrl = "modal-edit-menu-url";

function addEditModalBody(datas)
{
    var menuName = '<label class="control-label font-bold"> Menu Name </span></label><br /><input type="text" class="form-control" id="'+editModalMenuName+'" value="'+datas['name']+'">';
    var menuUrl = '<label class="control-label font-bold"> Menu Url </span></label><br /><input type="text" class="form-control" id="'+editModalMenuUrl+'" value="'+datas['url']+'">';
    $modalBody = $('#editModal').find('.modal-body');
    $modalBody.html(menuName+'<br/>'+menuUrl);
}

function getData(index)
{
    var menuItem = $("#menu-"+index);
    return {
        "name" : menuItem.attr('data-menu'),
        "url"  : menuItem.attr('data-url')
    };
}

function getDataFromModal()
{
    var name = $('#'+editModalMenuName).val();
    var url  = $('#'+editModalMenuUrl).val();

    return {
        "name" : name,
        "url"  : url
    };
}

function updateDataToMenu(index, datas)
{
    $("#menu-"+index).attr('data-menu', datas["name"]);
    $("#menu-"+index).attr('data-url', datas["url"]);
    $("#menu-"+index).data('menu', datas["name"]);
    $("#menu-"+index).data('url', datas["url"]);
    //update text in div
    $("#menu-"+index).find('.dd-handle').text(datas["name"]);
}

function editData(index)
{
    addEditModalBody(getData(index));
    $('#editModal').modal('show');

    $('#editModal').find("#okay").click(function() {
        //get data
        var datas = getDataFromModal();
        //update data
        updateDataToMenu(index, datas);

        //hide modal
        $('#editModal').modal('hide');
        //unbind ok event
        $('#editModal').find("#okay").unbind('click');

        //update
        $("#nestable").change();
        
    });
}

function getIndex(item)
{
    return item.attr('id').replace("menu-", "");
}

function unsetParentItem(parent)
{
    parent.removeClass('dd-collapsed');
    parent.children('[data-action]').remove();
    parent.children('ol').remove();

    var index = getIndex(parent);

    //*** Additional ***//
    parent.prepend($('<button onclick="deleteData('+index+')" data-action="delete" data-toggle="modal">Delete</button>'));
    parent.prepend($('<button onclick="editData('+index+')" data-action="edit" data-toggle="modal">Edit</button>'));
}

function checkNoSibing(item)
{
    var parent = item.parent('.dd-list')
    var nodes  = parent.find('.dd-item');
    if (nodes.length > 1) {
        return true;
    }
    return false;
}

function addDeleteModalBody(index)
{
    var menuName = $("#menu-"+index).attr('data-menu');    
    $modalBody   = $('#deleteModal').find('.modal-body');
    $modalBody.html('<p>Would you like to delete '+menuName+' ?</p>');
}

function deleteData(index)
{
    //add content
    addDeleteModalBody(index);
    //show modal
    $('#deleteModal').modal('show');
    
    $('#deleteModal').find("#okay").click(function() {
        var currentItem = $("#menu-"+index);
        //check sibling
        var haveSibling = checkNoSibing(currentItem);
        var parent      = currentItem.parents('.dd-list').parents('.dd-item');
        $("#menu-"+index).remove();
        $('#deleteModal').modal('hide');
        //unbind ok event        
        $('#deleteModal').find("#okay").unbind('click');

        if (!haveSibling) {
            unsetParentItem(parent);
        }

        //update
        $("#nestable").change();
    });
}

function addNewMenu()
{
    var index       = $("#index").val();
    var template    = '<li id="menu-'+index+'" class="dd-item" data-id="" data-menu="$[menu]" data-url="$[url]"><button onclick="editData('+index+')" data-action="edit" data-toggle="modal">Edit</button><button onclick="deleteData('+index+')" data-action="delete" data-toggle="modal">Delete</button><div class="dd-handle">New Menu</div></li>';
    var replaceMenu = "$[menu]";
    var replaceUrl  = "$[url]";

    //define variable datas
    var datas = {
        "name" : "New Menu",
        "url"  : ""
    };

    //replace
    var newItem = "";
    
    newItem     = template.replace(replaceMenu, datas["name"]);
    newItem     = newItem.replace(replaceUrl, datas["url"]);
    
    //add in nestable
    $("#nestable > ol").append(newItem);
    


    addEditModalBody(datas);
    $('#editModal').modal('show');

    $('#editModal').find("#okay").click(function() {
        //get data
        var datas = getDataFromModal();
        //update data
        updateDataToMenu(index, datas);

        //hide modal
        $('#editModal').modal('hide');
        $('#editModal').find("#okay").unbind('click');

        //update
        $("#nestable").change();
       
    });

    //update index
    $("#index").val(parseInt(index)+1);
}