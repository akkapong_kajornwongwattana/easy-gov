<?php namespace App\Services;

class MyServices {

    protected $OPERATOR  = "operator";
    protected $VALUE     = "value";
    protected $DELIMITER = ".";

    //for create condition list
    //$keys is List of field name you want to add condition
    //$operators is List of operator
    //$param Sample
    //$param = [
    //    'key' => 'value'
    //]
    public function createCondition($keys, $operators, $param)
    {
        $conditions = array();
        $index = 0;
        foreach ($keys as $key) {
            $conditions[$key]                  = [];
            //add Detail
            $conditions[$key][$this->OPERATOR] = $operators[$index];
            $conditions[$key][$this->VALUE]    = $param[$key];
            //plus index
            $index++;
        }
        return $conditions;
    }

    //methos for check duplication
    public function checkDuplicate($collectionObj, $keys, $operators, $param)
    {
        $isDuplicate = [false];
        $conditions  = $this->createCondition($keys, $operators, $param);
        //add condition to module
        $collection  = $this->addConditiontoCollection($conditions, $collectionObj);
        $datas       = $collection->get()->toArray();

        if ($datas) {
            $isDuplicate = [true, $datas[0]];
        }
        return $isDuplicate;
    }

    //for add condition to collection
    //sample condition
    //$condition = [
    //  "name" => [
    //      "operator" => "LIKE",
    //      "value"    => "John"
    //   ]
    //];
    public function addConditiontoCollection($conditions, $collectionObj)
    {
        if ($conditions) {
            foreach ($conditions as $key => $obj) {
                $operator = $obj[$this->OPERATOR];
                $value    = $obj[$this->VALUE];

                if (!empty($operator)) {
                    //add where
                    $collectionObj = $collectionObj->where($key, $operator, $value);

                }
            }
        }
        return $collectionObj;
    }

    public function sortByKey($collectionObj, $key, $type='desc')
    {
        $collectionObj = $collectionObj->orderBy($key , $type);
        return $collectionObj;
    }

    public function getValueFormObj($keys, $obj) {
        $key  = $keys[0];

        //keep in $obj
        if (isset($obj[$key])) {
            $obj  = $obj[$key];

            if (count($keys) > 1) {
                //cut first
                $keys = array_slice($keys, 1);
                return $this->getValueFormObj($keys, $obj);
            }
        } else {
            return "";
        }

        return $obj;

    }

    public function getListDataFromArr($datas, $key)
    {
        $output = [];
        foreach ($datas as $data) {
            $keyList  = explode($this->DELIMITER, $key);
            $output[] = $this->getValueFormObj($keyList, $data);
        }

        return $output;
    }

    public static function convertDateTime($dateStr, $dateFormat="d-m-Y")
    {
      if (empty($dateStr) || $dateStr == "0000-00-00") {
        return "";
      } else {
        return date($dateFormat, strtotime($dateStr));
      }
    }


}
