<?php namespace App\Http\Controllers\Html\CRUD;

use App\Http\Controllers\Html\CRUDHtmlController;
use App;

class SuperAdminHtmlController extends CRUDHtmlController {
    
    protected $userRepo;

    public function __construct()
    {
        parent::__construct();

        $this->userRepo = App::make('App\Repositories\UserRepository');
    }

    protected function canAccess($module="")
    {
        return $this->checkSuperAdmin();
    }

    private function checkSuperAdmin()
    {
        return $this->userRepo->checkSuperAdmin();
    }
}