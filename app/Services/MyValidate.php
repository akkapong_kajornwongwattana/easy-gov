<?php

namespace App\Services;

use App;
use Validator;
use Config;
use Request;
// use EggLog;

class MyValidate
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    private function manageError($validate)
    {
        //define validate output
        $result  = array();

        if ( $validate->fails() ) {
            foreach ($validate->messages()->all() as $message) {
                $result = array('error' => $message);
                break;
            }
        }
        return $result;

    }

    private function setDefault($inputs, $defualts)
    {
        foreach ($defualts as $key => $value) {
            if (empty($inputs[$key])) {
                $inputs[$key] = $value;
            }
        }
        return $inputs;
    }

    public function validator($inputs, $rules, $defaults)
    {
        //validate input
        $validate = Validator::make($inputs, $rules);
        //get message if error
        $validateRes = $this->manageError($validate);

        if (isset($validateRes["error"])) {
            return $validateRes;
        }

        return $this->setDefault($inputs, $defaults);
    }

    public function validateApi($rules, $default=array(), $input=array())
    {
        $input    = empty($input) ? \Input::all() : $input;
        $return   = array();
        $validate = $this->validate($input, $rules);

        if ( ! empty($validate['error'])) {
            return array('error' => $validate['error']);
        }

        foreach (array_keys($default) as $s_value) {
            @$s_input_data = $input[$s_value];

            if ($s_input_data != '') {
                $return[$s_value] = $s_input_data;
            } else {
                $return[$s_value] = @$default[$s_value];
            }
        }

        return  array_merge($input, $return);
    }

    public function validate($input, $rules)
    {
        // $validate = parent::make($input, $rules);
        $validate = Validator::make($input, $rules);

        if ($validate->fails()) {

            foreach ($validate->messages()->all() as $message) {
                return array('error' => $message);
            }
        } else {
            return array();
        }
    }

    public function validateError($msgError, $code = null, $errorCode = null)
    {
        if ( ! empty($code)) {
            $statusCode = Config::get('status.code'.$code);
            $statusText = Config::get('status.text'.$code);
        } else {
            $statusCode = Config::get('status.code290');
            $statusText = Config::get('status.text290');
            $errorCode = 1000;
        }

        $output = array(
            'status' => array(
                'code' => $statusCode,
                'text' => $statusText,
            ),
            'error' => array(
                'error_code' => (int) $errorCode,
                'msg'        => $msgError
            ),
        );

        // $this->logValidateError($output, $statusText, $statusCode);

        return $output;
    }

    public function logValidateError($output, $statusText, $statusCode)
    {
        $transaction_id = MyHelpers::getParams('transaction_id');
        $action         = MyHelpers::getAction();
        $worning        = Config::get('config.SYSLOG.WARNING.KEYWORD');

        $log_params = array(
            'transaction_id' => $transaction_id,
            'start'          => MyHelpers::getParams('time_start'),
            'action'         => array('route_controller' => $action['route_controller'], 'route_method' => $action['route_method']),
            'info'           => $worning,
            'config'         => array('ENVIRONMENT' => env('APP_ENV')),
            'description'    => 'Api out',
            'requestFullUrl' => Request::fullUrl(),
            'requestInput'   => Request::input(),
            'return_data'    => $output,
            'return_status'  => $statusText,
            'return_code'    => $statusCode,
        );
        EggLog::logApiOut($log_params);
    }

    /**
     * Validate bussiness error.
     * @param  string $field
     * @return array
     */
    public function validateBussinessError($field)
    {
        $errorMsg = Config::get('errorMsg');

        return $this->validateError($errorMsg[$field]['msg'], $errorMsg[$field]['code'], $errorMsg[$field]['error_code']);
    }

    public function findValidateError(array $validateData)
    {
        foreach ($validateData as $value) {

            if (isset($value['error'])) {
                return ['error' => $value['error']];
            }
        }

        return [];
    }
}