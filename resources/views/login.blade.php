<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title>CMS IOTALK ADMIN</title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/animate.css/animate.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/skins/all.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/styles-responsive.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/skins/all.css') }}">
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body class="login">
        <div class="row">
            <div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
                
                <!-- start: LOGIN BOX -->
                <div class="box-login">
                    <!-- Show error message -->
                    @if (isset($errormsg)) 
                        <div class="alert alert-danger">
                            <button data-dismiss="alert" class="close">
                                &times;
                            </button>
                            <strong>ERROR!</strong> {{ $errormsg }}
                        </div>
                    @endif 
                    <!-- End show error message -->
                    <h3>Sign in to your account</h3>
                    <p>
                        Please enter your username and password to log in.
                    </p>
                    <form id="login-form" class="form-login" action="{{ URL::to('login/login') }}" method="POST">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                        <div class="errorHandler alert alert-danger no-display">
                            <i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
                        </div>
                        <fieldset>
                            <div class="form-group">
                                <span class="input-icon">
                                    <input type="text" placeholder="Username" class="form-control" id="username" name="username">
                                    <i class="fa fa-user"></i> </span>
                            </div>
                            <div class="form-group form-actions">
                                <span class="input-icon">
                                    <input type="password" class="form-control password" id="password" name="password" placeholder="Password">
                                    <i class="fa fa-lock"></i>
                                    <!-- <a class="forgot" href="#">
                                        I forgot my password
                                    </a>  -->
                                </span>
                            </div>
                            <div class="form-actions">
                                <button id="login-btn" type="submit" class="btn btn-green pull-right">
                                    Login <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                            
                        </fieldset>
                    </form>
                </div>
                <!-- end: LOGIN BOX -->
                <!-- start: FORGOT BOX -->
                <div class="box-forgot">
                    <h3>Forget Password?</h3>
                    <p>
                        Enter your e-mail address below to reset your password.
                    </p>
                    <form class="form-forgot">
                        <div class="errorHandler alert alert-danger no-display">
                            <i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
                        </div>
                        <fieldset>
                            <div class="form-group">
                                <span class="input-icon">
                                    <input type="text" class="form-control" id="forget-username" name="forget-username" placeholder="Username">
                                    <i class="fa fa-envelope"></i> </span>
                            </div>
                            <div class="form-actions">
                                <a class="btn btn-light-grey go-back">
                                    <i class="fa fa-chevron-circle-left"></i> Log-In
                                </a>
                                <button id="forget-btn" type="submit" class="btn btn-green pull-right">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <!-- end: FORGOT BOX -->
                
            </div>
        </div>
        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script>
        <script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
        <![endif]-->
        <!--[if gte IE 9]><!-->
        <script src="{{ URL::asset('assets/plugins/jQuery/jquery-2.1.1.min.js') }}"></script>
        <!--<![endif]-->
        <script src="{{ URL::asset('assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/iCheck/jquery.icheck.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery.transit/jquery.transit.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/TouchSwipe/jquery.touchSwipe.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/main.js') }}"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="{{ URL::asset('assets/plugins/jquery-validation/dists/jquery.validate.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/login.js') }}"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script>
            
            jQuery(document).ready(function() {
                Main.init();
                Login.init();

                //clear cookie
                document.cookie = "name=; expires=Thu, 01 Jan 1970 00:00:00 UTC"; 
                
                //login button
                $('#login-btn').click(function() {
                    //submit
                    $("#login-form").submit();
                });
            });
        </script>
    </body>
    <!-- end: BODY -->
</html>