
@extends('listtemplate')


@section('list_name_page')
Module
@stop

@section('list_link_header')
                                    <li>
                                        <a href="{{ URL::to('/') }}">
                                            Dashboard
                                        </a>
                                    </li>
                                    <li class="active">
                                        List
                                    </li>
@stop

@section('list_content')
                                        <div class="row">
                                            <div class="col-md-12 space20">
                                                <!-- Add Button-->
                                                <div>
                                                    <a id="add-admin-btn" href="{{ URL::to('module/create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> Add Module</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover" id="sample_2">
                                                <thead>
                                                    <tr>
                                                        <th>Module name</th>
                                                        <th>Description</th>
                                                        <th>Create</th>
                                                        <th>Read</th>
                                                        <th>Update</th>
                                                        <th>Delete</th>
                                                        <th> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if ($modules)
                                                    @foreach($modules as $data)
                                                        @if (isset($data['id']))
                                                            <tr id="list-{{$data['id']}}">
                                                                <td>{{ $data['module_name'] }}</td>
                                                                <td>{{ $data['description'] }}</td>
                                                                <td class="center">
                                                                    <div class="checkbox-table">
                                                                        <label>
                                                                            <input type="checkbox" class="flat-grey foocheck" disabled="true"
                                                                            @if ($data['has_create'])
                                                                                checked="checked"
                                                                            @endif
                                                                            >
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td class="center">
                                                                    <div class="checkbox-table">
                                                                        <label>
                                                                            <input type="checkbox" class="flat-grey foocheck" disabled="true"
                                                                            @if ($data['has_read'])
                                                                                checked="checked"
                                                                            @endif
                                                                            >
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td class="center">
                                                                    <div class="checkbox-table">
                                                                        <label>
                                                                            <input type="checkbox" class="flat-grey foocheck" disabled="true"
                                                                            @if ($data['has_update'])
                                                                                checked="checked"
                                                                            @endif
                                                                            >
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td class="center">
                                                                    <div class="checkbox-table">
                                                                        <label>
                                                                            <input type="checkbox" class="flat-grey foocheck" disabled="true"
                                                                            @if ($data['has_delete'])
                                                                                checked="checked"
                                                                            @endif
                                                                            >
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                                                                        <a class="btn btn-primary" href="{{ URL::to('module/update/'.$data['id']) }}">Edit</a>
                                                                        <a onclick="deleteData({{ $data['id'] }});" class="btn btn-primary btn-red tooltips deleteButton" id="delete-button-{{ $data['id'] }}" data-id="{{ $data['id'] }}" data-toggle="modal"  data-original-title="Remove">Delete</a>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
@stop


@section('list_js_script')
    function deleteData(id) {
        $('#smallModal').modal('show');

        $('#smallModal').find("#okay").click(function() {
            if(id!=0){
                
                $.ajax({
                    type:'DELETE',
                    url: "{{ URL::to('module/delete') }}",
                    data:{ _token: '{{ csrf_token() }}', 'id':id },
                    async: false,
                    dataType: "json",
                    success:function(result){
                        $('.modal-footer').html('<button type="button" data-dismiss="modal" class="btn green" id="okay">OK</button>');

                        if(result.status==290){
                            $('.modal-body').html(result.message);
                        } else {
                            $('.modal-body').html("Delete Complete!");
                            $('#sample_2').find('#list-'+id).remove();
                            $('#smallModal').modal('hide');
                        }
                    },
                    error: function(){

                    }
                });
            }

        });

        $('#smallModal').on('hidden.bs.modal', function () {

            $('.modal-body').html('Would you like to delete?');
            $('.modal-footer').html('<button data-dismiss="modal" class="btn btn-default" type="button">Close</button><button class="btn btn-primary" type="button" id="okay">OK</button>');
        });
    }

@stop

@section('list_jquery')

@stop



