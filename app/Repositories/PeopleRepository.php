<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\People;

use Config;
use App;

class PeopleRepository
{
    protected $people;
    protected $messages;
    protected $myService;
    protected $userRepo;
    protected $cdnServices;

    public function __construct(People $people)
    {
        // parent::__construct();

        $this->people    = $people;
        $this->messages    = Config::get('message');

        $this->myService   = App::make('App\Services\MyServices');
    }

    public function getPeople($keys = [], $operators = [], $params = [])
    {
        //create param for add condition
        $conditions       = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $peopleCollection = $this->myService->addConditiontoCollection($conditions, $this->people);
        //get data
        $output           = $peopleCollection->get()->toArray();

        return $output;
    }

    public function getPeopleById($id)
    {
        return $this->people->find($id)->toArray();
    }


    //check duplicate module
    private function duplicatePeople($param)
    {
        //define parameters
        $keys               = ["card_no"];
        $operators          = ["="];
        
        $isDuplicate        = $this->myService->checkDuplicate($this->people, $keys, $operators, $param);
        
        return $isDuplicate;
    }

    private function checkDupForUpdate($param)
    {
        $isDuplicate = false;
        //check duplicate
        $duplicates = $this->duplicatePeople($param);
        if ($duplicates[0]) {
            $tmp = $duplicates[1];
            //check self
            if ((int)$tmp["id"] != (int)$param["id"]) {
                $isDuplicate = true;
            }
        }
        return $isDuplicate;
    }

    private function encryptPassword($password)
    {
        return md5($password);
    }

    //method for add not required field
    private function addNotRequiredField($obj, $params, $keys)
    {
        foreach ($keys as $key) {
            if (isset($params[$key])) {
                $obj->{$key} = $params[$key];
            }
        }
        return $obj;
    }

    public function addPeople($params)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicatePeople($params);

        if (!$isDups[0]) {

            // add data to database
            $people              = new People;
            // add data to collection object
            $people->firstname   = $params['firstname'];
            $people->lastname    = $params['lastname'];
            $people->card_no     = $params['card_no'];
            $people->change_name = (isset($params['change_name']))?$params['change_name']: false;
            $people->username    = $params['username'];
            $people->password    = $this->encryptPassword($params['password']);

            $people = $this->addNotRequiredField($people, $params, ["status", "birthday", "tel", "email"]);
            
            if (!$people->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            }

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;

    }

    public function updatePeople($params)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];

        if (!empty($params["id"])) {
            //get record
            $people      = $this->people->find($params['id']);

            //check password
            $isMatch      = $this->checkOldPassword($people["password"], $params["oldpassword"]);

            if (!$isMatch) {
                $result["success"] = false;
                $result["message"] = $this->messages["people"]["wrong_oldpassword"];
                return $result;
            }
            
            //check duplicate
            $isDuplicate = $this->checkDupForUpdate($params);

            if (!$isDuplicate){

                //update data
                $people->firstname   = $params['firstname'];
                $people->lastname    = $params['lastname'];
                $people->card_no     = $params['card_no'];
                $people->change_name = (isset($params['change_name']))?$params['change_name']: false;
        

                $people = $this->addNotRequiredField($people, $params, ["status", "birthday", "tel", "email", "username"]);
                
                
                //encrype password before save
                if (isset($params['password'])) {
                    $people->password  = $this->encryptPassword($params['password']);
                }
                
                if (!$people->save()) {
                    $result["success"] = false;
                    $result["message"] = $this->messages["update_error"];
                }

                
            } else {
                $result["success"] = false;
                $result["message"] = $this->messages["duplicate"];
            }
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
        
    }

    public function deletePeople($id)
    {
        $output = [
            "status"  => 200,
            "message" => ""
        ];

        $people = $this->people->find($id);
        
        //delete app
        $result = $people->delete();
        if (!$result) {
            
            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];

        } 
        
        return $output;

    }

    //method for check old password before change password
    private function checkOldPassword($password, $oldpassword)
    {
        //define result
        $isMatch     = true;

        $oldpassword = $this->encryptPassword($oldpassword);

        if ($password != $oldpassword) {
            $isMatch = false;
        }
        return $isMatch;
    }

    //Method for change people password
    public function changePeoplePassword($params)
    {
        //define result
        $result = [
            "success"  => true,
            "message" => ""
        ];

        //get people
        $people = $this->getPeopleById($params["people_id"]);

        if ($people) {
            
            //check password with old password input
            $isMatch = $this->checkOldPassword($people["password"], $params["old_password"]);

            if ($isMatch) {
                //update new password
                //get record
                $people           = $this->people->find($people['id']);
                $people->password = $this->encryptPassword($params["new_password"]);

                if (!$people->save()) {
                    $result = [
                        "success"  => false,
                        "message" => $this->messages["update_error"]
                    ];
                }

            } else {
                $result = [
                    "success"  => false,
                    "message" => $this->messages["people"]["wrong_oldpassword"]
                ];
            }
        } else {
            $result = [
                "success"  => false,
                "message" => $this->messages["people"]["no_data"]
            ];
        }

        return $result;
    }

    //Method for check login
    public function checkLogin($params)
    {
        //define output
        $output = [false, Config::get('message.login_fail')];
        //define parameter
        $paramCondition = [
            "username" => $params["username"],
            "password" => $this->encryptPassword($params["password"])
        ];

        //create condition
        $keys       = ["username", "password"];
        $operators  = ["=", "="];
        $conditions = $this->myService->createCondition($keys, $operators, $paramCondition);
        //add condition to user
        $people     = $this->myService->addConditiontoCollection($conditions, $this->people);
        $peopleData = $people->get()->toArray();

        if ($peopleData){
            $output = [true, $peopleData[0]];
        }
        return $output;
    }

    //Method for get people profile
    public function getProfile($param)
    {
        //define result
        $result = [
            "success"  => true,
            "message" => ""
        ];

        $people = $this->getPeopleById($param["people_id"]);
        
        if (!empty($people)) {
            $result["profile"] = $people;
        } else {
            $result = [
                "success"  => false,
                "message" => $this->messages["people"]["no_data"]
            ];
        }
        return $result;
    }
}