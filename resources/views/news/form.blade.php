
@extends('formtemplate')

@section('form_name_page')
News
@stop

@section('form_link_header')
                                    <li>
                                        <a href="{{ URL::to('news/list') }}">
                                            List
                                        </a>
                                    </li>
                                    <li class="active">
                                        News
                                    </li>
@stop

@section('form_content')
                                        <h1><i class="fa fa-pencil-square"></i> NEWS</h1>
                                        <hr>
                                        <form id="form" action="{{ URL::to('news/news') }}" method="POST" role="form" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" value="{{$data['id'] or ''}}" />
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="errorHandler alert alert-danger no-display">
                                                        <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                    </div>
                                                    <div class="successHandler alert alert-success no-display">
                                                        <i class="fa fa-ok"></i> Your form validation is successful!
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Application Name <span class="symbol required"></span>
                                                        </label>
                                                        <select id="app_id" name="app_id" class="form-control">
                                                        @if (isset($data['apps']))
                                                            @foreach ($data['apps'] as $app)
                                                            <option value="{{ $app['id'] }}"
                                                            @if ((isset($data['app_id']))&&($data['app_id'] == $app['id']))
                                                                selected
                                                            @endif
                                                            >{{$app['name']}}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="">Select Application..</option>
                                                        @endif
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Title <span class="symbol required"></span>
                                                        </label>
                                                        <input type="text" placeholder="Insert news title" class="form-control" id="title" name="title" value="{{$data['title'] or ''}}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Description 
                                                        </label>
                                                        <textarea id="description" name="description" class="ckeditor form-control" cols="10" rows="10">{{ $data['description'] or ''}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <span class="symbol required"></span>Required Fields
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">

                                                </div>
                                                 <div class="col-md-4">
                                                    <button id="save-btn" class="btn btn-primary btn-block" type="submit">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </form>


@stop

@section('js_req_for_form')
<script src="{{ URL::asset('additional_js/news-validation.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
@stop

@section('form_jquery')
        FormValidator.init();
@stop

@section('form_js_script')

@stop