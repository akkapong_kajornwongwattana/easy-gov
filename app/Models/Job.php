<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobs';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['job_type_id', 'app_id', 'position_detail'];

    //=== Set relation ===//
    //many to many
    public function documenttypes()
    {
        return $this->belongsToMany('App\Models\Documenttype');
    }

    public function job_type()
    {
        return $this->belongsTo('App\Models\JobType', 'job_type_id', 'id');
    }

    public function app()
    {
        return $this->belongsTo('App\Models\AppModel');
    }
    
}
