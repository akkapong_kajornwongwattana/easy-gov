<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Module;


class UserAcl extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table   = 'user_acl';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['module_id', 'user_id', 'can_create', 'can_delete', 'can_read', 'can_update'];


    // /**
    //  * Relation between user_acl and user.
    //  *
    //  * @return Collection
    //  */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    // /**
    //  * Relation between user_acl and module.
    //  *
    //  * @return Collection
    //  */
    public function module()
    {
        return $this->hasOne('App\Models\Module', 'id', 'module_id');
    }

    

}
