<?php namespace App\Http\Controllers\Html\CRUD\SuperAdmin;

use App\Http\Controllers\Html\CRUD\SuperAdminHtmlController;
use App\Repositories\MenuRepository;
use Config;
use App;
use Input;

class MenuController extends SuperAdminHtmlController{

    protected $menuRepo;

    //private $userAcl;
    private $myValidate;
    private $myResponse;
    private $canAccessMethod;

    private $module      = "MenuController";
    private $moduleName  = "menu";
    private $mapping = [
        "list"   => "menu",
        "create" => "menu",
        "update" => "menu",
        "view"   => "menu"
    ];


    public function __construct(MenuRepository $menuRepo)
    {
        parent::__construct();

        $this->myValidate = App::make('App\Services\MyValidate');
        $this->myResponse = App::make('App\Services\MyResponse');

        //check permission
        $this->canAccessMethod    = $this->canAccess();

        $this->menuRepo   = $menuRepo;
    }

    //------- Interface method --------//
    public function getList()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        //get user list
        //seletct all
        $menus = $this->menuRepo->getMenu();
        // dd($menus);
        return $this->getOutput($this->mapping["list"], ["menuDatas" => $menus, "module_name" => $this->module]);
    }

    public function getCreate()
    {
        // //get application
        // $apps = $this->appRepo->getApp([], [], []);

        // //can use this method
        // return $this->getOutput($this->mapping["create"], ["data" => ["apps" => $apps], "module_name" => $this->module]);

    }

    public function postMenu()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $inputs = Input::all();

        $menuDatas = json_decode($inputs["menuDatas"], true);

        $results = $this->menuRepo->updateMenu($menuDatas);

        if (!$results["success"]) {
            //FAIL
            //seletct all
            $menus = $this->menuRepo->getMenu();
            //change view 
            return $this->getOutputWithErrormsg($this->mapping["list"], ["menuDatas" => $menus, "module_name" => $this->module], $results["message"]);
        } else {
            //SUCCESS
            return redirect("menu/list");
        }
    }

    public function getView($id)
    {
        // //get Module
        // $users = $this->userRepo->getUser(["id"], ["="], ["id" => $id]);
        // if (!empty($users)) {
        //     $users = $users[0];
        // }
        // //get application
        // $apps = $this->appRepo->getApp([], [], []);
        // //add apps to users
        // $users["apps"] = $apps;
        // //return to view
        // return $this->getOutput($this->mapping["view"], ["data" => $users, "module_name" => $this->module]);
    }
    
    public function getUpdate($id)
    {
        // //update Module
        // $users = $this->userRepo->getUser(["id"], ["="], ["id" => $id]);
        // if (!empty($users)) {
        //     $users = $users[0];
        // }
        // //get application
        // $apps = $this->appRepo->getApp([], [], []);
        // //add apps to users
        // $users["apps"] = $apps;
        // //return to view
        // return $this->getOutput($this->mapping["update"], ["data" => $users, "module_name" => $this->module]);
    }

    public function deleteDelete()
    {
        // $inputs = Input::all();
        // $id = $inputs["id"];

        // $result = $this->userRepo->deleteUser($id);
        // //return to view
        // return $this->myResponse->getOutput('json', $result);
    }
    //------ End Interface method ------//

}
