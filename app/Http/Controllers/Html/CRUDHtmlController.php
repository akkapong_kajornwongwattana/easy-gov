<?php namespace App\Http\Controllers\Html;

use App\Interfaces\CRUDInterface;
use App\Http\Controllers\HtmlController;

class CRUDHtmlController extends HtmlController implements CRUDInterface {
    public function __construct()
    {
        parent::__construct();
    }

    protected function canAccess($module) {
        return true;
    }


    public function getList()     { die('this method needs to be defined'); }
    public function getCreate()    { die('this method needs to be defined'); }
    public function postStore()    { die('this method needs to be defined'); }
    public function getView($id)      { die('this method needs to be defined'); }
    public function getUpdate($id)    { die('this method needs to be defined'); }
    public function putUpdate()    { die('this method needs to be defined'); }
    public function deleteDelete() { die('this method needs to be defined'); }
}