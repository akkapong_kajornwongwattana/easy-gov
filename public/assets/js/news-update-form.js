var NewsUpdateForm = function () {
	"use strict";

    // function to initiate Validation Sample 2
    var runValidator2 = function () {
        var form2 = $('#form2');
        var errorHandler2 = $('.errorHandler', form2);
        var successHandler2 = $('.successHandler', form2);
        form2.validate({
            errorElement: "span", // contain the error msg in a small tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.hasClass("ckeditor") || element.attr("type") == "file") {
                    error.appendTo($(element).closest('.form-group'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: "",
            rules: {
                name_en: {
                    minlength: 2,
                    required: true
                },
                name_th: {
                    minlength: 2,
                    required: true
                },
                status: {
                    required: true
                },
                description_en: {
                    minlength: 2,
                    required: true
                },
                description_th: {
                    minlength: 2,
                    required: true
                }
            },
            messages: {
                firstname: "Please specify your first name",
                lastname: "Please specify your last name",
                email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                },
                services: {
                    minlength: jQuery.format("Please select  at least {0} types of Service")
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler2.hide();
                errorHandler2.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (form) {

                var chk_error = true;
                var thumb = $("#thumb").val();
                var image = $("#image").val();

                if(image){

                    var ext_image = image.split('.').pop().toLowerCase();

                    if($.inArray(ext_image, ['jpg','jpeg','png']) == -1) {

                        successHandler2.hide();
                        errorHandler2.show();
                        $('#image').closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                        $('.errorHandler').append('<br />Image can not support is type file.');
                        chk_error = false;

                    }
                }

                if(thumb){

                    var ext       = thumb.split('.').pop().toLowerCase();

                    if($.inArray(ext, ['jpg','jpeg','png']) == -1) {
                        successHandler2.hide();
                        errorHandler2.show();
                        $('#thumb').closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                        $('.errorHandler').append('<br />Thumb can not support is type file.');
                        chk_error = false;

                    }
                }

                if(chk_error) {

                    successHandler2.show();
                    errorHandler2.hide();

                    form.submit();
                    return true;
                }

                // submit form

            }
        });

        $('#form2 .summernote').summernote({
            height: 300
        });

    };
    return {
        //main function to initiate template pages
        init: function () {
            runValidator2();
        }
    };
}();