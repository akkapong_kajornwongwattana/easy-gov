<?php namespace App\Http\Controllers;

class ApiController extends Controller {

    public function __construct()
    {
        //call base url
        session_start();

        parent::setBaseUrl();

    }  

    protected function getOutput($format="array", $params)
    {
        switch ($format) {
            case "array":
                $this->setArrayFormat($params);
                break;
            case "json":
                $this->setJsonFormat($params);
                break;
            default:
                $this->setArrayFormat($params);
        }
    }

    protected function setJsonFormat($params)
    {
        echo json_encode($params);
    }

    protected function setArrayFormat($params)
    {
        return $params;
    }

    protected function authentication()
    {
        return true;
    }  

}
