<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('uploadform', 'Html\UploadFormController@getUploadform');

Route::get('/', 'Html\LoginHtmlController@getLogin');
Route::controllers(['login' => 'Html\LoginHtmlController']);

/**
 * ADMIN 
 */
Route::group(['middleware' => 'App\Http\Middleware\Login'], function() {
    //superadmin
    Route::controllers(['user'         => 'Html\CRUD\SuperAdmin\UserController']);
    Route::controllers(['dashboard'    => 'Html\CRUD\DashboardController']);
    Route::controllers(['module'       => 'Html\CRUD\SuperAdmin\ModuleController']);
    Route::controllers(['app'          => 'Html\CRUD\SuperAdmin\AppController']);
    Route::controllers(['permission'   => 'Html\CRUD\SuperAdmin\UserAclController']);
    Route::controllers(['menu'         => 'Html\CRUD\SuperAdmin\MenuController']);
    Route::controllers(['people'       => 'Html\CRUD\SuperAdmin\PeopleController']);
    Route::controllers(['documenttype' => 'Html\CRUD\SuperAdmin\DocumentTypeController']);
    Route::controllers(['jobtype'      => 'Html\CRUD\SuperAdmin\JobTypeController']);
    
    //admin
    Route::controllers(['job'          => 'Html\CRUD\Admin\JobController']);
    Route::controllers(['reqjob'       => 'Html\CRUD\Admin\RequestJobController']);
    Route::controllers(['news'         => 'Html\CRUD\Admin\NewsController']);

    
});


/**
* API
*/
Route::group(['prefix' => 'api'], function() {
    Route::controllers(['people'         => 'Api\PeopleController']);
    Route::controllers(['peopledocument' => 'Api\PeopleDocumentController']);
    Route::controllers(['job'            => 'Api\JobController']);
    Route::controllers(['reqjob'         => 'Api\RequestJobController']);
    Route::controllers(['documenttype'   => 'Api\DocumentTypeController']);
    Route::controllers(['jobtype'        => 'Api\JobTypeController']);
    Route::controllers(['news'           => 'Api\NewsController']);
});
