var ProductList = function () {

    var handleDelete  = function() {

        $( '.deleteButton' ).on( 'click', function(event) {

            var id = $(this).attr('data-id');

            $('#delete').modal('show');

            // $('#delete').find("#okay").click(function() {

            //     if(id!=0){
            //         $.ajax({
            //             type:'post',
            //             url:base_url+'/service/delete',
            //             data:{'id':id },
            //             async: false,
            //             dataType: "json",
            //             success:function(result){

            //                  $('.modal-footer').html('<button type="button" data-dismiss="modal" class="btn green" id="okay">OK</button>');

            //                 if(result.status==290){
            //                     $('.modal-body').html(result.message);
            //                 } else {
            //                     $('.modal-body').html("Delete Complete!");
            //                     $('#list-'+id).remove();
            //                 }
            //             }
            //         });
            //     }

            // });

            $('#delete').on('hidden.bs.modal', function () {
                id = 0;
                $('.modal-body').html('Would you like to delete?');
                $('.modal-footer').html('<button type="button" data-dismiss="modal" class="btn">Cancel</button><button type="button" data-toggle="delete" class="btn green" id="okay">OK</button>');
            });
        });

    }

    return {
        //main function to initiate the module
        init: function () {
            handleDelete();
        }

    };




}();