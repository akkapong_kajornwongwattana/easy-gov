<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Config;
use App;
use Input;
use Redirect;

class AppApiCRUDController extends ApiController {

    private $appRepository;
    private $myValidate;

    private $module  = "App";

    public function __construct()
    {
        parent::__construct();

        $this->myValidate    = App::make('App\Services\MyValidate');
        $this->appRepository = App::make('App\Repositories\AppRepository');

        $this->userAcl = $this->canAccess($this->module);
    }

    /**
     * Delete
     */
    public function deleteDelete()
    {
        echo "<pre>";
        print_r('What_to_print_r');
        echo "\n\n=============\n\n";
        exit('########## EXIT ##########');
    }
    //------ End Interface method ------//

}