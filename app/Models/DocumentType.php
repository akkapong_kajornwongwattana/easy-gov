<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Documenttype extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'documenttypes';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    //=== Set relation ===//
    //many to many
    public function jobs()
    {
        return $this->belongsToMany('App\Models\Job');
    }
}
