<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\RequestJob;
use App;
use DB;
use Config;

class RequestJobRepository {

    protected $myService;
    protected $messages;

    private $requestJob;

    //Warnning : check change name in database (documenttype) must be 3
    private $changeNameId = 3;
    

    public function __construct(RequestJob $requestJob)
    {
        $this->myService  = App::make('App\Services\MyServices');
        
        $this->requestJob = $requestJob;
        
        //get message
        $this->messages   = Config::get('message');
    }

    //For get request job from condition (use in list)
    public function getRequestJob($keys, $operators, $params, $isCurrent=false)
    {
        //create param for add condition
        $conditions    = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $jobCollection = $this->myService->addConditiontoCollection($conditions, $this->requestJob);
        $jobCollection = $jobCollection->with('app', 'job.job_type', 'people'); 

        if ($isCurrent) {
            $jobCollection = $jobCollection->whereBetween('datetime', [date("Y-m-d 00:00:00"), date("Y-m-d 23:59:59")]);
        }

        $output        = $jobCollection->get()->toArray();
        
        return $output;
    }

    //Method for check document in server
    private function checkRequiredDocument($requriedDocs, $peopleDocs, $people) 
    {
        //define result
        $message = "";

        if (!empty($requriedDocs)) {
            foreach ($requriedDocs as $requriedDoc) {
                //get each document id
                $eachDocId = $requriedDoc["id"];

                //define found
                $found = false;

                //check change name
                if ($eachDocId == $this->changeNameId) {
                    //check only people who changed name
                    if (!$people["change_name"]) {
                        $found = true;
                    }

                }

                if (!$found){
                    //check in server
                    foreach ($peopleDocs as $peopleDoc) {
                        if ($peopleDoc["documenttype_id"] == $eachDocId) {
                            //set found
                            $found = true;
                            break;
                        }
                    }
                }
                

                if (!$found) {
                    if (!empty($message)) {
                        $message .= ",";
                    }
                    $message.= str_replace("[document]", $requriedDoc["name"], $this->messages["requestjob"]["no_doc"]);
                }

            }
        }
        return $message;
    }

    //Method for people request job
    public function requestJob($params)
    {
        //save request job to database
        $result = $this->addRequestJob($params);

        if (!$result["success"]) {
            return $result;
        }

        //create job repository
        $jobRepo       = App::make('App\Repositories\JobRepository');
        $job           = $jobRepo->getJobById($params["job_id"]);

        //get required document from job detail
        $requiredDocs = $job["documenttypes"];
        
        //create people document repository
        $peopleDocRepo = App::make('App\Repositories\PeopleDocumentRepository');
        $peopleDocs    = $peopleDocRepo->getPeopleDocument(["people_id"], ["="], ["people_id" => $params["people_id"]]);
        
        //create people repository
        $peopleRepo = App::make('App\Repositories\PeopleRepository');
        $people    = $peopleRepo->getPeopleById($params["people_id"]);

        //get mesage after check document in server
        $result["message"] = $this->checkRequiredDocument($requiredDocs, $peopleDocs, $people);
        
        return $result;
    }

    //Method for add request job to database
    public function addRequestJob($params) 
    {
        //TODO
        $result = [
            "success" => true,
            "message" => ""
            ];
        
        // add data to database
        $requestJob            = new RequestJob;
        // add data to collection object
        $requestJob->app_id    = $params["app_id"];
        $requestJob->people_id = $params["people_id"];
        $requestJob->job_id    = $params["job_id"];
        $requestJob->datetime  = date("Y-m-d H:i:s");
        
        if (!$requestJob->save()) {
            $result["success"] = false;
            $result["message"] = $this->messages["add_error"];
        }
        return $result;
    }

    //TODO : create corn job for call this method 
    //Method for delete job request
    public function clearRequestJob() 
    {
        //get request job that have datetime less than today
        $today       = date("Y-m-d 00:00:00");

        $this->requestJob->where('datetime', '<', $today)->delete();

        return true;
    }
    
    //Method for get print document
    public function getPrintDocument($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];

        //get request document
        $reqJob              = $this->requestJob->with('job.documenttypes')->find($id)->toArray();
        
        //get required documrnt
        $requiredDocs        = $reqJob["job"]["documenttypes"];
        //create list of required document id
        $requiredDocIds      = $this->myService->getListDataFromArr($requiredDocs, "id");
        
        //create people document repository
        $peopleDocRepo       = App::make('App\Repositories\PeopleDocumentRepository');
        //get documents
        $output["documents"] = $peopleDocRepo->getDocumentFromIdList($reqJob["people_id"], $requiredDocIds);
        return $output;
    }
}
