<?php namespace App\Http\Controllers\Html\CRUD\SuperAdmin;

use App\Http\Controllers\Html\CRUD\SuperAdminHtmlController;
use App\Repositories\JobTypeRepository;
use Config;
use App;
use Input;

class JobTypeController extends SuperAdminHtmlController
{
    private $myValidate;
    private $myResponse;
    private $canAccessMethod;
    private $jobTypeRepo;

    protected $limitFileSize;

    private $module      = 'JobTypeController';
    private $moduleName  = 'job type';

    private $mapping = [
        "list"   => "jobtype.list",
        "create" => "jobtype.form",
        "update" => "jobtype.form",
        "view"   => "jobtype.form"
    ];
    private $derimetorView     = ".";
    private $derimetorRedirect = "/";


    public function __construct(JobTypeRepository $jobTypeRepo)
    {
        parent::__construct();

        $this->myValidate      = App::make('App\Services\MyValidate');
        $this->myResponse      = App::make('App\Services\MyResponse');
        
        $this->jobTypeRepo     = $jobTypeRepo;
        
        //check permission
        $this->canAccessMethod = $this->canAccess();
    }

    //------- Interface method --------//
    public function getList()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."list ".$this->moduleName);
        }

        //can use this method
        //get job type List
        $jobTypes = $this->jobTypeRepo->getJobType([], [], []);
        //return to view
        return $this->getOutput($this->mapping["list"], ["datas" => $jobTypes, "module_name" => $this->module]);
    }

    public function getCreate()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."create ".$this->moduleName);
        }

        //can use this method
        $datas         = [];
        

        return $this->getOutput($this->mapping["create"], ["data" => $datas, "module_name" => $this->module]);
    }

    private function afterPostError($inputs, $message)
    {
        $result                = [];
        $result["error"]       = [];
        $result["error"][]     = $message;
        
        //Add old input
        $result["data"]        = $inputs;
        //add module name
        $result["module_name"] = $this->module;

        return $result;
    }

    public function postJobtype()
    {
        $inputs = Input::all();
        $mode = (empty($inputs["id"]))? "create" : "update";

        //check permission
        if (!$this->canAccessMethod) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$mode." ".$this->moduleName);
        }

        //can use this method

        if (empty($inputs["id"])) {
            //create
            $result = $this->jobTypeRepo->addJobType($inputs);
        } else {
            //update
            $result = $this->jobTypeRepo->updateJobType($inputs);
        }

        if (!$result['success']) {
            $result = $this->afterPostError($inputs, $result['message']);
        }
        //redirect page
        return $this->changeToPage(str_replace($this->derimetorView, $this->derimetorRedirect, $this->mapping["list"]), $this->mapping["create"], $result);
    }

    public function getView($id)
    {


    }

    public function getUpdate($id)
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."update ".$this->moduleName);
        }

        //can use this method

        $inputs  = Input::all();
        //update Category
        $jobType = $this->jobTypeRepo->getJobType(["id"], ["="], ["id" => $id]);
        if (!empty($jobType)) {
            $jobType = $jobType[0];
        }

        //return to view
        return $this->getOutput($this->mapping["update"], ["data" => $jobType, "module_name" => $this->module]);
    }

    public function deleteDelete()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."delete ".$this->moduleName);
        }

        //can use this method
        $inputs = Input::all();
        $id     = $inputs["id"];

        $result = $this->jobTypeRepo->deleteJobType($id);
        //return to view
        return $this->myResponse->getOutput('json', $result);
    }
    //------ End Interface method ------//

}