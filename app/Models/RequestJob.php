<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestJob extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'request_job';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['people_id', 'job_id', 'datetime'];

    //=== set relation ===//
    public function job()
    {
        return $this->belongsTo('App\Models\Job');
    }

    public function people()
    {
        return $this->belongsTo('App\Models\People');
    }

    public function app()
    {
        return $this->belongsTo('App\Models\AppModel');
    }
}
