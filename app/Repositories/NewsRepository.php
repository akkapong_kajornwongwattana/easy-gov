<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\News;
use App;
use DB;
use Config;

class NewsRepository {

    protected $news;
    protected $myService;
    protected $messages;
    

    public function __construct(News $news)
    {
        $this->news      = $news;
        
        $this->myService = App::make('App\Services\MyServices');
        
        //get message
        $this->messages  = Config::get('message');
    }

    //For get job type from condition 
    public function getNews($keys, $operators, $params)
    {
        //create param for add condition
        $conditions     = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $newsCollection = $this->myService->addConditiontoCollection($conditions, $this->news);
        $output         = $newsCollection->with('app')->get()->toArray();
        
        return $output;
    }

    public function getNewsById($id)
    {
        return $this->news->with('app')->find($id)->toArray();
    }

    // //check duplicate module
    // private function duplicatenews($param)
    // {
    //     //define parameters
    //     $keys               = ["name"];
    //     $operators          = ["="];
        
    //     $isDuplicate        = $this->myService->checkDuplicate($this->news, $keys, $operators, $param);
        
    //     return $isDuplicate;
    // }

    public function addNews($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        
        // add data to database
        $news              = new news;
        // add data to collection object
        $news->app_id      = $param["app_id"];
        $news->title       = $param["title"];
        $news->description = $param["description"];
        
        if (!$news->save()) {
            $result["success"] = false;
            $result["message"] = $this->messages["add_error"];
        }
        return $result;
    }

    // private function checkDupForUpdate($param)
    // {
    //     $isDuplicate = false;
    //     //check duplicate
    //     $duplicates = $this->duplicatenews($param);
    //     if ($duplicates[0]) {
    //         $tmp = $duplicates[1];
    //         //check self
    //         if ((int)$tmp["id"] != (int)$param["id"]) {
    //             $isDuplicate = true;
    //         }
    //     }
    //     return $isDuplicate;
    // }

    public function updateNews($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        if (!empty($param["id"])) {
            //get record
            $news              = $this->news->find($param['id']);
            
            $news->app_id      = $param["app_id"];
            $news->title       = $param["title"];
            $news->description = $param["description"];

            if (!$news->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["update_error"];
            }
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
    }

    public function deleteNews($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];

        $news = $this->news->find($id);

        if (!$news->delete()) {
                
            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];

        }

        return $output;
    }

}