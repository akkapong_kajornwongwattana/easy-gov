<?php namespace App\Http\Controllers\Html;

use App\Http\Controllers\HtmlController;
use App\Repositories\UserRepository;
use App;
use Input;
use Session;
use Cookie;

class LoginHtmlController extends HtmlController {

    protected $myValidate;
    protected $userRepo;

    protected $redirectMap = [
        'login' => 'login/login',
        'main'  => 'dashboard/list'
    ];
    protected $mapping = [
        'login' => 'login'
    ];

    public function __construct(UserRepository $userRepo)
    {
        parent::__construct();

        $this->myValidate = App::make('App\Services\MyValidate');

        $this->userRepo   = $userRepo;
    }

    /**
    * show view
    */
    public function getLogin(){

        return $this->getOutput($this->mapping["login"], ['data' => []]);
    }

    /**
    * redirect to page input after update/add data to db
     *
     * @param  String $page , Object $result (from add/update)
     * @return redirect page
    */
    private function afterLogin($result) {
        //define params
        $params = [];
        if ($result[0]) {
            $user = $result[1];
            //set session
            Session::put('userId', $user["id"]);
            Session::put('userName', $user["username"]);
            //set cookie
            $cookie = Cookie::make('username', $user["username"], (time() + 3600));
            //redirect to main page
            return redirect($this->redirectMap['main'])->withCookie($cookie);
        } else {
            //show error page
            return $this->getOutputWithErrormsg($this->mapping['login'], [], $result[1]);
        }
    }

    // login //
    public function postLogin(){

        $inputs = Input::all();


        $rules = array(
            'username' => 'required',
            'password' => 'required',
        );

        $validation = $this->myValidate->validator($inputs, $rules, []);

        if (isset($validation['error'])) {
            //return error
            return view($this->mapping["login"])->withErrormsg($validation['error']);

        } else {

            //check login
            $result = $this->userRepo->checkLogin($inputs);

            return $this->afterLogin($result);

        }

    }

    // logout //
    public function getLogout()
    {
        //clear session
        Session::flush();
        //clear cookie
        $cookie = Cookie::forget('username');
        //goto login page
        return redirect($this->redirectMap['login'])->withCookie($cookie);;
    }


}

?>