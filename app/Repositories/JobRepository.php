<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\Job;
use App;
use DB;
use Config;

class JobRepository {

    protected $job;
    protected $myService;
    protected $messages;
    

    public function __construct(Job $job)
    {
        $this->job       = $job;
        
        $this->myService = App::make('App\Services\MyServices');
        
        //get message
        $this->messages  = Config::get('message');
    }

    //For get job from condition (use in list)
    public function getJob($keys, $operators, $params)
    {
        //create param for add condition
        $conditions    = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $jobCollection = $this->myService->addConditiontoCollection($conditions, $this->job);
        $jobCollection = $jobCollection->with('job_type', 'app', 'documenttypes');
        $output        = $jobCollection->get()->toArray();
        
        return $output;
    }

    //For get job by id (use in edit)
    public function getJobById($id)
    {
        return $this->job->with('job_type', 'documenttypes')->find($id)->toArray();
    }

    //check duplicate module
    private function duplicateJob($param)
    {
        //define parameters
        $keys               = ["job_type_id", "app_id"];
        $operators          = ["=", "="];
        
        $isDuplicate        = $this->myService->checkDuplicate($this->job, $keys, $operators, $param);
        
        return $isDuplicate;
    }

    public function addJob($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicateJob($param);

        if (!$isDups[0]) {

            // add data to database
            $job                  = new Job;
            // add data to collection object
            $job->job_type_id     = $param["job_type_id"];
            $job->app_id          = $param["app_id"];
            $job->position_detail = $param["position_detail"];
            
            if (!$job->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            }

            //TODO 
            if (isset($param['documenttype_ids']) && !empty($param['documenttype_ids'])) {
                //insert data to job_documenttype
                $job->documenttypes()->attach(explode(",", $param['documenttype_ids']));
            } 
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;
    }

    private function checkDupForUpdate($param)
    {
        $isDuplicate = false;
        //check duplicate
        $duplicates = $this->duplicateJob($param);
        if ($duplicates[0]) {
            $tmp = $duplicates[1];
            //check self
            if ((int)$tmp["id"] != (int)$param["id"]) {
                $isDuplicate = true;
            }
        }
        return $isDuplicate;
    }

    public function updateJob($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        if (!empty($param["id"])) {
            //get record
            $job         = $this->job->find($param['id']);

            //check duplicate
            $isDuplicate = $this->checkDupForUpdate($param);

            if (!$isDuplicate){
                
                $job->job_type_id     = $param["job_type_id"];
                $job->app_id          = $param["app_id"];
                $job->position_detail = $param["position_detail"];
            

                if (!$job->save()) {
                    $result["success"] = false;
                    $result["message"] = $this->messages["update_error"];
                }

                //TODO
                //delete all assosiate
                $job->documenttypes()->detach();

                if (isset($param['documenttype_ids']) && !empty($param['documenttype_ids'])) {
                    //insert data to job_documenttype
                    $job->documenttypes()->attach(explode(",", $param['documenttype_ids']));
                }
                
            } else {
                $result["success"] = false;
                $result["message"] = $this->messages["duplicate"];
            }
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
    }

    public function deleteJob($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];

        $job = $this->job->find($id);

        if (!$job->delete()) {
                
            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];

        }

        //delete all assosiate
        $job->documenttypes()->detach();

        return $output;
    }

    //Method for create coddition
    private function createCondition($inputs)
    {
        //define outputs
        $keys       = [];
        $operators  = [];
        $parameters = [];
        //define allow condition 
        $allows = ["job_type_id", "app_id"];
        foreach ($inputs as $key => $value) {
            //check allow
            if (in_array($key, $allows)) {
                $keys[] = $key;
                $operators[] = "=";
                $parameters[$key] = $value;
            }   
        }
        return [$keys, $operators, $parameters];
    } 

    //Method for get path of document
    private function getPathFile($documentId, $haveDocuments)
    {
        foreach ($haveDocuments as $haveDocument) {
            if ($haveDocument["documenttype_id"] == $documentId) {
                return $haveDocument["file"];
            }
        }
        return "";
    }

    //Method for generate filed from document
    private function generateFieldFromDocuments($reqDocuments, $haveDocuments, $haveDocumentIds)
    {
        //define output
        $outputs = [];

        if (!empty($reqDocuments)) {

            foreach ($reqDocuments as $document) {
                //define each field 
                $eachField = [
                    "label"       => $document["name"],
                    "document_id" => $document["id"],
                    "file_path"   => "",
                ];

                if (in_array($document["id"], $haveDocumentIds)) {
                    //already have document
                    $eachField["file_path"] = $this->getPathFile($document["id"], $haveDocuments);
                }
                
                //add to output
                $outputs[] = $eachField;
            }
        }
        return $outputs;
    }

    //Method for manage document control duplocate data
    private function manageDocumentForUploadForm($jobDatas)
    {
        //define output
        $documents = [];

        //define temp id (for keep document id that kept in output)
        $tmpIds = [];

        foreach ($jobDatas as $jobData) {
            //get each documents
            $eachDocs = $jobData["documenttypes"];

            foreach ($eachDocs as $eachDoc) {
                //Check the document not be in output
                if (!in_array($eachDoc["id"], $tmpIds)) {
                    //keep in output
                    $documents[] = $eachDoc;
                    //keep id in temp
                    $tmpIds[]    = $eachDoc["id"];
                }
            }
        }
        return $documents;
    }


    //Method for get job form
    public function getUploadForm($params)
    {
        //creaet condition for get job
        $conditions = $this->createCondition($params);
        //get job data
        $jobDatas   = $this->getJob($conditions[0], $conditions[1], $conditions[2]);

        if (!empty($jobDatas)) {
            //get documents 
            $documents     =  $this->manageDocumentForUploadForm($jobDatas);
            //get current document
            $peopleDocRepo = App::make('App\Repositories\PeopleDocumentRepository');
            $currentDocs   = $peopleDocRepo->getPeopleDocument(["people_id"], ["="], ["people_id" => $params["people_id"]]);
            
            $haveDocumentIds = $this->myService->getListDataFromArr($currentDocs, "documenttype_id");

            return $this->generateFieldFromDocuments($documents, $currentDocs, $haveDocumentIds);

        }
        
        return [];

    }

    //Method for reformat data for api (add data move in application)
    public function moveDataToApp($datas)
    {
        //define outputs
        $apps = [];

        if (!empty($datas)) {
            foreach ($datas as $data) {

                if (!isset($apps[$data["app_id"]])) {
                    //add jobs key
                    $data["app"]["jobs"]   = [];
                    $apps[$data["app_id"]] = $data["app"];
                }
                //remove app
                unset($data["app"]);

                //add job
                $apps[$data["app_id"]]["jobs"][] = $data;


            }
        }

        return array_values($apps);
    }

}