<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserAcl;


class User extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['namename', 'user_type', 'password', 'app_id', 'official_account'];

    // /**
    //  * Relation between user and user_acl.
    //  *
    //  * @return Collection
    //  */
    public function user_acl()
    {
        return $this->hasMany('App\Models\UserAcl');
    }

    // /**
    //  * Relation between user and app.
    //  *
    //  * @return Collection
    //  */
    public function app()
    {
        return $this->belongsTo('App\Models\AppModel');
    }
    

    

}
