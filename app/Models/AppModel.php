<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ShelfModel;
use App\Models\User;

class AppModel extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table   = 'app';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'desciption'];

    // /**
    //  * Relation between app and user.
    //  *
    //  * @return Collection
    //  */
    public function user()
    {
        return $this->hasMany('App\Models\User', 'app_id', 'id');
    }

}
