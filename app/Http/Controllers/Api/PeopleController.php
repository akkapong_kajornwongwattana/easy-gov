<?php namespace App\Http\Controllers\Api;

use App\Repositories\PeopleRepository;
use App\Http\Controllers\ApiController;
use App;
use Input;

class PeopleController extends ApiController {

    protected $peopleRepo;
    protected $myValidate;
    protected $myResponse;


    function __construct(PeopleRepository $peopleRepo)
    {
        parent::__construct();

        $this->peopleRepo = $peopleRepo;
        $this->myValidate = App::make('App\Services\MyValidate');
        $this->myResponse = App::make('App\Services\MyResponse');
    }

    //Method for login people
    public function postLogin()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [
            'username' => 'required',
            'password' => 'required',
        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for check employee login
        //--- Process Section ---//
        $employeeLogins = $this->peopleRepo->checkLogin($params);
        if (!$employeeLogins[0]) {
          return $this->myValidate->validateError($employeeLogins[1]);
        }

        //--- Output Section ---//
        return $this->myResponse->output($employeeLogins[1]);
    }

    //Method for login employee
    public function postPeople()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [];

        if (!isset($input["id"])) {
            //create
            $rules = [
                'firstname' => 'required',
                'lastname'  => 'required',
                'card_no'   => 'required',
                'username'  => 'required',
                'password'  => 'required',
            ];
        } else {
            //update
            $rules = [
                'oldpassword' => 'required',
            ];
        } 

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for create or update people
        //--- Process Section ---//
        if (isset($input["id"]) && !empty($input["id"])){
            //update
            $result = $this->peopleRepo->updatePeople($params);
        } else {
            //create 
            $result = $this->peopleRepo->addPeople($params);
        }
        if (!$result["success"]) {
          return $this->myValidate->validateError($result["message"]);
        }

        //--- Output Section ---//
        return $this->myResponse->output($result);
    }


    // Get notification history
    public function postChangepassword()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        //customer_id_key for check validate system
        $rules = [
            'people_id'    => 'required',
            'old_password' => 'required',
            'new_password' => 'required',
        ];

        /* Input Default */
        $setInputDefault = [

        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for get supervisor of the employee
        //--- Process Section ---//
        $result = $this->peopleRepo->changePeoplePassword($params);
        if (!$result["success"]) {
          return $this->myValidate->validateError($result["message"]);
        }

        // //--- Output Section ---//
        // return $this->myResponse->output($history);
        //--- Output Section ---//
        return $this->myResponse->output("");
    }

    //Method for get profile
    public function getProfile()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [
            'people_id' => 'required'
        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for check employee login
        //--- Process Section ---//
        $result = $this->peopleRepo->getProfile($params);
        if (!$result["success"]) {
          return $this->myValidate->validateError($result["message"]);
        }

        //--- Output Section ---//
        return $this->myResponse->output($result["profile"]);
    }

   
}