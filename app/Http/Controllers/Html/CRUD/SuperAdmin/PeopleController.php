<?php namespace App\Http\Controllers\Html\CRUD\SuperAdmin;

use App\Http\Controllers\Html\CRUD\SuperAdminHtmlController;
use App\Repositories\PeopleRepository;
use Config;
use App;
use Input;

class PeopleController extends SuperAdminHtmlController{

    protected $peopleRepo;

    //private $userAcl;
    private $myValidate;
    private $myResponse;
    private $canAccessMethod;

    private $module      = "PeopleController";
    private $moduleName  = "people";
    private $mapping = [
        "list"   => "people.list",
        "create" => "people.form",
        "update" => "people.form",
        "view"   => "people.form"
    ];

    private $derimetorView = ".";
    private $derimetorRedirect = "/";

    public function __construct(PeopleRepository $peopleRepo)
    {
        parent::__construct();

        $this->myValidate      = App::make('App\Services\MyValidate');
        $this->myResponse      = App::make('App\Services\MyResponse');
        
        //check permission
        $this->canAccessMethod = $this->canAccess();
        
        $this->peopleRepo      = $peopleRepo;
        }

    //------- Interface method --------//
    public function getList()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        //get user list
        //seletct all
        $users = $this->peopleRepo->getPeople([], [], []);
        return $this->getOutput($this->mapping["list"], ["datas" => $users, "module_name" => $this->module]);
    }

    public function getCreate()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        //get application
        // $apps = $this->appRepo->getApp([], [], []);

        // //can use this method
        // return $this->getOutput($this->mapping["create"], ["data" => ["apps" => $apps], "module_name" => $this->module]);

    }

    // public function postPeople()
    // {
    //     //check permission
    //     if (!$this->canAccessMethod) {
    //         //return 
    //         return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
    //     }
    //     $inputs = Input::all();
        
    //     if (!$inputs["id"]) {
    //         //create
    //         $result = $this->userRepo->addUser($inputs);
    //     } else {
    //         //update
    //         $result = $this->userRepo->updateUser($inputs);
    //     }

    //     if (!$result['success']) {
    //         $result["error"]       = array();
    //         $result["error"][]     = $result['message'];
    //         //Add old input
    //         $result["data"]        = $inputs;
    //         //add module name
    //         $result["module_name"] = $this->module;
    //     }
    //     //redirect page
    //     return $this->changeToPage(str_replace($this->derimetorView, $this->derimetorRedirect, $this->mapping["list"]), $this->mapping["create"], $result);
    // }

    public function getView($id)
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        // //get Module
        // $users = $this->userRepo->getUser(["id"], ["="], ["id" => $id]);
        // if (!empty($users)) {
        //     $users = $users[0];
        // }
        // //get application
        // $apps = $this->appRepo->getApp([], [], []);
        // //add apps to users
        // $users["apps"] = $apps;
        // //return to view
        // return $this->getOutput($this->mapping["view"], ["data" => $users, "module_name" => $this->module]);
    }
    
    public function getUpdate($id)
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        // //update Module
        // $users = $this->userRepo->getUser(["id"], ["="], ["id" => $id]);
        // if (!empty($users)) {
        //     $users = $users[0];
        // }
        // //get application
        // $apps = $this->appRepo->getApp([], [], []);
        // //add apps to users
        // $users["apps"] = $apps;
        // //return to view
        // return $this->getOutput($this->mapping["update"], ["data" => $users, "module_name" => $this->module]);
    }

    public function deleteDelete()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        $inputs = Input::all();
        $id = $inputs["id"];

        $result = $this->peopleRepo->deletePeople($id);
        //return to view
        return $this->myResponse->getOutput('json', $result);
    }
    //------ End Interface method ------//

}
