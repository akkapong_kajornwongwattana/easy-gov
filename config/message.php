<?php

return [
    "no_permission" => "You have no permision to access ",
    "duplicate"     => "Data is Duplicate.",
    "add_error"     => "Cannot add new record.",
    "update_error"  => "Cannot update this record.",
    "delete_error"  => "Cannot delete this record.",
    "login_fail"    => "Username or password is not correct!!",
    "curl_fail"     => "Curl failed!",
    "people" => [
        "no_data"           => "No people data",
        "wrong_oldpassword" => "Old password is not match",
    ],
    "requestjob" => [
        "no_doc" => "ไม่พบ [document] ในระบบกรุณานำติดตัวมาด้วย",
    ]
    
];
