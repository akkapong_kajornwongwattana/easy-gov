
@extends('formtemplate')

@section('form_name_page')
Module
@stop

@section('form_link_header')
                                    <li>
                                        <a href="{{ URL::to('module/list') }}">
                                            List
                                        </a>
                                    </li>
                                    <li class="active">
                                        Module
                                    </li>
@stop

@section('form_content')
                                        <!-- <h1><i class="fa fa-pencil-square"></i> MODULE</h1>
                                        <hr> -->
                                        <form id="form" action="{{ URL::to('module/module') }}" method="POST" role="form" >
                                            <input type="hidden" id="id" name="id" value="{{$data['id'] or ''}}" />
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="errorHandler alert alert-danger no-display">
                                                        <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                    </div>
                                                    <div class="successHandler alert alert-success no-display">
                                                        <i class="fa fa-ok"></i> Your form validation is successful!
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Module Name <span class="symbol required"></span>
                                                        </label>
                                                        <input type="text" placeholder="Insert controller module name" class="form-control" id="module_name" name="module_name" value="{{$data['module_name'] or ''}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Description <span class="symbol"></span>
                                                        </label>
                                                        <textarea class="form-control" name="description" id="description" >{{$data['description'] or ''}}</textarea>
                                                    </div>
                                                    <br>
                                                    <!-- PERMISSION SECTION -->
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Has Create <span class="symbol"></span>
                                                            </label>
                                                            <input type="checkbox" class="form-control flat-teal" name="has_create" value="true"
                                                            @if ((isset($data['has_create'])) && ($data['has_create']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Has Read <span class="symbol"></span>
                                                            </label>
                                                            <input type="checkbox" class="form-control flat-teal" name="has_read" value="true"
                                                            @if ((isset($data['has_read'])) && ($data['has_read']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Has Update <span class="symbol"></span>
                                                            </label>
                                                            <input type="checkbox" class="form-control flat-teal" name="has_update" value="true"
                                                            @if ((isset($data['has_update'])) && ($data['has_update']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Has Delete <span class="symbol"></span>
                                                            </label>
                                                            <input type="checkbox" class="form-control flat-teal" name="has_delete" value="true"
                                                            @if ((isset($data['has_delete'])) && ($data['has_delete']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                    </div>
                                                    <!-- MENU SECTION -->
                                                    <div class="">
                                                        <label class="control-label font-bold">
                                                            Menu <span class="symbol"></span>
                                                        </label>
                                                        <select name="menu_id" class="form-control">
                                                            <option value="">Select Menu..</option>
                                                        @if (isset($data['menuList']))
                                                            @foreach ($data['menuList'] as $eachmenu)
                                                            <option value="{{ $eachmenu['menu_id'] }}"
                                                            @if ((isset($data['menu_id']))&&($data['menu_id'] == $eachmenu['menu_id']))
                                                                selected
                                                            @endif
                                                            >{{$eachmenu['menu']}}</option>
                                                            @endforeach
                                                        @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <span class="symbol required"></span>Required Fields
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">

                                                </div>
                                                 <div class="col-md-4">
                                                    <button id="save-btn" class="btn btn-primary btn-block" type="submit">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </form>
@stop

@section('js_req_for_form')
<script src="{{ URL::asset('additional_js/module-validation.js') }}"></script>
@stop

@section('form_jquery')
        FormValidator.init();
@stop

@section('form_jquery')
@stop
