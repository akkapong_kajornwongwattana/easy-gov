<?php namespace App\Interfaces;

interface CRUDInterface {

    public function getList();
    public function getCreate();
    public function postStore();
    public function getView($id);
    public function getUpdate($id);
    public function putUpdate();
    public function deleteDelete();
}