<?php namespace App\Http\Controllers\Html\CRUD\SuperAdmin;

use App\Http\Controllers\Html\CRUD\SuperAdminHtmlController;
use App\Repositories\AppRepository;
use Config;
use App;
use Input;
use Redirect;

class AppController extends SuperAdminHtmlController {

    private $appRepository;
    private $myValidate;
    private $myResponse;
    private $cdnRepository;
    private $canAccessMethod;

    private $module     = "AppController";
    private $moduleName = 'App';
    private $mapping    = [
        "list"        => "app.list",
        "create"      => "app.form",
        "update"      => "app.form",
        "view"        => "app.form"
    ];
    private $derimetorView     = ".";
    private $derimetorRedirect = "/";

    public function __construct(AppRepository $appRepository)
    {
        parent::__construct();

        $this->myValidate          = App::make('App\Services\MyValidate');
        $this->myResponse          = App::make('App\Services\MyResponse');
        $this->cdnRepository       = App::make('App\Services\CdnServices');

        $this->appRepository = $appRepository;

        //check permission
        $this->canAccessMethod    = $this->canAccess();
    }

    //------- Interface method --------//
    /**
     * List data table
     */
    public function getList()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        //can use this method
        $data = $this->appRepository->getApp([], [], [], true);

        return $this->getOutput($this->mapping['list'], ['data' => $data, "module_name" => $this->module ]);
    }

    /**
     * Create form
     */
    public function getCreate()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        //can use this method
        $data                              = [];
        return $this->getOutput($this->mapping["create"], [ 'data' => $data, "module_name" => $this->module ]);
    }

    private function afterPostError($inputs, $message)
    {

        $result["error"]       = array();
        $result["error"][]     = $message;
        //Add old input
        $result["data"]        = $inputs;
        //add module name
        $result["module_name"] = $this->module;

        return $result;
    }

    /**
     * Save data
     */
    public function postApp()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $input = Input::all();

        /* Rules Validation*/
        $rules = array(
            'name'         => 'required',
        );

        /* Input Default */
        $setInputDefault = array(
        );

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);

        if (isset($params['error'])) {
            $result = $this->afterPostError($input, $params['error']);

        } else {

            if (!$params["id"]) {
                //create
                $result = $this->appRepository->addApp($params);

            } else {
                //update
                $result = $this->appRepository->updateApp($params);
            }

            if (!$result['success']) {
                $result = $this->afterPostError($input, $result['message']);
            }
        }

        //redirect page
        return $this->changeToPage(str_replace($this->derimetorView, $this->derimetorRedirect, $this->mapping["list"]), $this->mapping["create"], $result);
    }

    /**
     * View detail
     */
    public function getView($id)
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        //can use this method
    }


     

    /**
     * Update form
     */
    public function getUpdate($id)
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        //can use this method
        $apps = $this->appRepository->getApp(["id"], ["="], ["id" => $id], true);
        if (!empty($apps)) {
            $app = $apps[0];
        }

        $data   = $app;

        return $this->getOutput($this->mapping['update'], ['data' => $data, "module_name" => $this->module ]);
    }

    
    /**
     * Delete
     */
    public function deleteDelete()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        //can use this method
        $input = Input::all();

        /* Rules Validation*/
        $rules = array(
            'id' => 'required|numeric',
        );

        /* Input Default */
        $setInputDefault = array(
        );

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        $result = $this->appRepository->deleteApp($params['id']);

        return $this->myResponse->getOutput('json', $result);
    }

    //------ End Interface method ------//

}