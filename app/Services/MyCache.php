<?php namespace App\Services;

Use Cache;
Use Config;

class MyCache {

    public function generateCacheKey($method, $param)
    {
        return $method.":".json_encode($param);
    } 

    public function getCache($cacheKey)
    {
        $outputData = "";
        if ( Cache::has($cacheKey) )
        {
            $outputData = Cache::get($cacheKey);
        }
        return $outputData;
    }

    public function addCacheData($cacheKey, $data)
    {
        Cache::put($cacheKey, $data, Config::get("cache.lifetime"));
    }

    public function clearCache($cacheKey)
    {
        Cache::forget($cacheKey);
    }

    
}