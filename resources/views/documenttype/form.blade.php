
@extends('formtemplate')

@section('form_name_page')
Document Type
@stop

@section('form_link_header')
                                    <li>
                                        <a href="{{ URL::to('documenttype/list') }}">
                                            List
                                        </a>
                                    </li>
                                    <li class="active">
                                        Document Type
                                    </li>
@stop

@section('form_content')
                                        <h1><i class="fa fa-pencil-square"></i> DOCUMENT TYPE</h1>
                                        <hr>
                                        <form id="form" action="{{ URL::to('documenttype/documenttype') }}" method="POST" role="form" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" value="{{$data['id'] or ''}}" />
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="errorHandler alert alert-danger no-display">
                                                        <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                    </div>
                                                    <div class="successHandler alert alert-success no-display">
                                                        <i class="fa fa-ok"></i> Your form validation is successful!
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <!-- <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Application Name <span class="symbol required"></span>
                                                        </label>
                                                        <select id="app_id" name="app_id" class="form-control">
                                                        @if (isset($data['apps']))
                                                            @foreach ($data['apps'] as $app)
                                                            <option value="{{ $app['id'] }}"
                                                            @if ((isset($data['app_id']))&&($data['app_id'] == $app['id']))
                                                                selected
                                                            @endif
                                                            >{{$app['name']}}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="">Select Application..</option>
                                                        @endif
                                                        </select>
                                                    </div> -->
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Document Type <span class="symbol required"></span>
                                                        </label>
                                                        <input type="text" placeholder="Insert document name" class="form-control" id="name" name="name" value="{{$data['name'] or ''}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <span class="symbol required"></span>Required Fields
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">

                                                </div>
                                                 <div class="col-md-4">
                                                    <button id="save-btn" class="btn btn-primary btn-block" type="submit">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </form>


@stop

@section('js_req_for_form')
<script src="{{ URL::asset('additional_js/documenttype-validation.js') }}"></script>
@stop

@section('form_jquery')
        FormValidator.init();
@stop

@section('form_js_script')

@stop