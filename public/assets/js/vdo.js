var FormValidator = function () {
	"use strict";
	 
    // function to initiate Validation Sample 1
    var runValidator1 = function () {
        var $form1 = $('#file_info');
        var errorHandler1 = $('.errorHandler', $form1);
        var successHandler1 = $('.successHandler', $form1);

        function getYoutubeId(url) {
            var video_id = url.split('v=')[1];
            if (video_id) {
                var ampersandPosition = video_id.indexOf('&');
                if(ampersandPosition != -1) {
                  video_id = video_id.substring(0, ampersandPosition);
                }
            }
            return video_id;
        }

        $.validator.addMethod("checkUrl", function () {
            //if all values are selected$
            var url = $("#youtube").val();
            if (url == "") {
                return false
            } else {
                //check youtube id
                var youtube_id = getYoutubeId(url);
                if (youtube_id) {
                    url = url.toLowerCase();
                
                    if (url.indexOf("https") == 0) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
                
            }
            

        }, 'Please input Https Youtube link.');


        $form1.validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                    error.insertAfter($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: "",
            rules: {
                'youtube': "checkUrl"
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler1.hide();
                errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (form) {
                //successHandler1.show();
                errorHandler1.hide();
                // submit form
                form.submit();
            }
        });
    };
    
    return {
        //main function to initiate template pages
        init: function () {
        	runValidator1();
        }
    };
}();

function setDataToField(id, data) {
    var cmp = $("#"+id);

    if (cmp) {
        if (cmp.prop('tagName') == "IMG") {
            //Image
            cmp.attr("src", data);
        }
        else {
            //Text
            cmp.val(data);
        }

    }
}

function getValueFromCheckbok(list) {
    var return_vals = [];

    for (var i=0;i<list.length;i++) {
        var view = list[i];
        //get value
        var view_cmp = $('#'+view);
        var parents_class = view_cmp.parents().attr("class");
        if (parents_class.indexOf("checked") != -1) {
            var each_val = view_cmp.val();
            return_vals.push(each_val);
        }
    }

    return return_vals;
}

function getAllFileInfo() {
    //get status
    var status = $('input:radio[name=status]:checked').val();
    //add status to form
    $("#status").val(status);
    //get view
    var view_list = getValueFromCheckbok(["view_on_web", "view_on_app"]);
    var view = view_list.toString();
    //add view_on to form
    $("#view_on").val(view);
    //get thumbnail
    var image = $("#img-thumbnail").attr("src");
    //add thumbnail to form
    $("#thumbnail").val(image);
    //add youtube url to form
    var youtube = $("#youtube").val();
    $("#url").val(youtube);


    //add created_by to form
    $("#created_by").val(1);  //TODO TEST
    //add updated_by to form
    $("#updated_by").val(2);  //TODO TEST
}

function getIdFromURL(url) {
    var temp = url.split("/");
    var id = "";
    if (temp.length > 1)
    {
        var tmpId = temp[(temp.length-1)].replace("#", "");
        id = parseInt(tmpId)||"";
    }
    return id;
}

function clearAllView() {
    $("#view_on_web").prop("checked", false);
    $("#view_on_app").prop("checked", false);
}

function checkRadio(check, clears) {
    var checkRadio = $("input[name=status][value="+check+"]");
    checkRadio.attr('checked', 'checked');
    var parents = checkRadio.parents().addClass("checked");

    //clear 
    for (var i=0;i<clears.length;i++) {
        var each = clears[i];
        var clearRadio = $("input[name=status][value="+each+"]");
        clearRadio.removeAttr('checked');
        var parents = clearRadio.parents().removeClass("checked");
    }
}

function handelMenu(){
    $('#entertainment').addClass("active open");
    $('#entertain-vdo').addClass("active");
}

function handleSave() {
    //save button
    $('#save-button').click(function() {
        //get all input
        var vdo_detail_block = $('#youtube_append');
        if ($('#youtube_append').css('display') != 'none') {
            getAllFileInfo();
            $("#file_info").submit();
        } else {
            //Show error
            bootbox.alert("Please Sync with youtube first.");
        } 
        
        

    });
}