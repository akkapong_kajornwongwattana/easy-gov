<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\Menu;
use App;
use Session;
use Config;
use DB;

class MenuRepository {
    protected $menu;
    protected $user;
    protected $userId;
    protected $messages;
    protected $myService;
    

    public function __construct(Menu $menu) 
    {   
        $this->menu      = $menu;
        
        $this->user      = App::make('App\Models\User');
        $this->myService = App::make('App\Services\MyServices');
        $this->messages  = Config::get('message');
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function getUserId($userId)
    {
        return $this->userId;
    }

    public function checkUserAclForMenu($menu_id)
    {
        if (!empty($this->userId)) {
            //find user
            $userAclDatas = $this->user->find($this->userId)->user_acl()->get();
            foreach ($userAclDatas as $userAclData) {
                $module        = $userAclData->module()->first()->toArray();
                if ($module["menu_id"] == $menu_id) {
                    if ($userAclData->toArray()["side_menu"]) {
                        return [true, $module["module_name"]];
                    } else {
                        return [false];
                    }
                    
                }

            }
        }
        return [false];
    }

    public function getUserAclForMenu($id, $menus) 
    {
        $datas = $this->menu->where('parent_id', $id)->orderBy('order')->get()->toArray();
        $count = 0;
        
        foreach ($datas as $data) {
            //keep data
            $canAccess = $this->checkUserAclForMenu($data["id"]);
            if ($canAccess[0]) {
                $data["module_name"] = $canAccess[1];
                $menus[]             = $data;

                //get child
                $tmp = $this->menu->where('parent_id', $data["id"])->orderBy('order')->get()->toArray();
                if (!empty($tmp)) {
                    //recursive
                    $menus[$count]["child"] = [];
                    $menus[$count]["child"] = $this->getUserAclForMenu($data["id"], $menus[$count]["child"]);

                    if (empty($menus[$count]["child"])) {
                        //unset
                        unset($menus[$count]["child"]);
                    }
                }

                //Increase count
                $count++;
            }
        }

        return $menus;

    }

    public function deleteMenu($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];
        if ($id) {
            $menu    = $this->menu->find($id);

            if (!$menu->delete()) {
                $output["status"]  = 290;
                $output["message"] = $this->messages["delete_error"];
            }
        }
        
        return $output;
    }

    //============ MENU ===============
    private function getAllMenu($id, $menus) 
    {
        $datas = $this->menu->where('parent_id', $id)->orderBy('order')->get()->toArray();
        $count = 0;
        foreach ($datas as $data) {
            $menus[]             = $data;
            //get child
            $tmp = $this->menu->where('parent_id', $data["id"])->orderBy('order')->get()->toArray();
            if (!empty($tmp)) {
                //recursive
                $menus[$count]["child"] = [];
                $menus[$count]["child"] = $this->getAllMenu($data["id"], $menus[$count]["child"]);

                if (empty($menus[$count]["child"])) {
                    //unset
                    unset($menus[$count]["child"]);
                }
            }
            $count++;
        }

        return $menus;

    }

    private function formatMenuList($datas, $mappingList)
    {
        $outputs = [];
        foreach ($datas as $data) {
            $tmp = [];
            foreach ($mappingList as $key => $asKey) {
                $tmp[$asKey] = $data[$key];
            }

            $outputs[] = $tmp;
        }

        return $outputs;
    }

    public function getMenuList()
    {
        $menuList = $this->menu->get()->toArray();
        $mappingList = [
            "id"                => "menu_id",
            "menu_display_text" => "menu"
        ];

        $output = $this->formatMenuList($menuList, $mappingList);
        return $output;
    }

    public function getMenu()
    {
        $menus = $this->getAllMenu(0, []);
        // dd($menus);
        return $menus;
    }

    // //check duplicate module
    // private function duplicateMenu($param)
    // {
    //     //define parameters
    //     $keys        = ["id"];
    //     $operators   = ["="];
        
    //     $isDuplicate = $this->myService->checkDuplicate($this->menu, $keys, $operators, $param);
        
    //     return $isDuplicate;
    // }

    private function insertRecord($data, $order)
    {
        // if (empty($data["id"])) {
        //     //test
        //     $maxId = $this->getMaxId();
        //     $maxId++;
            
        //     $data["id"] = $maxId;
        // }
        
        $menu                    = new Menu;
        $menu->id                = $data["id"];
        $menu->menu_display_text = $data["menu"];
        $menu->parent_id         = $data["parent_id"];
        $menu->url               = $data["url"];
        $menu->order             = $order;

        return [$menu->save(), $data["id"]];
    }

    //Method for assign new id to item
    private function assignNewId($datas, $currentId)
    {
        //define outputs
        $outputs = [];
        if (!empty($datas)) {
            foreach ($datas as $data) {
                //assign id
                if (empty($data["id"])){
                    $data["id"] = $currentId;
                    $currentId++;
                }
                //$data["id"] = $currentId;
                //plus current id
                //$currentId++;

                if (isset($data["children"])) {
                    //recursive
                    $results          = $this->assignNewId($data["children"], $currentId);
                    //keep children
                    $data["children"] = $results[0];
                    //update currentId
                    $currentId        = $results[1];
                }
                //keep in $outputs
                $outputs[] = $data;
            }
        }
        return [$outputs, $currentId];
    }

    private function fineCurrentId($menuDatas, $currntId)
    {
        //define current id
        if (!empty($menuDatas)) {
            foreach ($menuDatas as $menu) {
                if ((int)$menu["id"] > $currntId) {
                    $currntId = (int)$menu["id"];
                }

                //children
                if (isset($menu["children"])) {
                    //recursive
                    $currntId = $this->fineCurrentId($menu["children"], $currntId);
                }
            }
        }
        return $currntId;
        
    }

    private function insertNewMenus($menuDatas)
    {
        //reassign id to menu data

        $nextCurrentId = $this->fineCurrentId($menuDatas, 0) + 1;

        $results       = $this->assignNewId($menuDatas, $nextCurrentId);
        $menuDatas     = $results[0];
        //define order
        $order         = 1;
        //insert new
        foreach ($menuDatas as $menuData) {
            //insert main
            $menuData["parent_id"] = 0;
            $res = $this->insertRecord($menuData, $order);
            //cout order
            $order += 1;
            //check error
            if (!$res[0]) {
                return false;
            }

            //update id
            $menuData["id"] = $res[1];

            if (isset($menuData["children"])) {
                //define sub order
                $subOrder = 1;
                //have sub
                foreach ($menuData["children"] as $child) {
                    //insert sub
                    $child["parent_id"] = $menuData["id"];
                    $res = $this->insertRecord($child, $subOrder);

                    //cout sub order
                    $subOrder += 1;
                    //check error
                    if (!$res[0]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //this menthod fron insert main and sub only (support 2 level)
    public function updateMenu($menuDatas)
    {
        $output = [
            "success" => true,
            "message" => ""
        ];
        
        DB::beginTransaction();
        //delete all data
        $res = $this->deleteAllMenu();
        if ($res) {
            //insert new
            $res = $this->insertNewMenus($menuDatas);
            if (!$res) {
                $output["success"] = false;
                $output["message"] = $this->messages["update_error"];
                DB::rollBack();
            } else {
                DB::commit();
            }
            
        } else {
            $output["success"] = false;
            $output["message"] = $this->messages["update_error"];
            DB::rollBack();
        }
        return $output;
        
    }

    // private function getMaxId()
    // {
    //     return $this->menu->max('id');
    // }

    private function deleteAllMenu()
    {
        $menus = $this->menu->get();

        foreach ($menus as $menu) {
            if (!$menu->delete()) {
                return false;
            }
        }
        return true;
    }
}