
function defaultAdminOption(selection)
{
    //default option Select Admin..
    var newOption       = document.createElement("option");
    newOption.value     = "";
    newOption.innerHTML = "Select Admin..";
    selection.options.add(newOption);
}

function addUserToSelection(users)
{
    var selection = document.getElementById('user_id');
    selection.innerHTML = "";
    if (users.length > 0) {
        for (var i=0; i < users.length; i++) {
            var newOption       = document.createElement("option");
            newOption.value     = users[i]["id"];
            newOption.innerHTML = users[i]["username"];
            selection.options.add(newOption);
        }
    } else {
        defaultAdminOption(selection);
    }
    
}

function selectAppName(selected)
{
    var app_id = selected.value;
    var users = getUserFromApp(app_id);
    if (users) {
        //add new option
        addUserToSelection(users);
    }
}

function addContentModalBody(datas)
{
    var content = 'Waiting server ...';
    $modalBody = $('#checkModal').find('.modal-body');
    $modalBody.html(content);
}



