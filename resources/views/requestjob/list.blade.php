
@extends('listtemplate')


@section('list_name_page')
Request Job
@stop

@section('list_link_header')
                                    <li>
                                        <a href="{{ URL::to('dashboard/list') }}">
                                            Dashboard
                                        </a>
                                    </li>
                                    <li class="active">
                                        List
                                    </li>
@stop

@section('list_content')
                                        
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover" id="sample_2">
                                                <thead>
                                                    <tr>
                                                        <th>App</th>
                                                        <th>Job</th>
                                                        <th>People Name</th>
                                                        <th>People Card No</th>
                                                        <th> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if ($datas)
                                                    @foreach($datas as $data)
                                                        @if (isset($data['id']))
                                                            <tr id="list-{{$data['id']}}">
                                                                <td>{{ $data['app']['name'] or '' }}</td>
                                                                <td>{{ $data['job']['job_type']['name'] or '' }}</td>
                                                                <td>{{ $data['people']['firstname'] or '' }} {{ $data['people']['lastname'] or '' }}</td>
                                                                <td>{{ $data['people']['card_no'] or '' }}</td>
                                                                <td>
                                                                    <a onclick="printDoc({{ $data['id'] }});" class="btn btn-xs btn-blue tooltips" id="print-button-{{ $data['id'] }}" data-id="{{ $data['id'] }}" data-toggle="modal" data-original-title="Print"><i class="fa fa-print"></i></a>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
@stop

@section('list_js_script')

    function printFile(documents) {
        for (var i=0; i < documents.length; i++) {
            var document = documents[i];
            
            var w = window.open(document["file"]);
            //w.document.write(document["file"]);
            if (navigator.appName == 'Microsoft Internet Explorer') window.print();
            else w.print();
        }
    }

    function printDoc(id) {
        $.ajax({
            type:'POST',
            url: "{{ URL::to('reqjob/print') }}",
            data:{ _token: '{{ csrf_token() }}', 'id':id },
            async: false,
            dataType: "json",
            success:function(result){
                var documents = result["documents"]||[];
                //call function for print
                printFile(documents);
            },
            error: function(){

            }
        });
    }

@stop

@section('list_jquery')

@stop