@extends('formtemplate')

@section('css_for_form')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/datepicker/css/datepicker.css') }}">
@stop

@section('form_name_page')
    App
@endsection

@section('form_link_header')
    <li>
        <a href="{{ URL::to('app/list') }}">
            List
        </a>
    </li>
    <li class="active">
        App
    </li>
@stop


@section('form_content')
    <!-- <h3><i class="fa fa-pencil-square"></i> APP</h3> -->
    <!-- <hr> -->

        <form id="form" action="{{ URL::to('app/app') }}" method="POST" role="form" >
            <input type="hidden" id="id" name="id" value="{{ $data['id'] or '' }}" />
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <div class="row">
                <div class="col-md-12">
                    <div class="errorHandler alert alert-danger no-display">
                        <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                    </div>
                    <div class="successHandler alert alert-success no-display">
                        <i class="fa fa-ok"></i> Your form validation is successful!
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">
                            App name <span class="symbol required"></span>
                        </label>
                        <input type="text" placeholder="Insert app name" class="form-control" id="name" name="name" value="{{ $data['name'] or ''}}" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Description 
                        </label>
                        <textarea type="text" class="form-control" id="description" name="description" >{{ $data['description'] or ''}}</textarea>
                    </div>
                    
                    <!-- TODO : Add more field such as location-->
                    <div class="form-group">
                        <label class="control-label">
                            Latitude <span class="symbol required"></span>
                        </label>
                        <input type="text" class="form-control" id="latitude" name="latitude" value="{{ $data['latitude'] or ''}}" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">
                            Longitude <span class="symbol required"></span>
                        </label>
                        <input type="text" class="form-control" id="longtitude" name="longtitude" value="{{ $data['longtitude'] or ''}}" required>
                    </div>
                </div>

            </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <span class="symbol required"></span>Required Fields
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">

            </div>
             <div class="col-md-4">
                <button class="btn btn-primary btn-block" type="submit">
                    Save <i class="fa fa-arrow-circle-right"></i>
                </button>
            </div>

        </div>
    </form>

@endsection

@section('js_req_for_form')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('additional_js/app-validation.js') }}"></script>
@stop

@section('form_js_script')
    function init()
    {
        $('.date-picker').datepicker({
            autoclose: true
        });
        
    }
@stop

@section('form_jquery')
    FormValidator.init();
    init();
@stop

