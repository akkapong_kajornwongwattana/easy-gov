<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_type';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];


}
