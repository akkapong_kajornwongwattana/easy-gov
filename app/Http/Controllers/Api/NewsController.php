<?php namespace App\Http\Controllers\Api;

use App\Repositories\NewsRepository;
use App\Http\Controllers\ApiController;
use App;
use Input;

class NewsController extends ApiController {

    protected $newsRepo;
    protected $myValidate;
    protected $myResponse;


    function __construct(NewsRepository $newsRepo)
    {
        parent::__construct();

        $this->newsRepo   = $newsRepo;
        $this->myValidate = App::make('App\Services\MyValidate');
        $this->myResponse = App::make('App\Services\MyResponse');
    }

    //Method for create coddition
    private function createCondition($inputs)
    {
        //define outputs
        $keys       = [];
        $operators  = [];
        $parameters = [];
        //define allow condition 
        $allows = ["id", "app_id", "title"];
        foreach ($inputs as $key => $value) {
            //check allow
            if (in_array($key, $allows)) {
                $keys[] = $key;
                if ($key == "title") {
                    $operators[]      = "LIKE";
                    $parameters[$key] = "%".$value."%";
                } else {
                    $operators[]      = "=";
                    $parameters[$key] = $value;
                }
                
            }   
        }
        return [$keys, $operators, $parameters];
    }

    //Method for get news
    public function getNews()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [

        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for create condition for query data
        $conds = $this->createCondition($input);
        //--- Process Section ---//
        $result = $this->newsRepo->getNews($conds[0], $conds[1], $conds[2]);

        //--- Output Section ---//
        return $this->myResponse->output($result);
    }

   
}