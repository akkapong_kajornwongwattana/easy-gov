<?php

return array(
    /*
    |--------------------------------------------------------------------------
    | Status Code & Text
    |--------------------------------------------------------------------------
    | status code & text for response
    |
    */
        'code200' => 200,
        'text200' => 'OK',

        'code204' => 204,
        'text204' => 'No Content',

        'code290' => 290,
        'text290' => 'Request Fail',

        'code291' => 291,
        'text291' => 'Content Incomplete',
);