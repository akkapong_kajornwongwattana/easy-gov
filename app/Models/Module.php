<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserAcl;


class Module extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table   = 'module';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['module_name', 'controller_name', 'has_create', 'has_read', 'has_update', 'has_delete', 'menu_id'];


    // /**
    //  * Relation between module and user_acl.
    //  *
    //  * @return Collection
    //  */
    public function user_acl()
    {
        return $this->hasOne('App\Models\UserAcl');
    }
    
    // /**
    //  * Relation between module and menu.
    //  *
    //  * @return Collection
    //  */
    public function menu()
    {
        return $this->hasOne('App\Models\Menu', 'id', 'menu_id');
    }
}
