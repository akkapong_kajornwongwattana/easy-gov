<?php

namespace App\Services;

use Config;
use Request;
// use EggLog;

class MyResponse
{
    public function getOutput($format="array", $params)
    {
        switch ($format) {
            case "array":
                $this->setArrayFormat($params);
                break;
            case "json":
                $this->setJsonFormat($params);
                break;
            default:
                $this->setArrayFormat($params);
        }
    }

    protected function setJsonFormat($params)
    {
        echo json_encode($params);
    }

    protected function setArrayFormat($params)
    {
        return $params;
    }

    protected function authentication()
    {
        return true;
    }

    public function output($data, $code = null)
    {
        if ( ! empty($code)) {
            $statusCode = Config::get('status.code'.$code);
            $statusText = Config::get('status.text'.$code);
        } else {
            $statusCode = Config::get('status.code200');
            $statusText = Config::get('status.text200');
        }

        $output = array(
            'status' => array(
                'code' => $statusCode,
                'text' => $statusText,
            ),
            'data' => $data,
        );

        // $this->logOutput($output, $statusText, $statusCode);

        return $output;
    }

    public function outputForGet($data, $params, $code = null)
    {
        if ( ! is_null($code)) {
            $statusCode = \Config::get('status.code'.$code);
            $statusText = \Config::get('status.text'.$code);
        } else {
            $statusCode = \Config::get('status.code200');
            $statusText = \Config::get('status.text200');
        }

        if (isset($data['count'])) {
            $dataCount = $data['count'];
            unset($data['count']);
        }

        if (isset($data['getTotalRecord'])) {
            $getTotalRecord = $data['getTotalRecord'];
            unset($data['getTotalRecord']);
        }

        $output = array(
            'status' => array(
                'code' => $statusCode,
                'text' => $statusText,
            ),
            'data'        => $data,
            'result_page' => array(
                'offset'        => $params['offset'],
                'limit'         => $params['limit'],
                'total_result'  => isset($dataCount) ? (string)$dataCount : '0',
                'summary_count' => isset($getTotalRecord) ? (string)$getTotalRecord : '0',
            ),
        );

        // $this->logOutput($output, $statusText, $statusCode);

        return $output;
    }

    public function outputForDataTable($data)
    {
        return response($data)->header('Content-Type', 'application/json; charset=UTF-8');
    }
}