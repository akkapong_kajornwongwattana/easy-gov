<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Uplaod Form</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    
</head>
<body>

    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        
        jQuery(document).ready(function() {
            //TODO
            //get document for the job
            $.ajax({
                type: 'post',
                data: {_method: 'delete', _token :'{{{ csrf_token() }}}', 'id': id},
                url: "{{ URL::to('app/delete') }}",
                async: false,
                dataType: "json",
                success:function(result){

                     $('.modal-footer').html('<button type="button" data-dismiss="modal" class="btn green" id="okay">OK</button>');

                    if(result.status==290){
                        $('.modal-body').html(result.message);
                    } else {

                        $('.modal-body').html("Delete Complete!");
                        $('#sample_2').find('#list-'+id).remove();
                        $('#smallModal').modal('hide');
                        table.row('#list-'+id).remove().draw( false );
                        //reload page
                        //location.reload();
                    }
                }
            });

        });
    </script>
</body>
</html>
