
@extends('formtemplate')

@section('form_name_page')
User
@stop

@section('form_link_header')
                                    <li>
                                        <a href="{{ URL::to('user/list') }}">
                                            List
                                        </a>
                                    </li>
                                    <li class="active">
                                        User
                                    </li>
@stop

@section('form_content')
                                        <!-- <h1><i class="fa fa-pencil-square"></i> USER</h1>
                                        <hr> -->
                                        <form id="form" action="{{ URL::to('user/user') }}" method="POST" role="form" >
                                            <input type="hidden" id="id" name="id" value="{{$data['id'] or ''}}" />
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="errorHandler alert alert-danger no-display">
                                                        <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                    </div>
                                                    <div class="successHandler alert alert-success no-display">
                                                        <i class="fa fa-ok"></i> Your form validation is successful!
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Username <span class="symbol required"></span>
                                                        </label>
                                                        <input type="text" placeholder="Insert username" class="form-control" id="username" name="username" value="{{$data['username'] or ''}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            User type <span class="symbol required"></span>
                                                        </label>
                                                        <select name="user_type" class="form-control" required>
                                                            <option value="">Select User Type..</option>
                                                            <option value="super"
                                                            @if ((isset($data['user_type']))&&($data['user_type'] == "super"))
                                                                selected
                                                            @endif>Super Admin</option>
                                                            <option value="admin"
                                                            @if ((isset($data['user_type']))&&($data['user_type'] == "admin"))
                                                                selected
                                                            @endif>Admin</option>
                                                        </select>
                                                    </div>
                                                    <br />
                                                    <!-- Password Section -->
                                                    @if (!empty($data['id']))
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Old Password <span class="symbol required"></span>
                                                        </label>
                                                        <input type="password" placeholder="Insert Old Password" class="form-control" id="oldpassword" name="oldpassword" value="">
                                                    </div>
                                                    <br />
                                                    @endif
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                        @if(!empty($data['id']))
                                                            New Password
                                                        @else
                                                            Password
                                                        @endif<span class="symbol required"></span>
                                                        </label>
                                                        <input type="password" placeholder=
                                                        @if(!empty($data['id']))
                                                            "Insert New Password"
                                                        @else
                                                            "Insert Password"
                                                        @endif class="form-control" id="password" name="password" value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Confirm Password <span class="symbol required"></span>
                                                        </label>
                                                        <input type="password" placeholder="Insert Confirm Password" class="form-control" id="confirmpassword" name="confirmpassword" value="">
                                                    </div>
                                                    <br />
                                                    <!-- Password Section -->
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            App <span class="symbol"></span>
                                                        </label>
                                                        <select name="app_id" class="form-control">
                                                            <option value="">Select Application..</option>
                                                        @if (isset($data['apps']))
                                                            @foreach ($data['apps'] as $eachapp)
                                                            <option value="{{ $eachapp['id'] }}"
                                                            @if ((isset($data['app_id']))&&($data['app_id'] == $eachapp['id']))
                                                                selected
                                                            @endif
                                                            >{{$eachapp['name']}}</option>
                                                            @endforeach
                                                        @endif
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <span class="symbol required"></span>Required Fields
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">

                                                </div>
                                                 <div class="col-md-4">
                                                    <button id="save-btn" class="btn btn-primary btn-block" type="submit">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </form>
@stop

@section('js_req_for_form')
<script src="{{ URL::asset('additional_js/user-validation.js') }}"></script>
@stop

@section('form_jquery')
        FormValidator.init();
@stop

@section('form_jquery')
@stop
