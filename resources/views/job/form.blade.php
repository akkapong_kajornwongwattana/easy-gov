
@extends('formtemplate')

@section('form_name_page')
Job
@stop

@section('css_for_form')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/select2/select2.css') }}">
@stop

@section('form_link_header')
                                    <li>
                                        <a href="{{ URL::to('job/list') }}">
                                            List
                                        </a>
                                    </li>
                                    <li class="active">
                                        Job
                                    </li>
@stop

@section('form_content')
                                        <h1><i class="fa fa-pencil-square"></i> JOB </h1>
                                        <hr>
                                        <form id="form" action="{{ URL::to('job/job') }}" method="POST" role="form" enctype="multipart/form-data">
                                            <input type="hidden" id="id" name="id" value="{{$data['id'] or ''}}" />
                                            <input type="hidden" id="documenttype_ids" name="documenttype_ids" value="" />
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="errorHandler alert alert-danger no-display">
                                                        <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                    </div>
                                                    <div class="successHandler alert alert-success no-display">
                                                        <i class="fa fa-ok"></i> Your form validation is successful!
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <!-- APP (office) SECTION -->
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Application Name <span class="symbol required"></span>
                                                        </label>
                                                        <select id="app_id" name="app_id" class="form-control">
                                                        @if (isset($data['apps']))
                                                            @foreach ($data['apps'] as $app)
                                                            <option value="{{ $app['id'] }}"
                                                            @if ((isset($data['app_id']))&&($data['app_id'] == $app['id']))
                                                                selected
                                                            @endif
                                                            >{{$app['name']}}</option>
                                                            @endforeach
                                                        @endif
                                                        </select>
                                                    </div>
                                                    <!-- JOB TYPR SECTION -->
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Job type <span class="symbol required"></span>
                                                        </label>
                                                        <select id="job_type_id" name="job_type_id" class="form-control">
                                                        @if (isset($data['job_types']))
                                                            @foreach ($data['job_types'] as $job_type)
                                                            <option value="{{ $job_type['id'] }}"
                                                            @if ((isset($data['job_type_id']))&&($data['job_type_id'] == $job_type['id']))
                                                                selected
                                                            @endif
                                                            >{{$job_type['name']}}</option>
                                                            @endforeach
                                                        @endif
                                                        </select>
                                                    </div>
                                                    <!-- POSITION DETAIL SECTION -->
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Position detail
                                                        </label>
                                                        <textarea id="position_detail" name="position_detail" class="ckeditor form-control" cols="10" rows="10">{{ $data['position_detail'] or ''}}</textarea>
                                                    </div>

                                                    <hr>
                                                    <!-- REQUIRED DOCUMENT SECTION -->
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Required document
                                                        </label>
                                                        <div class="row">
                                                            <div class="col-md-12 space20">
                                                                <button class="btn btn-green add-row" type="button" onclick="addDocument();">
                                                                    Add Document <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-hover" id="document-table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Document</th>
                                                                        <th> </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if (isset($data['documenttypes']))
                                                                    @foreach($data['documenttypes'] as $document)
                                                                        @if (isset($document['id']))
                                                                            <tr id="list-{{$document['id']}}">
                                                                                <td>{{ $document['name'] or '' }}</td>
                                                                                <td>
                                                                                    <a onclick="deleteDocument({{ $document['id'] }});" title="Edit" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-times fa fa-white"></i></a>
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <span class="symbol required"></span>Required Fields
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">

                                                </div>
                                                 <div class="col-md-4">
                                                    <button id="save-btn" class="btn btn-primary btn-block" type="button" onclick="saveData()">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </form>


<div class="modal fade" id="required-documnet" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Select document</h4>
       </div>
        <div class="modal-body">
            <select id="select-document"  class="form-control search-select">
                <option value=""></option>
            @if (isset($data["documents"]))
                @foreach ($data["documents"] as $documents)
                <option value="{{ $documents['id'] }}">{{$documents['name']}}</option>
                @endforeach
            @endif
            </select>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-primary" type="button" id="okay">OK</button>
        </div>
    </div>
  </div>
</div>

@stop

@section('js_req_for_form')
<script src="{{ URL::asset('assets/plugins/select2/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('additional_js/job-validation.js') }}"></script>
@stop

@section('form_jquery')
        FormValidator.init();

        $(".search-select").select2({
            placeholder: "Select document",
            allowClear: false
        });

        init();
@stop

@section('form_js_script')
    //define golbal 
    var documentIds = [];


    $('#required-documnet').find("#okay").click(function() {
        //get selected id
        var selcetedDocId = $('#select-document').val();
        //check data not in documentIds
        if (checkDataInArry(selcetedDocId)){
            //add to table 
            addDocumentToTable(selcetedDocId);
            //and documentIds
            documentIds.push(selcetedDocId);
        }
        
        //close model
        $('#required-documnet').modal('hide');
    });

    function init() {
        //set document Id to documentIds
        var datas = <?php echo json_encode($data); ?>;

        var documentTypes = datas["documenttypes"]||[];

        for (var i=0; i < documentTypes.length; i++) {
            documentIds.push(parseInt(documentTypes[i]["id"]));
        }

    }

    function getDocumentData(id) {
        var datas = <?php echo json_encode($data); ?>;

        var documents = datas["documents"];

        for (var i=0; i < documents.length; i ++) {
            var document = documents[i];
            if (document["id"] == id) {
                return document;
            }
        }
        return null;
    }

    function addDocumentToTable(selcetedDocId) {
        //get document data
        var document = getDocumentData(selcetedDocId);
        //create new row
        var documentRow = '<tr id="list-'+selcetedDocId+'" >' +
                                '<td>'+document["name"]+'</td>' +
                                    '<td>' +
                                        '<a onclick="deleteDocument('+selcetedDocId+');" title="Edit" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-times fa fa-white"></i></a>' +
                                    '</td>' +
                                '</tr>';

        documentRow = $.parseHTML(documentRow);
        //add to table 
        $('#document-table').append(documentRow);
    }

    function checkDataInArry(checkVal) {
        if (documentIds.indexOf(checkVal) == -1 ) {
            return true;
        }
        return false;
    }

    function removeDataFromArray(id) {
        // Find and remove item from an array
        var i = documentIds.indexOf(id);
        if(i != -1) {
            documentIds.splice(i, 1);
        }
    }

    function addDocument() {
        //show modal
        $('#required-documnet').modal('show');
    }

    function deleteDocument(id) {
        //delete data from documentIds
        removeDataFromArray(id);
        //delete data from table
        $('#document-table').find('#list-'+id).remove();
    }

    function saveData()
    {
        //add data to documenttype_ids
        $('#documenttype_ids').val(documentIds);
        //Submit
        $('#save-btn').submit();
    }
@stop