<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAclTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_acl', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('module_id');
			$table->integer('user_id');
			$table->tinyInteger('can_create');
			$table->tinyInteger('can_read');
			$table->tinyInteger('can_update');
			$table->tinyInteger('can_delete');
			$table->tinyInteger('side_menu');
		});

		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_acl');
	}

}
