<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');
		$this->call('UserAclTableSeeder');
		$this->call('ModuleTableSeeder');
		$this->call('MenuTableSeeder');
		$this->call('AppTableSeeder');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        // Insert default data
	    DB::table('user')->insert(
	        [
				'user_type' => 'super',
				'username'  => 'Superadmin',
				'password'  => 'e10adc3949ba59abbe56e057f20f883e'
	        ]
	    );
    }

}

class UserAclTableSeeder extends Seeder {

    public function run()
    {
        /// Insert default data
	    DB::table('user_acl')->insert([
	        [	//MENU
				'module_id'  => 1,
				'user_id'    => 1,
				'can_create' => 1,
				'can_read'   => 1,
				'can_update' => 1,
				'can_delete' => 1,
				'side_menu'  => 1,
	        ],[ //DASHBOARD
				'module_id'  => 2,
				'user_id'    => 1,
				'can_create' => 0,
				'can_read'   => 1,
				'can_update' => 0,
				'can_delete' => 0,
				'side_menu'  => 1,
	        ],[ //MODULE
				'module_id'  => 3,
				'user_id'    => 1,
				'can_create' => 1,
				'can_read'   => 1,
				'can_update' => 1,
				'can_delete' => 1,
				'side_menu'  => 1,
	        ],[ //USER
				'module_id'  => 4,
				'user_id'    => 1,
				'can_create' => 1,
				'can_read'   => 1,
				'can_update' => 1,
				'can_delete' => 1,
				'side_menu'  => 1,
	        ],[ //APP
				'module_id'  => 5,
				'user_id'    => 1,
				'can_create' => 1,
				'can_read'   => 1,
				'can_update' => 1,
				'can_delete' => 1,
				'side_menu'  => 1,
	        ]
	    ]);
    }

}

class ModuleTableSeeder extends Seeder {

    public function run()
    {
        // Insert default data
	    DB::table('module')->insert([
	    	        [
	    				'module_name' => 'MenuController',
	    				'description' => 'Manage menu list',
	    				'has_create'  => 1,
	    				'has_read'    => 1,
	    				'has_update'  => 1,
	    				'has_delete'  => 1,
	    				'menu_id'     => 1,
	    	        ],[
	    				'module_name' => 'DashboardController',
	    				'description' => 'Display dashboard',
	    				'has_create'  => 0,
	    				'has_read'    => 1,
	    				'has_update'  => 0,
	    				'has_delete'  => 0,
	    				'menu_id'     => 2,
	    	        ],[
	    				'module_name' => 'ModuleController',
	    				'description' => 'Manage Module in this application',
	    				'has_create'  => 1,
	    				'has_read'    => 1,
	    				'has_update'  => 1,
	    				'has_delete'  => 1,
	    				'menu_id'     => 3,
	    	        ],[
	    				'module_name' => 'UserController',
	    				'description' => 'Manage user',
	    				'has_create'  => 1,
	    				'has_read'    => 1,
	    				'has_update'  => 1,
	    				'has_delete'  => 1,
	    				'menu_id'     => 4,
	    	        ],[
	    				'module_name' => 'AppController',
	    				'description' => 'Manage Application',
	    				'has_create'  => 1,
	    				'has_read'    => 1,
	    				'has_update'  => 1,
	    				'has_delete'  => 1,
	    				'menu_id'     => 5,
	    	        ]
	    	    ]);
    }

}

class MenuTableSeeder extends Seeder {

    public function run()
    {
        // Insert default data
	    DB::table('menu')->insert([
	    	        [
	    				'menu_display_text' => 'Menu',
	    				'parent_id'         => 0,
	    				'url'               => 'menu/list',
	    				'order'             => 1
	    	        ],[
	    				'menu_display_text' => 'Dashboard',
	    				'parent_id'         => 0,
	    				'url'               => 'dashboard/list',
	    				'order'             => 2
	    	        ],[
	    				'menu_display_text' => 'Module',
	    				'parent_id'         => 0,
	    				'url'               => 'module/list',
	    				'order'             => 3
	    	        ],[
	    				'menu_display_text' => 'User',
	    				'parent_id'         => 0,
	    				'url'               => 'user/list',
	    				'order'             => 4
	    	        ],[
	    				'menu_display_text' => 'Application',
	    				'parent_id'         => 0,
	    				'url'               => 'app/list',
	    				'order'             => 5
	    	        ]
	    	    ]);
    }

}

class AppTableSeeder extends Seeder {

    public function run()
    {
        // Insert default data
	    DB::table('app')->insert([
	    	        [
	    				'name'               => 'Template',
	    				'company_name'       => 'Eggdigital',
	    				'company_type'       => 'Media & Content',
	    				'contact_name'       => '',
	    				'contact_email'      => '',
	    				'contact_phone'      => '029999999',
	    				'status'             => 1,
	    	        ]
	    	    ]);
    }

}
