
@extends('formtemplate')

@section('form_name_page')
Permission
@stop

@section('form_link_header')
                                    <li>
                                        <a href="{{ URL::to('permission/list?user_id='.$data['user_id']) }}">
                                            List
                                        </a>
                                    </li>
                                    <li class="active">
                                        User
                                    </li>
@stop

@section('form_content')
                                        <!-- <h1><i class="fa fa-pencil-square"></i> USER</h1>
                                        <hr> -->
                                        <form id="form" action="{{ URL::to('permission/permission?user_id='.$data['user_id']) }}" method="POST" role="form" >
                                            <input type="hidden" id="id" name="id" value="{{$data['id'] or ''}}" />
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="errorHandler alert alert-danger no-display">
                                                        <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                    </div>
                                                    <div class="successHandler alert alert-success no-display">
                                                        <i class="fa fa-ok"></i> Your form validation is successful!
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Module Name <span class="symbol required"></span>
                                                        </label>
                                                        <select name="module_id" onchange="selectModuleName(this)" class="form-control" required>
                                                            <option value="">Select Module Name..</option>
                                                        @if (isset($data["modules"]))
                                                            @foreach ($data["modules"] as $module)
                                                            <option value="{{ $module['id'] }}"
                                                            @if ((isset($data['module_id']))&&($data['module_id'] == $module['id']))
                                                                selected
                                                            @endif
                                                            >{{$module['module_name']}}</option>
                                                            @endforeach
                                                        @endif
                                                        </select>
                                                    </div>
                                                    <br />
                                                    <!-- Permission Section -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Can Create <span class="symbol"></span>
                                                            </label>
                                                            <input id="can_create" type="checkbox" class="form-control flat-teal" name="can_create" value="true"
                                                            @if ((isset($data['can_create'])) && ($data['can_create']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Can Read <span class="symbol"></span>
                                                            </label>
                                                            <input id="can_read" type="checkbox" class="form-control flat-teal" name="can_read" value="true"
                                                            @if ((isset($data['can_read'])) && ($data['can_read']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Can Update <span class="symbol"></span>
                                                            </label>
                                                            <input id="can_update" type="checkbox" class="form-control flat-teal" name="can_update" value="true" 
                                                            @if ((isset($data['can_update'])) && ($data['can_update']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Can Delete <span class="symbol"></span>
                                                            </label>
                                                            <input id="can_delete" type="checkbox" class="form-control flat-teal" name="can_delete" value="true" 
                                                            @if ((isset($data['can_delete'])) && ($data['can_delete']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label font-bold">
                                                                Side Menu <span class="symbol"></span>
                                                            </label>
                                                            <input id="side_menu" type="checkbox" class="form-control flat-teal" name="side_menu" value="true" 
                                                            @if ((isset($data['side_menu'])) && ($data['side_menu']))
                                                                checked="checked"
                                                            @endif>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <span class="symbol required"></span>Required Fields
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">

                                                </div>
                                                 <div class="col-md-4">
                                                    <button id="save-btn" class="btn btn-primary btn-block" type="submit">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </form>                    
@stop

@section('js_req_for_form')
<script src="{{ URL::asset('additional_js/user-validation.js') }}"></script>
@stop

@section('form_js_script')

    function getModulePermission(module_id)
    {
        var modules = <?php echo json_encode($data['modules']) ?>;
        for (var i=0;i < modules.length;i++) {
            var module = modules[i];
            if (module["id"] == module_id) {
                return module;
            }
        }
        return null;
    }

    function disableUserPermission(hasAction, checkboxId) {
        
        var checkbox = $("#"+checkboxId);
        if (hasAction != 0) {
            //ENABLE
            checkbox.removeAttr("disabled");
            //REMOVE STYLE
            checkbox.parents(".icheckbox_flat-aero").prev().removeAttr("style");

        } else {
            var c
            //UNCHECK
            checkbox.parents(".icheckbox_flat-aero").removeClass("checked");
            //DISABLE
            checkbox.attr("disabled", true);
            //ADD STYLE TO LABLE
            checkbox.parents(".icheckbox_flat-aero").prev().css({"color":"#D3D3D3"}); //gray

        }
    }
    
    function selectModuleName(selected)
    {
        var module_id = selected.value;
        var modulePermission = getModulePermission(module_id);
        if (modulePermission) {
            //create 
            disableUserPermission(modulePermission["has_create"], "can_create");
            //read 
            disableUserPermission(modulePermission["has_read"], "can_read");
            //update 
            disableUserPermission(modulePermission["has_update"], "can_update");
            //delete 
            disableUserPermission(modulePermission["has_delete"], "can_delete");
        }
    }

@stop

@section('form_jquery')
        FormValidator.init();
@stop

