<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\PeopleDocument;
use App;
use DB;
use Config;

class PeopleDocumentRepository {

    protected $peopleDoc;
    protected $myService;
    protected $cdnServices;
    protected $messages;

    private $fieldNameList = ["people_id", "documenttype_id"];
    

    public function __construct(PeopleDocument $peopleDoc)
    {
        $this->peopleDoc   = $peopleDoc;
        
        $this->myService   = App::make('App\Services\MyServices');
        $this->cdnServices = App::make('App\Services\CdnServices');
        
        //get message
        $this->messages    = Config::get('message');
    }

    //Method for get full path of document
    private function getFullDocumentPath($datas)
    {
        //define outputs
        $outputs = [];

        if ($datas) {
            $base = $this->cdnServices->getUploadPath();
            foreach ($datas as $data) {
                $data["file"] = $base.$data["people_id"]."/".$data["file"];
                $outputs[]    = $data;
            }
        }
        return $outputs;
    }

    //For get document type from condition 
    public function getPeopleDocument($keys, $operators, $params)
    {
        //create param for add condition
        $conditions          = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $peopleDocCollection = $this->myService->addConditiontoCollection($conditions, $this->peopleDoc);
        $peopleDocCollection = $peopleDocCollection->with('document_type');
        $output              = $peopleDocCollection->get()->toArray();
        
        $output              = $this->getFullDocumentPath($output);
        
        return $output;
    }

    //check duplicate module
    private function duplicateDocumentType($param)
    {
        //define parameters
        $keys               = ["people_id", "documenttype_id"];
        $operators          = ["=", "="];
        
        $isDuplicate        = $this->myService->checkDuplicate($this->peopleDoc, $keys, $operators, $param);
        
        return $isDuplicate;
    }

    //Method for upload file to server
    private function uploadFileToServer($param, $folder="")
    {
        $url = $this->cdnServices->manageUpload($param, "file", "old_file", $folder);

        return $url;
    }

    public function uploadPeopleDocument($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicateDocumentType($param);

        if (!$isDups[0]) {

            // add data to database
            $peopleDoc                  = new PeopleDocument;
            // add data to collection object
            $peopleDoc->people_id       = $param["people_id"];
            $peopleDoc->documenttype_id = $param["documenttype_id"];
            $peopleDoc->file            = $this->uploadFileToServer($param, $param["people_id"]);
            
            if (!$peopleDoc->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            } else {
                $result["file"] = $peopleDoc->file;
            }

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;
    }

    //Method for change document
    public function changeDocumentFile($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        if (!empty($param["id"])) {
            //get record
            $peopleDoc       = $this->peopleDoc->find($param['id']);
            
            $peopleDoc->file = $this->uploadFileToServer($param, $peopleDoc->people_id);

            if (!$peopleDoc->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["update_error"];
            } else {
                $result["file"] = $peopleDoc->file;
            }
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
    }

    //Method for create condition
    public function createCondition($params)
    {
        $keys       = [];
        $opereators = [];
        $parameters = [];

        if ($params) {
            foreach ($params as $key => $value) {
                
                if (in_array($key, $this->fieldNameList)) {
                    $keys[]           = $key;
                    $opereators[]     = "=";
                    $parameters[$key] = $value;
                }
            }
        }

        return [$keys, $opereators, $parameters];
    }

    //Method for get server path of document
    private function getServerDocumentPath($datas)
    {
        //define outputs
        $outputs = [];

        if ($datas) {
            $base = public_path().'/uploads/';
            foreach ($datas as $data) {
                $data["file"] = $base.$data["people_id"]."/".$data["file"];
                $outputs[]    = $data;
            }
        }
        return $outputs;
    }

    //Method for get document from id list
    public function getDocumentFromIdList($peopleId, $documentIds)
    {
        $documents = $this->peopleDoc->where("people_id", $peopleId)->whereIn("documenttype_id", $documentIds)->get()->toArray();
        return $this->getFullDocumentPath($documents);
    }
}