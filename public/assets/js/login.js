var Login = function() {
    "use strict";
    var handleLogin = function() {
        // consloe.log('handleLogin');
        var sendLogin = function(){
                var message;
                var email    = $('#email').val();
                var password = $('#password').val();

                // consloe.log('email: ', email);
                // consloe.log('password: ', password);

                $.ajax({
                    type: 'POST',
                    // url: App.getBaseUrl()+'/user/checklogin',
                    url: App.getBaseUrl()+'/loginpage',
                    data: {
                        email : email,
                        password : password
                    } ,

                    dataType: 'json',
                    cache: false,
                    success: function(response){
                        // consloe.log('success: ', response);
                        if(response.status != ''){

                            if(response.status == 200) {
                                window.location=App.getBaseUrl()+'/phonebook';
                            } else {

                                message = response.message;
                                App.messageCallback('.alert-error','#login-form',message);
                            }

                        } else {

                            message = response.message;
                            App.messageCallback('.alert-error','#login-form',message);
                        }

                    },
                    error: function(response){
                        // consloe.log('error: ', response);
                    }

                });

            }

        $('#login-form').validate({
                errorElement: 'label', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    email: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                },

                messages: {
                    email: {
                        required: "email is required1."
                    },
                    password: {
                        required: "Password is required2."
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    // consloe.log('invalidHandler');
                    $('.alert-error', $('#login-form')).show();
                },

                highlight: function (element) { // hightlight error inputs
                    // consloe.log('highlight');
                    $(element)
                        .closest('.control-group').addClass('error'); // set error class to the control group
                },

                success: function (label) {
                    // consloe.log('success');
                    label.closest('.control-group').removeClass('error');
                    label.remove();
                },

                errorPlacement: function (error, element) {
                    // consloe.log('errorPlacement');
                    error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
                },

                submitHandler: function (form) {
                    // consloe.log('validate()->sendLogin()');
                    sendLogin();
                    //form.submit();
                }
            });

            $('#login-form input').keypress(function (e) {
                if (e.which == 13) {
                    if ($('#login-form').validate().form()) {
                        $('#login-form').submit();
                    }
                    return false;
                }
            });
    }

    var runBoxToShow = function() {
        var el = $('.box-login');
        if (getParameterByName('box').length) {
            switch(getParameterByName('box')) {
                case "register" :
                    el = $('.box-register');
                    break;
                case "forgot" :
                    el = $('.box-forgot');
                    break;
                default :
                    el = $('.box-login');
                    break;
            }
        }
        el.show().addClass("animated flipInX").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            $(this).removeClass("animated flipInX");
        });
    };
    var runLoginButtons = function() {
        $('.forgot').on('click', function() {
            $('.box-login').removeClass("animated flipInX").addClass("animated bounceOutRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).hide().removeClass("animated bounceOutRight");

            });
            $('.box-forgot').show().addClass("animated bounceInLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).show().removeClass("animated bounceInLeft");

            });
        });
        $('.register').on('click', function() {
            $('.box-login').removeClass("animated flipInX").addClass("animated bounceOutRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).hide().removeClass("animated bounceOutRight");

            });
            $('.box-register').show().addClass("animated bounceInLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).show().removeClass("animated bounceInLeft");

            });

        });
        $('.go-back').click(function() {
            var boxToShow;
            if ($('.box-register').is(":visible")) {
                boxToShow = $('.box-register');
            } else {
                boxToShow = $('.box-forgot');
            }
            boxToShow.addClass("animated bounceOutLeft").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                boxToShow.hide().removeClass("animated bounceOutLeft");

            });
            $('.box-login').show().addClass("animated bounceInRight").on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                $(this).show().removeClass("animated bounceInRight");

            });
        });
    };
    //function to return the querystring parameter with a given name.
    var getParameterByName = function(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    var runSetDefaultValidation = function() {
        $.validator.setDefaults({
            errorElement : "span", // contain the error msg in a small tag
            errorClass : 'help-block',
            errorPlacement : function(error, element) {// render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") {// for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                    error.appendTo($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore : ':hidden',
            success : function(label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error');
            },
            highlight : function(element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').addClass('has-error');
                // add the Bootstrap error class to the control group
            },
            unhighlight : function(element) {// revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            }
        });
    };
    var runLoginValidator = function() {
        var form = $('.form-login');
        var errorHandler = $('.errorHandler', form);
        form.validate({
            rules : {
                email : {
                    minlength : 2,
                    required : true
                },
                password : {
                    minlength : 6,
                    required : true
                }
            },
            submitHandler : function(form) {
                errorHandler.hide();
                form.submit();
            },
            invalidHandler : function(event, validator) {//display error alert on form submit
                errorHandler.show();
            }
        });
    };
    var runForgotValidator = function() {
        var form2 = $('.form-forgot');
        var errorHandler2 = $('.errorHandler', form2);
        form2.validate({
            rules : {
                email : {
                    required : true
                }
            },
            submitHandler : function(form) {
                errorHandler2.hide();
                form2.submit();
            },
            invalidHandler : function(event, validator) {//display error alert on form submit
                errorHandler2.show();
            }
        });
    };
    var runRegisterValidator = function() {
        var form3 = $('.form-register');
        var errorHandler3 = $('.errorHandler', form3);
        form3.validate({
            rules : {
                full_name : {
                    minlength : 2,
                    required : true
                },
                address : {
                    minlength : 2,
                    required : true
                },
                city : {
                    minlength : 2,
                    required : true
                },
                gender : {
                    required : true
                },
                email : {
                    required : true
                },
                password : {
                    minlength : 6,
                    required : true
                },
                password_again : {
                    required : true,
                    minlength : 5,
                    equalTo : "#password"
                },
                agree : {
                    minlength : 1,
                    required : true
                }
            },
            submitHandler : function(form) {
                errorHandler3.hide();
                form3.submit();
            },
            invalidHandler : function(event, validator) {//display error alert on form submit
                errorHandler3.show();
            }
        });
    };
    return {
        //main function to initiate template pages
        init : function() {
            runBoxToShow();
            runLoginButtons();
            runSetDefaultValidation();
            runLoginValidator();
            runForgotValidator();
            runRegisterValidator();
            handleLogin();
        }
    };
}();
