
@extends('formtemplate')

@section('form_name_page')
People
@stop


@section('css_for_form')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/datepicker/css/datepicker.css') }}">
@stop

@section('form_link_header')
                                    <li>
                                        <a href="{{ URL::to('people/list') }}">
                                            List
                                        </a>
                                    </li>
                                    <li class="active">
                                        Employee
                                    </li>
@stop

@section('form_content')
                                        <!-- <h1><i class="fa fa-pencil-square"></i> USER</h1>
                                        <hr> -->
                                        <form id="form" action="{{ URL::to('people/people') }}" method="POST" role="form" >
                                            <input type="hidden" id="id" name="id" value="{{$data['id'] or ''}}" />
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="errorHandler alert alert-danger no-display">
                                                        <i class="fa fa-times-sign"></i> You have some form errors. Please check below.
                                                    </div>
                                                    <div class="successHandler alert alert-success no-display">
                                                        <i class="fa fa-ok"></i> Your form validation is successful!
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Firstname <span class="symbol required"></span>
                                                        </label>
                                                        <input type="text" placeholder="Insert first name" class="form-control" id="firstname" name="firstname" value="{{$data['firstname'] or ''}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Lastname <span class="symbol required"></span>
                                                        </label>
                                                        <input type="text" placeholder="Insert last name" class="form-control" id="lastname" name="lastname" value="{{$data['lastname'] or ''}}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Birth date 
                                                        </label>
                                                        <div class="input-group col-md-4">
                                                            <input type="text" id="birthday" name="birthday" data-date-format="yyyy-mm-dd" data-date-viewmode="years" class="form-control date-picker" value="{{ $data['birthday'] or '' }}">
                                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Card no <span class="symbol required"></span>
                                                        </label>
                                                        <input type="text" placeholder="Insert card no" class="form-control" id="card_no" name="card_no" value="{{$data['card_no'] or ''}}">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Email<span class="symbol required"></span>
                                                        </label>
                                                        <input type="email" placeholder="Insert email" class="form-control" id="email" name="email" value="{{$data['email'] or ''}}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="control-label font-bold">
                                                            Phone no 
                                                        </label>
                                                        <input type="text" placeholder="Insert phone no" class="form-control" id="tel" name="tel" value="{{$data['tel'] or ''}}">
                                                    </div>

                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div>
                                                        <span class="symbol required"></span>Required Fields
                                                        <hr>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">

                                                </div>
                                                 <div class="col-md-4">
                                                    <button id="save-btn" class="btn btn-primary btn-block" type="submit">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </form>
@stop

@section('js_req_for_form')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('additional_js/people-validation.js') }}"></script>
@stop

@section('form_jquery')
        FormValidator.init();
        init();
@stop

@section('form_js_script')

    
@stop
