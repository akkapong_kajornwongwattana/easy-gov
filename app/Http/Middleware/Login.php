<?php namespace App\Http\Middleware;

use Closure;

class Login {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $login;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        if(! \Session::has('userId'))
        {
            return redirect()->guest('login/login');
        }
        return $next($request);
    }

}
