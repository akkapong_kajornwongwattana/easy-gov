@extends('listtemplate')

@section('list_name_page')
App
@endsection

@section('list_link_header')
                                    <li>
                                        <a href="{{ URL::to('/dashboard/list') }}">
                                            Dashboard
                                        </a>
                                    </li>
                                    <li class="active">
                                        List
                                    </li>
@stop

@section('list_content')

    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 space20">
                <!-- Add Button-->
                <div>
                    <a id="add-admin-btn" href="{{ URL::to('app/create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> Add App</a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>App name</th>
                        <th width="500px">Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
            @if ( isset($data) )
                @foreach ($data as  $key => $value)
                    <tr id="list-{{$value['id']}}">
                        <td>{{ $value['name'] }}</td>
                        <td>{{ $value['description'] }}</td>
                        <td class="center">
                            <a href="{{ URL::to('app/update/' . $value['id'] . '?id=') }}{{$value['id']}}" title="Edit" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                            <a onClick="deleteData({{ $value['id'] }});" class="btn btn-xs btn-red tooltips deleteButton" id="delete-button-{{ $value['id'] }}" data-id="{{ $value['id'] }}" data-toggle="modal"  data-original-title="Remove"><i class="fa fa-times fa fa-white"></i></a>
                        </td>

                    </tr>
                @endforeach
            @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('list_js_script')
    function deleteData(id){
            $('#smallModal').modal('show');

            $('#smallModal').find("#okay").click(function() {

                var table = $('#sample_2').DataTable();

                if(id!=0){
                    $.ajax({
                        type: 'post',
                        data: {_method: 'delete', _token :'{{{ csrf_token() }}}', 'id': id},
                        url: "{{ URL::to('app/delete') }}",
                        async: false,
                        dataType: "json",
                        success:function(result){

                             $('.modal-footer').html('<button type="button" data-dismiss="modal" class="btn green" id="okay">OK</button>');

                            if(result.status==290){
                                $('.modal-body').html(result.message);
                            } else {

                                $('.modal-body').html("Delete Complete!");
                                $('#sample_2').find('#list-'+id).remove();
                                $('#smallModal').modal('hide');
                                table.row('#list-'+id).remove().draw( false );
                                //reload page
                                //location.reload();
                            }
                        }
                    });
                }

            });

            $('#smallModal').on('hidden.bs.modal', function () {

                id = 0;
                $('.modal-body').html('Would you like to delete?');
                $('.modal-footer').html('<button data-dismiss="modal" class="btn btn-default" type="button">Close</button><button class="btn btn-primary" type="button" id="okay">OK</button>');
            });
        }
@stop
@section('list_jquery')
@stop

