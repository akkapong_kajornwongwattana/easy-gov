<?php namespace App\Http\Controllers;

use App;
use Cookie;

class HtmlController extends Controller {

    public function __construct()
    {
        //call base url
        session_start();
        parent::__construct();
        parent::setBaseUrl();

        // $this->userRepo = App::make('App\Repositories\UserRepository');

        //TODO log
        //$this->logWebIn();
    }

    //methos for prepare output parameter
    private function prepareParamOutput($param)
    {
        //add menu in params
        $menus             = $this->listMenu();
        $param["menus"]    = $menus;
        //get user name from cookie
        $param["username"] = Cookie::get("username");

        return $param;
    }

    protected function getOutputWithErrormsg($viewPage, $params, $message)
    {
        return view($viewPage,  $this->prepareParamOutput($params))->withErrormsg($message);
    }

    protected function getOutput($viewPage, $params)
    {
        if (!isset($params['error'])) {
            return view($viewPage,  $this->prepareParamOutput($params));
        } else {
            return view($viewPage,  $this->prepareParamOutput($params))->withErrormsg($params['error'][0]);
        }

    }

    /**
    * redirect to page input after update/add data to db
     *
     * @param  String $page , Object $result (from add/update)
     * @return redirect page
    */
    protected function redirectToPage($page, $pageError, $result) {
        if (!isset($result['error'])) {
            return redirect($page);
        } else {
            //show error page TODO
            return redirect($pageError)->withErrormsg($result['error'][0]);
        }
    }

    protected function changeToPage($page, $pageError, $result) {
        if (!isset($result['error'])) {
            return redirect($page);
        } else {
            //show error page TODO
            return $this->getOutput($pageError, $result);
        }
    }

    private function listMenu()
    {
        //create user repository
        $userRepo = App::make('App\Repositories\UserRepository');
        $menus    = $userRepo->getMenuData();
        return $menus;
        // return [];
    }

}
