<?php namespace App\Http\Controllers\Html\CRUD\Admin;

use App\Http\Controllers\Html\CRUD\AdminHtmlController;
use App\Repositories\RequestJobRepository;
use Config;
use App;
use Input;

class RequestJobController extends AdminHtmlController
{
    private $myValidate;
    private $myResponse;
    private $userAcl;
    private $reqJobRepo;

    private $module      = 'RequestJobController';
    private $moduleName  = 'request job';

    private $mapping = [
        "list"   => "requestjob.list",
    ];
    private $derimetorView     = ".";
    private $derimetorRedirect = "/";


    public function __construct(RequestJobRepository $reqJobRepo)
    {
        parent::__construct();

        $this->myValidate = App::make('App\Services\MyValidate');
        $this->myResponse = App::make('App\Services\MyResponse');
        
        $this->reqJobRepo = $reqJobRepo;
        
        //check permission
        $this->userAcl    = $this->canAccess($this->module);
    }

    private function createParamsCondition()
    {
        //create app repository
        $appRepo   = App::make('App\Repositories\AppRepository');
        //get appp id from user
        $app_id    = $appRepo->getAppIdFromUser();
        $keys      = [];
        $operators = [];
        $params    = [];
        if (!empty($app_id)) {
            $keys[]           = "app_id";
            $operators[]      = "=";
            $params["app_id"] = $app_id;
        }

        return [$keys, $operators, $params];
    }

    //------- Interface method --------//
    public function getList()
    {
        //check permission
        if (!$this->userAcl["read"]) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."list ".$this->moduleName);
        }

        //can use this method
        //create condition
        $conds   = $this->createParamsCondition();
        //get job List
        $reqJobs = $this->reqJobRepo->getRequestJob($conds[0], $conds[1], $conds[2], true);
        // dd($reqJobs);
        //return to view
        return $this->getOutput($this->mapping["list"], ["datas" => $reqJobs, "module_name" => $this->module]);
    }

    public function getCreate()
    {
    

    }

    public function getView($id)
    {


    }

    public function getUpdate($id)
    {
       

    }

    public function deleteDelete()
    {
        
    }
    //------ End Interface method ------//


    public function postPrint()
    {
        $inputs = Input::all();
        //TODO
        //get request job
        $result = $this->reqJobRepo->getPrintDocument($inputs["id"]);
        
        //return to view
        return $this->myResponse->getOutput('json', $result);
    }
}