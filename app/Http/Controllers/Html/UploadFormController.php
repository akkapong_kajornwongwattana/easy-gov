<?php namespace App\Http\Controllers\Html;

use App\Http\Controllers\HtmlController;
use App\Repositories\JobRepository;
use App;
use Input;

class UploadFormController extends HtmlController {

    protected $myValidate;
    protected $jobRepo;

    protected $redirectMap = [
        'login' => 'login/login',
        'main'  => 'dashboard/list'
    ];
    protected $mapping = [
        'login' => 'login'
    ];

    public function __construct(JobRepository $jobRepo)
    {
        parent::__construct();

        $this->myValidate = App::make('App\Services\MyValidate');
        $this->jobRepo    = $jobRepo;
    }

    public function getUploadform()
    {
        //Input must have app_id, people_id and job_type_id
        //get input
        $inputs = Input::all();

        $fields = $this->jobRepo->getUploadForm($inputs);

        return view("upload_form", ["fields" => $fields, "people_id" => $inputs["people_id"]]);
    }


}

?>