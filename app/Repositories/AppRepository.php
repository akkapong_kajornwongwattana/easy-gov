<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\AppModel;

use Config;
use App;

class AppRepository
{
    protected $appModel;
    protected $messages;
    protected $myService;
    protected $userRepo;
    protected $cdnServices;

    public function __construct(AppModel $appModel)
    {
        // parent::__construct();

        $this->appModel    = $appModel;
        $this->messages    = Config::get('message');

        $this->myService   = App::make('App\Services\MyServices');
        $this->userRepo    = App::make('App\Repositories\UserRepository');
        $this->cdnServices = App::make('App\Services\CdnServices');
    }

    public function getApp($keys = array(), $operators = array(), $params = array(), $is_array = true)
    {
        //create param for add condition
        $conditions    = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $appCollection = $this->myService->addConditiontoCollection($conditions, $this->appModel);
        if ( true === $is_array ) {
            $output = $appCollection->get()->toArray();
        } else {
            $output = $appCollection->get();
        }

        return $output;
    }

    public function getAppById($id)
    {
        return $this->appModel->find($id);
    }


    //check duplicate module
    private function duplicateApp($param)
    {
        //define parameters
        $keys               = ["name"];
        $operators          = ["="];
        
        $isDuplicate        = $this->myService->checkDuplicate($this->appModel, $keys, $operators, $param);
        
        return $isDuplicate;
    }

    private function checkDupForUpdate($param)
    {
        $isDuplicate = false;
        //check duplicate
        $duplicates = $this->duplicateApp($param);
        if ($duplicates[0]) {
            $tmp = $duplicates[1];
            //check self
            if ((int)$tmp["id"] != (int)$param["id"]) {
                $isDuplicate = true;
            }
        }
        return $isDuplicate;
    }



    public function addApp($params)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicateApp($params);

        if (!$isDups[0]) {

            // add data to database
            $app              = new AppModel;
            // add data to collection object
            $app->name        = $params['name'];
            $app->description = $params['description'];
            $app->latitude    = $params['latitude'];
            $app->longtitude  = $params['longtitude'];
            
            if (!$app->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            }

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;

    }

    public function updateApp($params)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];

        if (!empty($params["id"])) {
            //get record
            $app      = $this->appModel->find($params['id']);
            
            //check duplicate
            $isDuplicate = $this->checkDupForUpdate($params);

            if (!$isDuplicate){

                //update data
                $app->name        = $params['name'];
                $app->description = $params['description'];
                $app->latitude    = $params['latitude'];
                $app->longtitude  = $params['longtitude'];
            

                if (!$app->save()) {
                    $result["success"] = false;
                    $result["message"] = $this->messages["update_error"];
                }

                
            } else {
                $result["success"] = false;
                $result["message"] = $this->messages["duplicate"];
            }
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
        
    }

    public function deleteApp($id)
    {
        $output = [
            "status"  => 200,
            "message" => ""
        ];

        $app    = $this->appModel->find($id);
        
        //delete app
        $result = $app->delete();
        if (!$result) {
            
            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];

        } 
        
        return $output;

    }

    public function getAppIdFromUser()
    {
        $curentUser = $this->userRepo->getLoginUserData();
        //print_r($curentUser); die();
        return (string)$curentUser["app_id"];
    }

    public function getAppAndUserDatas()
    {
        $app_id        = $this->getAppIdFromUser();
        $appCollection = $this->appModel->with('user');
        if (!empty($app_id)) {
            $appCollection = $appCollection->where("id", $app_id);
        }
        return $appCollection->get()->toArray();

    }

    public function addAppToDatas($datas)
    {
        $outputs = [];
        foreach ($datas as $data) {
            if (!empty($data["app_id"])){
                $app = $this->getAppById($data["app_id"]);

                if (!empty($app)) {
                    $data["app"] = $app->toArray();
                }

                //keep to output
                $outputs[] = $data;
            }
        }
        return $outputs;
    }
}