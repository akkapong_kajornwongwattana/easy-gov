<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Config;
use App;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;

    protected $baseUrl;

    public function __construct()
    {
        
    }

    protected function setBaseUrl()
    {
        $this->baseUrl = Config::get('config.base_url');

        return $this;
    }

    // //method for get permission of user
    // //return in array of permission
    // public function canAccess($module)
    // {
    //     return $this->userRepo->canAccess($module);
    // }
}