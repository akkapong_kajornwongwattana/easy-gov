<?php namespace App\Http\Controllers\Html\CRUD;

use App\Http\Controllers\Html\CRUDHtmlController;
use App\Repositories\UserRepository;
use Config;
use App;
use Session;

class DashboardController extends CRUDHtmlController {

    //protected $userAcl; 
    protected $myValidate;
    protected $userRepo;

    private $module  = "DashboardController";
    private $mapping = [
        "list"   => "dashboard",
        "create" => "dashboard",
        "update" => "dashboard",
        "view"   => "dashboard"
    ];

    public function __construct(UserRepository $userRepo)
    {
        parent::__construct();
        
        $this->myValidate = App::make('App\Services\MyValidate');
        
        //$this->userAcl    = $this->canAccess($this->module);
        
        $this->userRepo   = $userRepo;
    }

    //------- Interface method --------//
    public function getList()
    {
        //check permission
        // if (!$this->userAcl["read"]) {
        //     //return no permission 
        //     return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."list dashboard");
        // } 
        //get user detail
        $user = $this->userRepo->getUserById(Session::get("userId"));
        //can use this method
        return $this->getOutput($this->mapping["list"], ["data" => ["user" => $user], "module_name" => $this->module]);

    }

    public function getCreate()
    {
        //check permission
        // if (!$this->userAcl["create"]) {
        //     //return no permission page
        //     return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."create dashboard");
        // } 

        //TODO
        //can use this method

        return $this->getOutput($this->mapping["create"], ["data" => [], "module_name" => $this->module]);
        
    }

    public function getView($id)
    {
       //check permission
        // if (!$this->userAcl["read"]) {
        //     //return no permission page
        //     return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."view dashboard");
        // } 
        
        //TODO
        //can use this method
    }
    
    public function getUpdate($id)
    {
        //check permission
        // if (!$this->userAcl["update"]) {
        //     //return no permission page
        //     return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."update dashboard");
        // } 

        //TODO
        //can use this method
    }
    
    public function deleteDelete()
    {
        //check permission
        // if (!$this->userAcl["delete"]) {
        //     //return no permission page
        //     return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."delete dashboard");
        // } 

        //TODO
        //can use this method
    }
    //------ End Interface method ------//

}