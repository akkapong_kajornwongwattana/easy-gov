<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Uplaod Form</title>

    <link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/modal/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}">
    
</head>
<body>

    <div id="form_upload_panel">
        <form id="form" action="{{ URL::to('api/peopledocument/uploadmultidocument') }}" method="POST" role="form" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <input type="hidden" id="people_id" name="people_id" value="" />
            <input type="hidden" id="document_type_ids" name="document_type_ids" value="" />
            <div class="row">
                <div class="col-md-6" id="form_data">
                <!-- FIELD IN FORM -->
                @foreach ($fields as $field)
                    <div class="form-group bg-info" id="document_{{ $field['document_id'] }}_panel" >
                        <div class="form-group">
                            <label class="control-label">
                                {{ $field['label'] }}
                            </label>
                            @if (isset($field['file_path']) && !empty($field['file_path']))
                            <div class="form-group">
                                <label class="control-label col-sm-2" >Current File</label>
                                <a target="_blank" href="{{ $field['file_path'] or '#' }}" >{{ $field['file_path'] }}</a>
                            </div>
                            @endif
                            <div class= "clearfix"></div>
                            <div class="input-group col-md-10">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-group">
                                        <div class="form-control">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <div class="input-group-btn">
                                            <div class="btn btn-light-grey btn-file">
                                                <span class="fileupload-new"><i class="fa fa-folder-open-o"></i> Select file</span>
                                                <span class="fileupload-exists"><i class="fa fa-folder-open-o"></i> Change</span>
                                                <input type="file" class="file-input" name="file_{{ $field['document_id'] }}" id="file_{{ $field['document_id'] }}" >
                                            </div>
                                            <a href="#" class="btn btn-light-grey fileupload-exists" data-dismiss="fileupload">
                                                <i class="fa fa-times"></i> Remove
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class= "clearfix"></div></br></hr>

                @endforeach
                </div>

            </div>
            <br>
            
            <div class="row">
                <div class="col-md-8">

                </div>
                 <div class="col-md-4">
                    <button class="btn btn-primary btn-block" type="submit">
                        Upload Files <i class="fa fa-arrow-circle-right"></i>
                    </button>
                </div>

            </div>
        </form> 
    </div>
    <!-- Scripts -->

    <script src="{{ URL::asset('assets/plugins/jQuery/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>

    <script>
        
        function addDocumentIds(fields)
        {
            //define ids
            var ids = [];
            for (var i=0; i < fields.length; i++) {
                ids.push(fields[i]["document_id"]);

            }

            //add to hidden field
            $("#document_type_ids").val(ids.toString());
        }

        $(document).ready(function() {
            var fields   = <?php echo json_encode($fields); ?>;
            var peopleId = <?php echo json_encode($people_id); ?>; 
            //add document id
            addDocumentIds(fields)
            //add people to hidden field
            $("#people_id").val(peopleId);

        });
    </script>
</body>
</html>
