<?php namespace App\Http\Controllers\Api;

use App\Repositories\JobRepository;
use App\Http\Controllers\ApiController;
use App;
use Input;

class JobController extends ApiController {

    protected $jobRepo;
    protected $myValidate;
    protected $myResponse;


    function __construct(JobRepository $jobRepo)
    {
        parent::__construct();

        $this->jobRepo    = $jobRepo;
        $this->myValidate = App::make('App\Services\MyValidate');
        $this->myResponse = App::make('App\Services\MyResponse');
    }

    //Method for create coddition
    private function createCondition($inputs)
    {
        //define outputs
        $keys       = [];
        $operators  = [];
        $parameters = [];
        //define allow condition 
        $allows = ["job_type_id", "app_id"];
        foreach ($inputs as $key => $value) {
            //check allow
            if (in_array($key, $allows)) {
                $keys[] = $key;
                $operators[] = "=";
                $parameters[$key] = $value;
            }   
        }
        return [$keys, $operators, $parameters];
    }

    //Method for get job
    public function getJob()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [

        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for create condition for query data
        $conds = $this->createCondition($input);
        //--- Process Section ---//
        $result = $this->jobRepo->getJob($conds[0], $conds[1], $conds[2]);
        $result = $this->jobRepo->moveDataToApp($result);

        //--- Output Section ---//
        return $this->myResponse->output($result);
    }

   
}