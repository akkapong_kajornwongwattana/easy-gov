<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\Module;
use App;
use Config;
use DB;

class ModuleRepository {
    protected $module;
    protected $myService;
    protected $messages;
    protected $menuRepo;

    
    public function __construct(Module $module) 
    {   
        $this->module    = $module;
        
        $this->myService = App::make('App\Services\MyServices');
        $this->menuRepo  = App::make('App\Repositories\MenuRepository');
        $this->messages  = Config::get('message');
    }

    public function getModule($keys, $operators, $params)
    {
        //create param for add condition
        $conditions       = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $moduleCollection = $this->myService->addConditiontoCollection($conditions, $this->module);
        $output           = $moduleCollection->get()->toArray();
        return $output;
    }

    //check duplicate module
    private function duplicateModule($param)
    {
        //define parameters
        $keys        = ["module_name"];
        $operators   = ["="];
        
        $isDuplicate = $this->myService->checkDuplicate($this->module, $keys, $operators, $param);
        
        return $isDuplicate;
    }

    public function addModule($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicateModule($param);

        if (!$isDups[0]) {
            
            // add data to database
            $module = new Module;
            // add data to collection object
            $module->module_name = $param["module_name"];
            $module->description = $param["description"];
            $module->has_create  = ((isset($param["has_create"]))&&($param["has_create"] === 'true'))? true: false;
            $module->has_read    = ((isset($param["has_read"]))&&($param["has_read"] === 'true'))? true: false;
            $module->has_update  = ((isset($param["has_update"]))&&($param["has_update"] === 'true'))? true: false;
            $module->has_delete  = ((isset($param["has_delete"]))&&($param["has_delete"] === 'true'))? true: false;
            $module->menu_id     = $param["menu_id"];

            if (!$module->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            } 
            
        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;
    }

    private function checkDupForUpdate($param)
    {
        $isDuplicate = false;
        //check duplicate
        $duplicates = $this->duplicateModule($param);
        if ($duplicates[0]) {
            $tmp = $duplicates[1];
            //check self
            if ((int)$tmp["id"] != (int)$param["id"]) {
                $isDuplicate = true;
            }
        }
        return $isDuplicate;
    }

    public function updateModule($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        if (!empty($param["id"])) {
            //get record
            $module      = $this->module->find($param['id']);
            
            //check duplicate
            $isDuplicate = $this->checkDupForUpdate($param);

            if (!$isDuplicate){

                //update data
                $module->module_name = $param["module_name"];
                $module->description = $param["description"];
                $module->has_create  = ((isset($param["has_create"]))&&($param["has_create"] === 'true'))? true: false;
                $module->has_read    = ((isset($param["has_read"]))&&($param["has_read"] === 'true'))? true: false;
                $module->has_update  = ((isset($param["has_update"]))&&($param["has_update"] === 'true'))? true: false;
                $module->has_delete  = ((isset($param["has_delete"]))&&($param["has_delete"] === 'true'))? true: false;
                $module->menu_id     = $param["menu_id"];

                if (!$module->save()) {
                    $result["success"] = false;
                    $result["message"] = $this->messages["update_error"];
                }

                
            } else {
                $result["success"] = false;
                $result["message"] = $this->messages["duplicate"];
            }
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
    }

    public function deleteModule($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];

        $module    = $this->module->find($id);
        
        DB::beginTransaction();
        //delete menu
        $result = $this->menuRepo->deleteMenu($module["menu_id"]);
        
        if($result["status"] != 200) {

            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];
            DB::rollBack();
            
        } else {
            //delete module
            $result = $module->delete();
            if (!$result) {
                
                $output["status"]  = 290;
                $output["message"] = $this->messages["delete_error"];
                DB::rollBack();

            } else {
                DB::commit();
            }
        }
        
        return $output;
    }

    public function getModuleNotInList($list, $key)
    {
        $modules = $this->module->whereNotIn($key, $list)->get()->toArray();
        return $modules;
    }

    

}