
@extends('listtemplate')


@section('list_name_page')
People
@stop

@section('css_for_list')
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/select2/select2.css') }}">
@stop

@section('list_link_header')
                                    <li>
                                        <a href="{{ URL::to('backend/dashboard/list') }}">
                                            Dashboard
                                        </a>
                                    </li>
                                    <li class="active">
                                        List
                                    </li>
@stop

@section('list_content')
                                        <!-- <div class="row">
                                            <div class="col-md-12 space20">
                                                <div>
                                                    <a id="add-admin-btn" href="{{ URL::to('backend/employee/create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> Add Employee</a>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover" id="sample_2">
                                                <thead>
                                                    <tr>
                                                        <th>Fisrtname</th>
                                                        <th>Lastname</th>
                                                        <th>Birthday</th>
                                                        <th>Card No</th>
                                                        <th>Phone No</th>
                                                        <th>Email</th>
                                                        <th> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if ($datas)
                                                    @foreach($datas as $data)
                                                        @if (isset($data['id']))
                                                            <tr id="list-{{$data['id']}}">
                                                                <td>{{ $data['firstname'] or '' }}</td>
                                                                <td>{{ $data['lastname'] or '' }}</td>
                                                                <td>{{ $data['birthday'] or '' }}</td>
                                                                <td>{{ $data['card_no'] or '' }}</td>
                                                                <td>{{ $data['tel'] or '' }}</td>
                                                                <td>{{ $data['email'] or '' }}</td>
                                                                <td>
                                                                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                                                                        <!-- <a class="btn btn-primary tooltips" href="{{ URL::to('people/update/'.$data['id']) }}" data-original-title="Edit">Edit</a> -->
                                                                        <a onclick="deleteData({{ $data['id'] }});" class="btn btn-primary btn-red tooltips deleteButton" id="delete-button-{{ $data['id'] }}" data-id="{{ $data['id'] }}" data-toggle="modal"  data-original-title="Remove">Delete</a>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>


@stop


@section('list_js_script')

    function deleteData(id) {
        $('#smallModal').modal('show');

        $('#smallModal').find("#okay").click(function() {
            if(id!=0){
                $.ajax({
                    type:'DELETE',
                    url: "{{ URL::to('people/delete') }}",
                    data:{ _token: '{{ csrf_token() }}', 'id':id },
                    async: false,
                    dataType: "json",
                    success:function(result){
                        $('.modal-footer').html('<button type="button" data-dismiss="modal" class="btn green" id="okay">OK</button>');

                        if(result.status==290){
                            $('#smallModal .modal-body').html(result.message);

                        } else {
                            $('#smallModal .modal-body').html("Delete Complete!");
                            $('#sample_2').find('#list-'+id).remove();
                            $('#smallModal').modal('hide');
                        }
                    },
                    error: function(){

                    }
                });
            }

        });

        $('#smallModal').on('hidden.bs.modal', function () {

            $('#smallModal .modal-body').html('Would you like to delete?');
            $('#smallModal .modal-footer').html('<button data-dismiss="modal" class="btn btn-default" type="button">Close</button><button class="btn btn-primary" type="button" id="okay">OK</button>');
        });
    }

@stop

@section('list_jquery')
    $(".search-select").select2({
        placeholder: "Select chart",
        allowClear: false
    });
@stop



