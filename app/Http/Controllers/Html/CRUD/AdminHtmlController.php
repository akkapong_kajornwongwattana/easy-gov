<?php namespace App\Http\Controllers\Html\CRUD;

use App\Http\Controllers\Html\CRUDHtmlController;
use App;

class AdminHtmlController extends CRUDHtmlController {
    protected $userRepo;

    public function __construct()
    {
        parent::__construct();

        $this->userRepo = App::make('App\Repositories\UserRepository');
    }

    protected function canAccess($module)
    {
        return $this->userRepo->canAccess($module);
    }
}