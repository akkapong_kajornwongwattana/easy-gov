<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\UserAcl;
use App;
use Config;

class UserAclRepository {
    protected $userAcl;
    protected $moduleRepo;
    protected $myService;
    protected $messages;

    public function __construct(UserAcl $userAcl)
    {
        $this->userAcl    = $userAcl;

        $this->myService  = App::make('App\Services\MyServices');
        $this->moduleRepo = App::make('App\Repositories\ModuleRepository');
        $this->messages   = Config::get('message');
    }

    public function getUserAcl($keys, $operators, $params)
    {
        //create param for add condition
        $conditions        = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $userAclCollection = $this->myService->addConditiontoCollection($conditions, $this->userAcl);
        $output            = $userAclCollection->with('user', 'module')->get()->toArray();
        return $output;
    }

    //check duplicate module
    private function duplicateUserAcl($param)
    {
        //define parameters
        $keys        = ["module_id", "user_id"];
        $operators   = ["=", "="];

        $isDuplicate = $this->myService->checkDuplicate($this->userAcl, $keys, $operators, $param);

        return $isDuplicate;
    }

    public function addUserAcl($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicateUserAcl($param);

        if (!$isDups[0]) {

            // add data to database
            $userAcl = new UserAcl;
            // add data to collection object
            $userAcl->module_id  = $param["module_id"];
            $userAcl->user_id    = $param["user_id"];
            $userAcl->can_create = ((isset($param["can_create"]))&&($param["can_create"] === 'true'))? true: false;
            $userAcl->can_read   = ((isset($param["can_read"]))&&($param["can_read"] === 'true'))? true: false;
            $userAcl->can_update = ((isset($param["can_update"]))&&($param["can_update"] === 'true'))? true: false;
            $userAcl->can_delete = ((isset($param["can_delete"]))&&($param["can_delete"] === 'true'))? true: false;
            $userAcl->side_menu  = ((isset($param["side_menu"]))&&($param["side_menu"] === 'true'))? true: false;

            if (!$userAcl->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            }

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;
    }

    private function checkDupForUpdate($param)
    {
        $isDuplicate = false;
        //check duplicate
        $duplicates = $this->duplicateUserAcl($param);
        if ($duplicates[0]) {
            $tmp = $duplicates[1];
            //check self
            if ((int)$tmp["id"] != (int)$param["id"]) {
                $isDuplicate = true;
            }
        }
        return $isDuplicate;
    }

    public function updateUserAcl($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        if (!empty($param["id"])) {
            //get record
            $userAcl      = $this->userAcl->find($param['id']);

            //check duplicate
            $isDuplicate = $this->checkDupForUpdate($param);

            if (!$isDuplicate){

                //update data
                $userAcl->module_id  = $param["module_id"];
                $userAcl->user_id    = $param["user_id"];
                $userAcl->can_create = ((isset($param["can_create"]))&&($param["can_create"] === 'true'))? true: false;
                $userAcl->can_read   = ((isset($param["can_read"]))&&($param["can_read"] === 'true'))? true: false;
                $userAcl->can_update = ((isset($param["can_update"]))&&($param["can_update"] === 'true'))? true: false;
                $userAcl->can_delete = ((isset($param["can_delete"]))&&($param["can_delete"] === 'true'))? true: false;
                $userAcl->side_menu  = ((isset($param["side_menu"]))&&($param["side_menu"] === 'true'))? true: false;

                if (!$userAcl->save()) {
                    $result["success"] = false;
                    $result["message"] = $this->messages["update_error"];
                }


            } else {
                $result["success"] = false;
                $result["message"] = $this->messages["duplicate"];
            }


        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
    }

    public function deleteUserAcl($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];

        $userAcl = $this->userAcl->find($id);
        $result  = $userAcl->delete();

        if (!$result) {

            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];

        }

        return $output;
    }

    public function getModuleForUserAcl($userAclId, $userId="")
    {
        $modules = [];
        if (empty($userAclId)) {
            //create
            //create param
            $params    = ["user_id" => $userId];
            //get current user acl list
            $userAcls  = $this->getUserAcl(["user_id"], ["="], $params);
            //get only module_id
            $moduleIds = $this->myService->getListDataFromArr($userAcls, "module_id");
            //get module
            $modules   = $this->moduleRepo->getModuleNotInList($moduleIds, "id");

        } else {
            //update
            $users   = $this->getUserAcl(["id"], ["="], ["id" => $userAclId]);
            if (!empty($users)) {
                $users = $users[0];
            }
            //get modules
            $modules = $this->moduleRepo->getModule(["id"], ["="], ["id" => $users["module_id"]]);
        }

        return $modules;
    }

}