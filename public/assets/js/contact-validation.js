var FormValidator = function () {
	"use strict";
	
    // function to initiate Validation Sample 1
    var runValidator1 = function () {
        var form1 = $('#form');
        var errorHandler1 = $('.errorHandler', form1);
        var successHandler1 = $('.successHandler', form1);
        
        $('#form').validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                    error.insertAfter($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: "",
            rules: {
                name: {
                    required: true
                },
                surname: {
                    required: true
                },
                personal_email: {
                    required: true,
                    email: true
                },
                office_email: {
                    email: true
                },
                position: {
                    required: true
                },
                mobile_phone1: {
                    required: true,
                    number: true,
                    minlength: 10
                },
                mobile_phone2: {
                    number: true,
                    minlength: 10
                },
                home_number: {
                    number: true,
                    minlength: 9
                },
                office_number: {
                    number: true,
                    minlength: 9
                },
                department: {
                    required: true
                },
                
            },
            messages: {
                name: "Please specify your first name",
                surname: "Please specify your last name",
                personal_email: {
                    required: "Email cannot be empty.",
                    email: "Your email address must be in the format of name@domain.com"
                },
                office_email: {
                    email: "Your email address must be in the format of name@domain.com"
                },
                position: "Position cannot be empty",
                department: "Department cannot be empty",
                mobile_phone1: {
                    required: "Mobile phone cannot be empty",
                    number: "Mobile phone must be in mumber",
                    minlength: "Mobile phone must have atleast 10 digits"
                },
                mobile_phone2: {
                    number: "Mobile phone must be in mumber",
                    minlength: "Mobile phone must have atleast 10 digits"
                },
                home_number: {
                    number: "Home number must be in mumber",
                    minlength: "Home number must have atleast 9 digits"
                },
                office_number: {
                    number: "Office number must be in mumber",
                    minlength: "Office number must have atleast 9 digits"
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler1.hide();
                errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (form) {
                //successHandler1.show();
                errorHandler1.hide();
                // submit form
                form.submit();
            }
        });
    };
    return {
        //main function to initiate template pages
        init: function () {
        	runValidator1();
        }
    };
}();