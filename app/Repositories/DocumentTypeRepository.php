<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\Documenttype;
use App;
use DB;
use Config;

class DocumentTypeRepository {

    protected $documentType;
    protected $myService;
    protected $messages;
    

    public function __construct(Documenttype $documentType)
    {
        $this->documentType = $documentType;
        
        $this->myService    = App::make('App\Services\MyServices');
        
        //get message
        $this->messages     = Config::get('message');
    }

    //For get document type from condition 
    public function getDocumentType($keys, $operators, $params)
    {
        //create param for add condition
        $conditions             = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $documentTypeCollection = $this->myService->addConditiontoCollection($conditions, $this->documentType);
        // $documentTypeCollection = $documentTypeCollection->with('app');
        $output                 = $documentTypeCollection->get()->toArray();
        
        return $output;
    }

    //check duplicate module
    private function duplicateDocumentType($param)
    {
        //define parameters
        $keys               = ["name"];
        $operators          = ["="];
        
        $isDuplicate        = $this->myService->checkDuplicate($this->documentType, $keys, $operators, $param);
        
        return $isDuplicate;
    }

    public function addDocumentType($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicateDocumentType($param);

        if (!$isDups[0]) {

            // add data to database
            $documentType               = new Documenttype;
            // add data to collection object
            $documentType->name         = $param["name"];
            
            if (!$documentType->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            }

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;
    }

    private function checkDupForUpdate($param)
    {
        $isDuplicate = false;
        //check duplicate
        $duplicates = $this->duplicateDocumentType($param);
        if ($duplicates[0]) {
            $tmp = $duplicates[1];
            //check self
            if ((int)$tmp["id"] != (int)$param["id"]) {
                $isDuplicate = true;
            }
        }
        return $isDuplicate;
    }

    public function updateDocumentType($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        if (!empty($param["id"])) {
            //get record
            $documentType = $this->documentType->find($param['id']);
            
            //check duplicate
            $isDuplicate  = $this->checkDupForUpdate($param);

            if (!$isDuplicate){
                
                // $documentType->app_id       = $param["app_id"];
                $documentType->name         = $param["name"];

                if (!$documentType->save()) {
                    $result["success"] = false;
                    $result["message"] = $this->messages["update_error"];
                }
                
            } else {
                $result["success"] = false;
                $result["message"] = $this->messages["duplicate"];
            }
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
    }

    public function deleteDocumentType($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];

        $documentType = $this->documentType->find($id);

        if (!$documentType->delete()) {
                
            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];

        }

        return $output;
    }

}