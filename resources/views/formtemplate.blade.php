
@extends('layout')

@section('css_req_for_page')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/plugins/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/modal/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/DataTables/media/css/DT_bootstrap.css') }}">
@yield('css_for_form')
@stop

@section('main_content')

                    <div class="container">
                        <!-- start: PAGE HEADER -->
                        <!-- start: TOOLBAR -->
                        <div class="toolbar row">
                            <div class="col-sm-6 hidden-xs">
                                <div class="page-header">
                                    <h1>@yield('form_name_page') <small>Management </small></h1>
                                </div>
                            </div>
                        </div>
                        <!-- Show error message -->
                        @if (!empty($errormsg))
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close">
                                    &times;
                                </button>
                                <strong>ERROR!</strong> {{ $errormsg }}
                            </div>
                        @endif
                        <!-- end: Show error message -->
                        <!-- end: TOOLBAR -->
                        <!-- end: PAGE HEADER -->
                        <!-- start: BREADCRUMB -->
                        <div class="row">
                            <div class="col-md-12">
                                <ol class="breadcrumb">
                                    @yield('form_link_header')
                                </ol>
                            </div>
                        </div>
                        <!-- end: BREADCRUMB -->
                        <!-- start: PAGE CONTENT -->
                        <div class="row">
                            <div >
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><span class="text-bold">@yield('form_name_page')</span></h4>
                                        <div class="panel-tools">
                                            <div class="dropdown">
                                                <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
                                                    <i class="fa fa-cog"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-light pull-right" role="menu">
                                                    <li>
                                                        <a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
                                                    </li>
                                                    <li>
                                                        <a class="panel-refresh" href="#">
                                                            <i class="fa fa-refresh"></i> <span>Refresh</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="panel-config" href="#panel-config" data-toggle="modal">
                                                            <i class="fa fa-wrench"></i> <span>Configurations</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="panel-expand" href="#">
                                                            <i class="fa fa-expand"></i> <span>Fullscreen</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a class="btn btn-xs btn-link panel-close" href="#">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>

                                    <div id="form-iotalk-admin" class="panel-body">
                                        @yield('form_content')
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end: PAGE CONTENT-->
                    </div>
                    <div class="subviews">
                        <div class="subviews-container"></div>
                    </div>

@stop

@section('js_req_for_page')
<script src="{{ URL::asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/ckeditor/adapters/jquery.js') }}"></script>
@yield('js_req_for_form')
@stop

@section('js_script')


    @yield('form_js_script')
@stop

@section('jquery')

        @yield('form_jquery')
@stop
