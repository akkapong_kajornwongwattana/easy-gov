<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserAcl;


class PeopleDocument extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'people_document';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['people_id', 'documenttype_id', 'file'];

    // /**
    //  * Relation between people document and document type.
    //  *
    //  * @return Collection.
    //  */
    public function document_type()
    {
        return $this->hasMany('App\Models\Documenttype', 'id', 'documenttype_id');
    }
}
