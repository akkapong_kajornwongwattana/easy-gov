<?php namespace App\Http\Controllers\Api;

use App\Repositories\PeopleDocumentRepository;
use App\Http\Controllers\ApiController;
use App;
use Input;

class PeopleDocumentController extends ApiController {

    protected $peopleDocRepo;
    protected $myValidate;
    protected $myResponse;


    function __construct(PeopleDocumentRepository $peopleDocRepo)
    {
        parent::__construct();

        $this->peopleDocRepo = $peopleDocRepo;
        $this->myValidate    = App::make('App\Services\MyValidate');
        $this->myResponse    = App::make('App\Services\MyResponse');
    }

    //Method for upload document
    public function postUploaddocument()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [
            'people_id'       => 'required',
            'documenttype_id' => 'required',
        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for upload document
        //--- Process Section ---//
        $result = $this->peopleDocRepo->uploadPeopleDocument($params);
        if (!$result["success"]) {
          return $this->myValidate->validateError($result["message"]);
        }

        //--- Output Section ---//
        return $this->myResponse->output($result["file"]);
    }



    //Method for get document
    public function getPeopledocument()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [
            'people_id' => 'required'
        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        //create condition
        $conditions = $this->peopleDocRepo->createCondition($params);
        // for get document
        //--- Process Section ---//
        $result = $this->peopleDocRepo->getPeopleDocument($conditions[0], $conditions[1], $conditions[2]);

        //--- Output Section ---//
        return $this->myResponse->output($result);
    }

    //Method for change document file
    public function postChangedocument()
    {
        $input = Input::all();
        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [
            'id' => 'required'
        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for change document
        //--- Process Section ---//
        $result = $this->peopleDocRepo->changeDocumentFile($params);
        if (!$result["success"]) {
          return $this->myValidate->validateError($result["message"]);
        }

        //--- Output Section ---//
        return $this->myResponse->output($result["file"]);
    }

    //Method for upload document
    public function postUploadmultidocument()
    {
        $input = Input::all();

        //create result list
        $successList = [];

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [
            'people_id'         => 'required',
            'document_type_ids' => 'required',
        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for upload document
        //--- Process Section ---//
        //convert to list
        $documentIds = explode(",", $params["document_type_ids"]);
        foreach ($documentIds as $documentId) {
            //create parameter for upload
            if (isset($params["file_".$documentId]) && !empty($params["file_".$documentId])) {
                $eachParam = [
                    'people_id'       => $params["people_id"],
                    'documenttype_id' => $documentId,
                    'file'            => $params["file_".$documentId],
                    'old_file'        => "",
                ];

                //replace file for cdn
                $_FILES["file"] = $_FILES["file_".$documentId];
                
                $result = $this->peopleDocRepo->uploadPeopleDocument($eachParam);
                if ($result["success"]) {
                    $successList[$documentId] = $result["file"];
                }
            }
            
        }

        //--- Output Section ---//
        return $this->myResponse->output($successList);
    }
   
}