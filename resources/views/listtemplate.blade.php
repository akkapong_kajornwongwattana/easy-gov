
@extends('layout')

@section('css_req_for_page')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/plugins/select2/select2.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/modal/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/DataTables/media/css/DT_bootstrap.css') }}">
@yield('css_for_list')
@stop

@section('main_content')
                    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
                    <div class="container">
                        <!-- start: PAGE HEADER -->
                        <!-- start: TOOLBAR -->
                        <div class="toolbar row">
                            <div class="col-sm-6 hidden-xs">
                                <div class="page-header">
                                    <h1>@yield('list_name_page') <small>Management</small></h1>
                                </div>
                            </div>

                        </div>
                        <!-- end: TOOLBAR -->
                        <!-- end: PAGE HEADER -->
                        <!-- start: BREADCRUMB -->
                        <div class="row">
                            <div class="col-md-12">
                                <ol class="breadcrumb">
                                    @yield('list_link_header')

                                </ol>
                            </div>
                        </div>
                        <!-- end: BREADCRUMB -->
                        <div class="row">
                            <div class="col-md-12">
                                <!-- start: DYNAMIC TABLE PANEL -->
                                <div class="panel panel-white">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">@yield('list_name_page') <span class="text-bold">List</span></h4>
                                        <div class="panel-tools">
                                            <div class="dropdown">
                                                <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
                                                    <i class="fa fa-cog"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-light pull-right" role="menu">
                                                    <li>
                                                        <a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
                                                    </li>
                                                    <li>
                                                        <a class="panel-refresh" href="#">
                                                            <i class="fa fa-refresh"></i> <span>Refresh</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="panel-config" href="#panel-config" data-toggle="modal">
                                                            <i class="fa fa-wrench"></i> <span>Configurations</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="panel-expand" href="#">
                                                            <i class="fa fa-expand"></i> <span>Fullscreen</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a class="btn btn-xs btn-link panel-close" href="#">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        @yield('list_content')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<div class="modal fade" id="smallModal" tabindex="-1" role="dialog" aria-labelledby="smallModal" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Would you like to delete?</h4>
       </div>
        <div class="modal-body">
            <p>Would you like to delete?</p>
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-primary" type="button" id="okay">OK</button>
        </div>
    </div>
  </div>
</div>

@stop

@section('js_req_for_page')
<script type="text/javascript" src="{{ URL::asset('assets/plugins/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/table-data.js') }}"></script>
@stop
@section('js_req_for_list')
@stop

@section('js_script')


    @yield('list_js_script')
@stop

@section('jquery')
    TableData.init();
    @yield('list_jquery')
@stop
