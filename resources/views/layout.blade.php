<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->

<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
        <title> Dashboard Admin</title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <!-- <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'> -->
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/iCheck/skins/all.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/animate.css/animate.min.css') }}">
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/owl-carousel/owl-carousel/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/owl-carousel/owl-carousel/owl.theme.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/owl-carousel/owl-carousel/owl.transitions.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/summernote/dist/summernote.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/fullcalendar/fullcalendar/fullcalendar.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/toastr/toastr.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}">
        <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        @section('css_req_for_page')
        @show
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- start: CORE CSS -->

        <link rel="stylesheet" href="{{ URL::asset('assets/css/styles.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/styles-responsive.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/themes/theme-default.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/print.css') }}" type="text/css" media="print">

        <!-- end: CORE CSS -->
        <!-- <link rel="shortcut icon" href="favicon.ico" /> -->
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>

        <div class="main-wrapper">
            <div id="base_url" url="{{ URL::to('') }}"></div>
            <!-- start: TOPBAR -->
            <header class="topbar navbar navbar-inverse navbar-fixed-top inner">
                <!-- start: TOPBAR CONTAINER -->
                <div class="container">

                    <div class="topbar-tools">
                        <!-- start: TOP NAVIGATION MENU -->
                        <ul class="nav navbar-right">
                            <!-- start: USER DROPDOWN -->
                            <li class="dropdown current-user">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                <span class="username hidden-xs">{{ $username or "" }}</span> <i class="fa fa-caret-down "></i>
                                </a>
                                <ul class="dropdown-menu dropdown-dark">
                                    <li>
                                        <a href="pages_user_profile.html">
                                            My Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a id="logout-link" href="{{ URL::to('login/logout') }}">
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: USER DROPDOWN -->
                        </ul>
                        <!-- end: TOP NAVIGATION MENU -->
                    </div>
                </div>
                <!-- end: TOPBAR CONTAINER -->
            </header>
            <!-- end: TOPBAR -->

            <!-- start: PAGESLIDE LEFT -->
            <a class="closedbar inner hidden-sm hidden-xs" href="#">
            </a>
            <nav id="pageslide-left" class="pageslide inner">
                <div class="navbar-content">

                    <!-- start: SIDEBAR -->
                    <div class="main-navigation left-wrapper transition-left">
                        <div class="navigation-toggler hidden-sm hidden-xs">
                            <a href="#main-navbar" class="sb-toggle-left">
                            </a>
                        </div>
                        <div class="user-profile border-top padding-horizontal-10 block">
                            <div class="inline-block">
                                <img src="{{ URL::asset('assets/images/anonymous.jpg') }}" alt="" style="width:50px;height:50px;">
                            </div>
                            <div class="inline-block">
                                <h5 class="no-margin"> Welcome </h5>
                                <h4 class="no-margin"> {{ $username or "" }} </h4>
                            </div>
                        </div>
                        <!-- start: MAIN NAVIGATION MENU -->
                        <ul class="main-navigation-menu">
                            @foreach ($menus as $menu)
                            <li class="main-menu" id="{{ $menu['module_name'] }}">
                                @if (!empty($menu['url']))
                                <a href="{{ URL::to( $menu['url'] ) }}">
                                @else
                                <a href="javascript:void(0)">
                                @endif
                                    <i class="fa fa-cogs"></i><span class="title"> {{ $menu['menu_display_text'] }} </span><i class="icon-arrow"></i>
                                </a>
                                @if (isset($menu['child']))
                                <ul class="sub-menu">
                                    @foreach ($menu['child'] as $child)

                                    <li id="{{ $child['module_name'] }}">
                                        @if (!empty($child['url']))
                                        <a href="{{ URL::to( $child['url'] ) }}">
                                        @else
                                        <a href="javascript:void(0)">
                                        @endif
                                            <span class="title"> {{ $child['menu_display_text'] }} </span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                            <li class="main-menu" id="space_menu">
                                <div style="height:20px;"></div>
                            </li>
                        </ul>
                        <!-- end: MAIN NAVIGATION MENU -->
                    </div>
                    <!-- end: SIDEBAR -->
                </div>

            </nav>
            <!-- end: PAGESLIDE LEFT -->

            <!-- start: MAIN CONTAINER -->
            <div class="main-container inner">
                <!-- start: PAGE -->
                <div class="main-content">
                    <!-- start: PANEL CONFIGURATION MODAL FORM -->
                    @yield('main_content')
                </div>
                <!-- end: PAGE -->
            </div>
            <!-- end: MAIN CONTAINER -->
            <!-- start: FOOTER -->
            <footer class="inner">
                <div class="footer-inner">
                    <div class="pull-left">
                        Dashboard Admin
                    </div>
                    <div class="pull-right">
                        <span class="go-top"><i class="fa fa-chevron-up"></i></span>
                    </div>
                </div>
            </footer>
            <!-- end: FOOTER -->


        </div>
        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script>
        <script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
        <![endif]-->
        <!--[if gte IE 9]><!-->
        <script src="{{ URL::asset('assets/plugins/jQuery/jquery-2.1.1.min.js') }}"></script>
        <!--<![endif]-->
        <script src="{{ URL::asset('assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/blockUI/jquery.blockUI.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/iCheck/jquery.icheck.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/moment/min/moment.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootbox/bootbox.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery.appear/jquery.appear.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery-cookie/jquery.cookie.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/velocity/jquery.velocity.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/TouchSwipe/jquery.touchSwipe.min.js') }}"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
        <script src="{{ URL::asset('assets/plugins/owl-carousel/owl-carousel/owl.carousel.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery-mockjax/jquery.mockjax.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/toastr/toastr.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/jquery-validation/dists/jquery.validate.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/DataTables/media/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/truncate/jquery.truncate.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/summernote/dist/summernote.min.js') }}"></script>
        <script src="{{ URL::asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <!--<script src="{{ URL::asset('assets/js/subview.js') }}"></script>
        <script src="{{ URL::asset('assets/js/subview-examples.js') }}"></script>-->
        <script src="{{ URL::asset('assets/js/main.js') }}"></script>

        <!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        @section('js_req_for_page')
        @show
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <!-- start: CORE JAVASCRIPTS  -->

        <!-- end: CORE JAVASCRIPTS  -->
        <script>
            function getBaseUrl(){
                var div_bass_url = $('#base_url');
                return div_bass_url.attr("url");
            }

            // function checkCookie(){
            //     var username = "{{ Cookie::get('username') }}";
            //     if (username == ""){
            //         //return to login page
            //         var url = getBaseUrl();
            //         window.location = url+"/login/login";
            //     }
            // }

            function handleMenu(menu_id, sub_id){
                var module_name   = "<?php echo $module_name ?>";
                //var module_name = "#SubUserCRUDController";
                var selectedMenu  = $('#'+module_name);
                var parent        = selectedMenu.parent('.sub-menu');
                var mainMenu      = parent.parent('.main-menu');

                mainMenu.addClass("active open");
                selectedMenu.addClass("active");
                // console.log(parent);
                // console.log($('#Subuser').parent('.sub-menu'));
                // if (parent) {
                //     var mainMenu = parent.parent('.main-menu');
                //     mainMenu.addClass("active open");
                //     selectedMenu.addClass("active");
                // }else {
                //     selectedMenu.addClass("active open");
                // }


            }

            @yield('js_script')

            jQuery(document).ready(function() {
                Main.init();
                //SVExamples.init();
                handleMenu();
                //check cookie
                //checkCookie();
                @yield('jquery')
            });

        </script>


        @yield('js_init_req_for_page')
    </body>
    <!-- end: BODY -->
</html>