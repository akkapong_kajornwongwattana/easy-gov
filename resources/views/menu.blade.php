
@extends('formtemplate')

@section('form_name_page')
Menu
@stop

@section('form_link_header')
                                    <li>
                                        <a href="{{ URL::to('dashboard/list') }}">
                                            Dashboard
                                        </a>
                                    </li>
                                    <li class="active">
                                        Menu
                                    </li>
@stop

@section('form_content')
                                        <h1><i class="fa fa-pencil-square"></i> MENU</h1>
                                        <hr>
                                        <div id="nestable-menu">
                                            <button type="button" data-action="expand-all" class="btn btn-blue">
                                                Expand All
                                            </button>
                                            <button type="button" data-action="collapse-all" class="btn btn-blue">
                                                Collapse All
                                            </button>
                                            <button type="button" onclick="addNewMenu()" class="btn btn-green">
                                                Add New
                                            </button>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5> Manage Menu </h5>
                                                <!-- start: DRAGGABLE HANDLES 1 -->
                                                <div class="dd" id="nestable">
                                                <?php $index=1; ?>    
                                                @if (isset($menuDatas))
                                                    <ol class="dd-list">
                                                    @foreach ($menuDatas as $menu)
                                                        <li id="menu-{{ $index }}" class="dd-item" data-id="{{ $menu['id'] }}" data-menu="{{ $menu['menu_display_text'] or '' }}" data-url="{{ $menu['url'] or '' }}">
                                                            <button onclick="editData({{ $index }})" data-action="edit" data-toggle="modal">Edit</button>
                                                            <button onclick="deleteData({{ $index }})" data-action="delete" data-toggle="modal">Delete</button>
                                                            <div class="dd-handle">
                                                                {{ $menu['menu_display_text'] or '' }}
                                                                
                                                            </div>
                                                        <?php $index++; ?>    
                                                        
                                                        @if (isset($menu['child']))
                                                            <ol class="dd-list">
                                                            @foreach ($menu['child'] as $child)
                                                                <li id="menu-{{ $index }}" class="dd-item" data-id="{{ $child['id'] }}" data-menu="{{ $child['menu_display_text'] or '' }}" data-url="{{ $child['url'] or '' }}">
                                                                    <button onclick="editData({{ $index }})" data-action="edit" data-toggle="modal">Edit</button>
                                                                    <button onclick="deleteData({{ $index }})" data-action="delete" data-toggle="modal">Delete</button>
                                                                    <div class="dd-handle">
                                                                        {{ $child['menu_display_text'] or '' }}
                                                                        
                                                                    </div>
                                                                    
                                                                </li>
                                                                <?php $index++; ?>  
                                                            @endforeach  
                                                            </ol>
                                                        @endif
                                                        </li>
                                                    @endforeach   
                                                    </ol>
                                                @endif    

                                                        
                                                </div>
                                                <!-- end: DRAGGABLE HANDLES 1 -->
                                            </div>

                                        </div>

                                        <form id="form" action="{{ URL::to('menu/menu') }}" method="POST" role="form" >
                                            <input type="hidden" id="nestable-output" name="menuDatas">
                                            <input type="hidden" id="index" name="index" value="{{ $index }}">
                                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                            
                                            <div class="row">
                                                <div class="col-md-8">

                                                </div>
                                                 <div class="col-md-4">
                                                    <button id="save-btn" class="btn btn-primary btn-block" type="submit">
                                                        Save <i class="fa fa-arrow-circle-right"></i>
                                                    </button>
                                                </div>

                                            </div>
                                        </form>    


<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Manage Menu</h4>
       </div>
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-primary" type="button" id="okay">OK</button>
        </div>
    </div>
  </div>
</div>  

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Would you like to delete?</h4>
       </div>
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-primary" type="button" id="okay">OK</button>
        </div>
    </div>
  </div>
</div>               
@stop

@section('js_req_for_form')
<script src="{{ URL::asset('assets/plugins/nestable/jquery.nestable.js') }}"></script>
<script src="{{ URL::asset('additional_js/menu-nestable.js') }}"></script>
@stop

@section('form_js_script')

@stop

@section('form_jquery')
        UINestable.init();
@stop


