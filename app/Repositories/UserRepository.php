<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\User;
use Session;
use App;
use Config;

class UserRepository {

    protected $user;
    protected $userId;
    protected $myService;
    protected $messages;
    protected $menuRepo;
    protected $moduleRepo;

    protected $mapping_acl = [
        "can_create" => "create",
        "can_read"   => "read",
        "can_update" => "update",
        "can_delete" => "delete",
    ];

    public function __construct(User $user)
    {
        $this->user       = $user;
        $this->moduleRepo = App::make('App\Repositories\ModuleRepository');

        $this->myService  = App::make('App\Services\MyServices');
        $this->messages   = Config::get('message');

        //for test
        //Session::put("userId", 1);
        //get user id from session
        $this->userId     = Session::get("userId");
        $this->menuRepo   = App::make('App\Repositories\MenuRepository');
        //set userId to menuRepo
        $this->menuRepo->setUserId($this->userId);



    }

    //=============== PERMISSION ===================
    public function checkSuperAdmin()
    {
        //define result
        $result = false;
        $user = $this->user->find($this->userId)->toArray();

        if (!empty($user)) {
            if ($user["user_type"] == "super") {
                $result = true;
            }
        }

        return $result;
    }

    private function formatPermissionData($data)
    {
        $output = [];

        foreach ($this->mapping_acl as $key => $value) {
            $output[$value] = ((isset($data[$key]))&&($data[$key]))? true:false;
        }

        return $output;
    }

    private function getModuleId($module)
    {
        $moduleId = 0;

        $module = $this->moduleRepo->getModule(["module_name"], ["="], ["module_name" => $module]);
        if (!empty($module)) {
            $moduleId = $module[0]["id"];
        }
        return $moduleId;
    }

    private function getUserAcl($module)
    {
        //define user output
        $userAcl = [];

        //find module id
        $moduleId = $this->getModuleId($module);
        if ((!empty($this->userId)) && ($moduleId)) {
            //find user
            $userObj = $this->user->find($this->userId);
            if (!empty($userObj)){
                //get only first
                $userAclTmp = $userObj->user_acl()->where('module_id', $moduleId)->first();
                if (!empty($userAclTmp)) {
                    $userAcl = $userObj->user_acl()->where('module_id', $moduleId)->first()->toArray();
                }


                // $userAcl = $userObj->user_acl()->with(['module' => function($query) use ($module) {
                //     $query->where('module.module_name', $module);
                // }])->get()->toArray();

            }

        }
        return $userAcl;
    }

    public function canAccess($module)
    {
        $userAcl = $this->getUserAcl($module);
        $output  = $this->formatPermissionData($userAcl);

        return $output;
    }

    //=============== MENU LIST ===================
    public function getMenuData()
    {
        $menuData = $this->menuRepo->getUserAclForMenu(0, []);
        return $menuData;
    }

    private function encryptPassword($password)
    {
        return md5($password);
    }

    //check user in mock datas
    private function checkMockUser($params)
    {
        $result = [];
        $users = config("mock.login");

        if (isset($users[$params["username"]])) {
            //HAVE USER
            if ($users[$params["username"]]["password"] == $params["password"]) {
                $result = $users[$params["username"]];
            }
        }
        return $result;
    }


    public function checkLogin($params)
    {
        //define output
        $output = [false, Config::get('message.login_fail')];
        //define parameter
        $paramCondition = [
            "username" => $params["username"],
            "password" => $this->encryptPassword($params["password"])
        ];

        //check login
        // $userData = $this->checkMockUser($paramCondition);

        //create condition
        $keys       = ["username", "password"];
        $operators  = ["=", "="];
        $conditions = $this->myService->createCondition($keys, $operators, $paramCondition);
        //add condition to user
        $user       = $this->myService->addConditiontoCollection($conditions, $this->user);
        $userData   = $user->get()->toArray();

        if ($userData){
            $output = [true, $userData[0]];
        }
        return $output;
    }

    // public function getUserById($id)
    // {
    //     //Mock datas
    //     $users = config("mock.login");
    //     foreach ($users as $username => $user) {
    //         if ($user["id"] == $id) {
    //             return $user;
    //         }
    //     }
    //     return [];
    // }

    //===============USER======================
    public function getUserById($id)
    {
        //define output
        $output = [];
        if (!empty($id)) {
            $output = $this->user->find($id)->toArray();
        }
        return $output;
    }

    public function getUser($keys, $operators, $params)
    {
        //add condition
        $conditions = $this->myService->createCondition($keys, $operators, $params);
        //add condition to user
        $user       = $this->myService->addConditiontoCollection($conditions, $this->user);
        $output     = $user->with('app')->get()->toArray();

        return $output;
    }

    //check duplicate user
    private function duplicateUser($param)
    {
        //define parameters
        $keys        = ["username"];
        $operators   = ["="];

        $isDuplicate = $this->myService->checkDuplicate($this->user, $keys, $operators, $param);

        return $isDuplicate;
    }

    public function addUser($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicateUser($param);

        if (!$isDups[0]) {

            // add data to database
            $user            = new User;
            // add data to collection object
            $user->user_type = $param["user_type"];
            $user->username  = $param["username"];
            $user->password  = $this->encryptPassword($param["password"]);
            $user->app_id    = ($param["app_id"])? $param["app_id"] : null;

            if (!$user->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            }

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;
    }

    private function checkDupForUpdate($param)
    {
        $isDuplicate = false;
        //check duplicate
        $duplicates = $this->duplicateUser($param);
        if ($duplicates[0]) {
            $tmp = $duplicates[1];
            //check self
            if ((int)$tmp["id"] != (int)$param["id"]) {
                $isDuplicate = true;
            }
        }
        return $isDuplicate;
    }

    private function checkOldPassword($password, $oldpassword)
    {
        //define result
        $isMatch     = true;

        $oldpassword = $this->encryptPassword($oldpassword);

        if ($password != $oldpassword) {
            $isMatch = false;
        }
        return $isMatch;
    }

    public function updateUser($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];

        if (!empty($param["id"])) {
            //get record
            $user        = $this->user->find($param['id']);

            //check password
            $isMatch      = $this->checkOldPassword($user["password"], $param["oldpassword"]);

            if (!$isMatch) {
                $result["success"] = false;
                $result["message"] = $this->messages["user"]["pass_error"];
                return $result;
            }

            //check duplicate
            $isDuplicate = $this->checkDupForUpdate($param);

            if (!$isDuplicate){

                //update data
                $user->user_type = $param["user_type"];
                $user->username  = $param["username"];
                $user->password  = $this->encryptPassword($param["password"]);
                $user->app_id    = ($param["app_id"])? $param["app_id"] : null;

                if (!$user->save()) {
                    $result["success"] = false;
                    $result["message"] = $this->messages["update_error"];
                }


            } else {
                $result["success"] = false;
                $result["message"] = $this->messages["duplicate"];
            }


        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
    }

    public function deleteUser($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];

        $user    = $this->user->find($id);

        //delete user data
        $result = $user->delete();
        if (!$result) {

            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];

        }

        return $output;
    }

    public function getUsername($userId)
    {
        $user = $this->getUserById($userId);
        $userName = "";
        if ($user){
            $userName = $user["username"];
        }
        return $userName;
    }

    public function getOfficialaccountFromUserId($userId)
    {
        $output = [];
        if (!empty($userId)) {
            $userCollection = $this->user->find($userId);
            $output = $userCollection->officialaccount()->get()->toArray();;
        }
        return $output;

    }

    public function getLoginUserData()
    {
        return $this->user->find($this->userId)->toArray();
    }

    public function mapUsername($datas)
    {
        $outputs = [];
        foreach ($datas as $data) {
            if (isset($data["updated_by"])) {
                $data["updated_name"] = $this->getUsername($data["updated_by"]);
            }

            //keep data to putput
            $outputs[] = $data;
        }
        return $outputs;
    }


}
