<?php namespace App\Services;

use App\Services\CurlServices;
use Config;

class AncServices extends CurlServices {

    public function __construct()
    {
        parent::__construct();

        $this->setUrl(Config::get('api.anc.url'));
    }

    public function getCountSendAndReceive($uri, $params)
    {
        $response = $this->callApi('get', $uri, $params, true);
        //TODO write log in case error
        return $response;
    }

    
}
