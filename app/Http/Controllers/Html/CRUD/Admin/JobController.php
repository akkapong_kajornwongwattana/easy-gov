<?php namespace App\Http\Controllers\Html\CRUD\Admin;

use App\Http\Controllers\Html\CRUD\AdminHtmlController;
use App\Repositories\JobRepository;
use Config;
use App;
use Input;

class JobController extends AdminHtmlController
{
    private $myValidate;
    private $myResponse;
    private $userAcl;
    private $jobRepo;

    private $module      = 'JobController';
    private $moduleName  = 'job';

    private $mapping = [
        "list"   => "job.list",
        "create" => "job.form",
        "update" => "job.form",
        "view"   => "job.form"
    ];
    private $derimetorView     = ".";
    private $derimetorRedirect = "/";


    public function __construct(JobRepository $jobRepo)
    {
        parent::__construct();

        $this->myValidate      = App::make('App\Services\MyValidate');
        $this->myResponse      = App::make('App\Services\MyResponse');
        
        $this->jobRepo         = $jobRepo;
        
        //check permission
        $this->userAcl        = $this->canAccess($this->module);
    }

    private function createParamsCondition()
    {
        //create app repository
        $appRepo   = App::make('App\Repositories\AppRepository');
        //get appp id from user
        $app_id    = $appRepo->getAppIdFromUser();
        $keys      = [];
        $operators = [];
        $params    = [];
        if (!empty($app_id)) {
            $keys[]           = "app_id";
            $operators[]      = "=";
            $params["app_id"] = $app_id;
        }

        return [$keys, $operators, $params];
    }

    //------- Interface method --------//
    public function getList()
    {
        //check permission
        if (!$this->userAcl["read"]) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."list ".$this->moduleName);
        }

        //can use this method
        //create condition
        $conds = $this->createParamsCondition();
        //get job List
        $jobs  = $this->jobRepo->getJob($conds[0], $conds[1], $conds[2]);
        //return to view
        return $this->getOutput($this->mapping["list"], ["datas" => $jobs, "module_name" => $this->module]);
    }

    public function getCreate()
    {
        //check permission
        if (!$this->userAcl["create"]) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."create ".$this->moduleName);
        }

        //can use this method
        $datas              = [];
        //create app repository
        $appRepo            = App::make('App\Repositories\AppRepository');
        //add app
        $datas['apps']      = $appRepo->getAppAndUserDatas();
        //create job type repository
        $jobTypeRepo        = App::make('App\Repositories\JobTypeRepository');
        //add app
        $datas['job_types'] = $jobTypeRepo->getJobType([], [], []);
        //create document type repository
        $cocTypeRepo        = App::make('App\Repositories\DocumentTypeRepository');
        //add app
        $datas['documents'] = $cocTypeRepo->getDocumentType([], [], []);
        

        return $this->getOutput($this->mapping["create"], ["data" => $datas, "module_name" => $this->module]);
    }

    private function afterPostError($inputs, $message)
    {
        $result                = [];
        $result["error"]       = [];
        $result["error"][]     = $message;
        
        //create app repository
        $appRepo               = App::make('App\Repositories\AppRepository');
        //add app
        $inputs['apps']        = $appRepo->getAppAndUserDatas();
        //create job type repository
        $jobTypeRepo           = App::make('App\Repositories\JobTypeRepository');
        //add app
        $inputs['job_types']   = $jobTypeRepo->getJobType([], [], []);
        //create document type repository
        $cocTypeRepo           = App::make('App\Repositories\DocumentTypeRepository');
        //add app
        $inputs['documents']   = $cocTypeRepo->getDocumentType([], [], []);
        
        //Add old input
        $result["data"]        = $inputs;
        //add module name
        $result["module_name"] = $this->module;

        return $result;
    }

    public function postJob()
    {
        $inputs = Input::all();
        $mode = (empty($inputs["id"]))? "create" : "update";

        //check permission
        if (!$this->userAcl[$mode]) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$mode." ".$this->moduleName);
        }

        //can use this method

        if (empty($inputs["id"])) {
            //create
            $result = $this->jobRepo->addJob($inputs);
        } else {
            //update
            $result = $this->jobRepo->updateJob($inputs);
        }

        if (!$result['success']) {
            $result = $this->afterPostError($inputs, $result['message']);
        }
        //redirect page
        return $this->changeToPage(str_replace($this->derimetorView, $this->derimetorRedirect, $this->mapping["list"]), $this->mapping["create"], $result);
    }

    public function getView($id)
    {


    }

    public function getUpdate($id)
    {
        //check permission
        if (!$this->userAcl["update"]) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."update ".$this->moduleName);
        }

        //can use this method

        $inputs                  = Input::all();
        //update Category
        $job                     = $this->jobRepo->getJobById($id);
        
        //create app repository
        $appRepo                 = App::make('App\Repositories\AppRepository');
        //add app
        $job['apps']             = $appRepo->getAppAndUserDatas();
        //create job type repository
        $jobTypeRepo             = App::make('App\Repositories\JobTypeRepository');
        //add app
        $job['job_types']        = $jobTypeRepo->getJobType([], [], []);
        //create document type repository
        $cocTypeRepo             = App::make('App\Repositories\DocumentTypeRepository');
        //add app
        $job['documents']        = $cocTypeRepo->getDocumentType([], [], []);
        //TODO get document type id to documenttype_ids
        // $job['documenttype_ids'] = [];
        // dd($job);
        //return to view
        return $this->getOutput($this->mapping["update"], ["data" => $job, "module_name" => $this->module]);
    }

    public function deleteDelete()
    {
        //check permission
        if (!$this->userAcl["delete"]) {
            //return no permission
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission")."delete ".$this->moduleName);
        }

        //can use this method
        $inputs = Input::all();
        $id     = $inputs["id"];

        $result = $this->jobRepo->deleteJob($id);
        //return to view
        return $this->myResponse->getOutput('json', $result);
    }
    //------ End Interface method ------//

}