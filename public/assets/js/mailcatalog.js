var FormValidator = function () {
	"use strict";
	 
    // function to initiate Validation Sample 1
    var runValidator1 = function () {
        var form1 = $('#mailcatalog_form');
        var errorHandler1 = $('.errorHandler', form1);
        var successHandler1 = $('.successHandler', form1);

        function createDate(strDate){
            var list = strDate.split("-");
            return new Date(list[1]+"/"+list[0]+"/"+list[2]);
        }


        $('#mailcatalog_form').validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                    error.insertAfter($(element).closest('.form-group').children('div'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: "",
            rules: {
                name_en: {
                    required: true
                },
                name_th: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler1.hide();
                errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (form) {
                //successHandler1.show();
                errorHandler1.hide();

                // submit form
                form.submit();
            }
        });
    };
    
    return {
        //main function to initiate template pages
        init: function () {
        	runValidator1();
        }
    };
}();


function handlePDFUpdate() {
    var type = getFileType();
    if (type == "pdf") {
        var $tableRecord = $('#file-table-id tbody tr:visible');
        if ($tableRecord.length > 0){
            var $files = $('input:file[name="upload-file"]');
            for (var i=0;i<$files.length;i++){
                var file = $($files[i]);
                if (file.val())
                {
                    bootbox.alert(pdf_cannot_upload_msg);
                    return false;
                }
            }
        }
    }
    return true;
}

function deleteFile(id, asset_directory) {
    handleDeleteFile(id, asset_directory, function(){
        //reload page
        location.reload();
    });
    
}

function getFileType(){
    return $('input:radio[name=catalog_type]:checked').val();
}

function addNewAttach(){
var $devAttach = $("#attach_files");
    var html_string = '<div class="fileupload fileupload-new" data-provides="fileupload"><div class="input-group"><div class="form-control uneditable-input"><i class="fa fa-file fileupload-exists"></i><span class="fileupload-preview"></span></div><div class="input-group-btn"><div class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-folder-open-o"></i> Select file</span><span class="fileupload-exists"><i class="fa fa-folder-open-o"></i> Change</span><input type="file" class="file-input" name="upload-file[]" ></div><a href="#" class="btn btn-light-grey fileupload-exists" data-dismiss="fileupload"><i class="fa fa-times"></i> Remove </a></div></div></div>';
    
    $devAttach.append(html_string).fadeIn('slow');
}

function clearAttach(){
    var $devAttach = $("#attach_files");
    $devAttach.html("");

    //add new one
    addNewAttach();
}

function manageAddMore(catalog_type) {
    
    if (catalog_type == "pdf") {
        //disable add more
        $('#add-more-btn').attr('disabled','disabled');

        //claer dev attach
        clearAttach();

        
    } else {
        //enable add more
        $('#add-more-btn').removeAttr('disabled');

        //claer dev attach
        clearAttach();
    }
    
}

function handleAddMore(){
    $("#add-more-btn").click(function() {
        //add new one
        addNewAttach();
    });
}

// function handleCatalogType(){
//     $(".iCheck-helper").click(function() {

//         var $input = $(this).prev();

//         //show/hide table
//         $('.catalog-radio').hide();
//         $('.catalog-' + $input.val()).show();
        
//         manageAddMore($input.val());
//     });
// }


function getMailCatalogInfo() {
    //get status
    var status = $('input:radio[name=status]:checked').val();
    //add status to form
    $("#status").val(status);

    //get catalog type
    var catalog_type = $('input:radio[name=catalog_type]:checked').val();
    //add catalog type to form
    $("#catalog_type").val(catalog_type);

    //get product mail catalog type
    var mail_catalog_type = $('input:radio[name=mail_cat_type]:checked').val();
    //add catalog type to form
    $("#product_mail_catalog_type").val(mail_catalog_type);
    
    
    
}

function handelMenu(){
    $('#mail-and-catalog').addClass("active open");
    $('#mail-and-catalog-list').addClass("active");
}

function handleSave(){
    //save button
    $('#save-button').click(function() {
        $("#mailcatalog_form").submit();


    });
}

function getIdFromURL(url) {
    var temp = url.split("/");
    var id = "";
    if (temp.length > 1)
    {
        var tmpId = temp[(temp.length-1)].replace("#", "");
        id = parseInt(tmpId)||"";
    }
    return id;
}

function handleTable() {
    //for show file information
    var id = getIdFromURL(document.URL);
    if (id != "") {
        //Show Info 
        $('#file-management').show();

    }
}


