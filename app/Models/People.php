<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserAcl;


class People extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'people';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'card_no', 'change_name', 'birthday', 'tel', 'email', 'username', 'password'];

    // /**
    //  * Relation between user and user_acl.
    //  *
    //  * @return Collection.
    //  */
    // public function module()
    // {
    //     return $this->hasOne('App\Models\Module');
    // }
}
