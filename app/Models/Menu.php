<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserAcl;


class Menu extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'menu_display_text', 'parent_id', 'url', 'order'];

    // /**
    //  * Relation between user and user_acl.
    //  *
    //  * @return Collection.
    //  */
    public function module()
    {
        return $this->hasOne('App\Models\Module');
    }
}
