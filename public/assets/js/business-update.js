var FormValidatorUpdateBusiness = function () {
	"use strict";
	 
    // function to initiate Validation Sample 1
    var runValidator1 = function () {
        var form1 = $('#mailcatalog_form');
        var errorHandler1 = $('.errorHandler', form1);
        var successHandler1 = $('.successHandler', form1);

        $.validator.addMethod("uploadFile", function () {
            //if all values are selected
            var files = $('input:file[name="upload-file[]"]');
            for (var i=0;i<files.length;i++){
                var $file = $(files[i]);
                if ($file.val())
                {
                    return true;
                }

            }
            //case edit
            var dataTables = $('#file-table-id tbody tr:visible');
            if (dataTables.length > 0) {
                return true;  
            };
            
            return false;
        }, '<br /><br />  You must upload file');
        $.validator.addMethod("iconPic", function () {
            //if all values are selected
            var currentFile = $("#img-icon").val();
            var latestFile  = $("#catalog_icon").val();

            if (currentFile || latestFile) {
                return true;
            } else {
                return false;
            }

        }, 'Please select Icon first.');

        $('#business_form').validate({
            errorElement: "span", // contain the error msg in a span tag
            errorClass: 'help-block',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.attr("name") == "dd" || element.attr("name") == "mm" || element.attr("name") == "yyyy") {
                    error.insertAfter($(element).closest('.form-group').children('div'));
                }  else if (element.hasClass("ckeditor") || element.attr("type") == "file" ) {
                    error.appendTo($(element).closest('.form-group'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            ignore: "",
            rules: {
                name_en: {
                    required: true
                },
                name_th: {
                    required: true
                },
                description: {
                    required: true
                },
                active_date: {
                    required: true
                },
                status: {
                    required: true
                },
                catalog_type: {
                    required: true
                },
                icon: "iconPic",
                'upload-file[]': "uploadFile"
                
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                successHandler1.hide();
                errorHandler1.show();
            },
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                // display OK icon
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                // add the Bootstrap error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error');
                // set error class to the control group
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            submitHandler: function (form) {
                //successHandler1.show();
  
                var chk_error = true;
                var icon_img = $("#icon_img").val();

                console.log(icon_img);

                if(icon_img){

                    var ext       = icon_img.split('.').pop().toLowerCase();

                    if($.inArray(ext, ['jpg','jpeg','png']) == -1) {
                        successHandler1.hide();
                        errorHandler1.show();
                        $('#icon_img').closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                        $('.errorHandler').append('<br />Thumb can not support is type file.');
                        chk_error = false;

                    }
                }

                if(chk_error) {
                    successHandler1.show();
                    errorHandler1.hide();
                    form.submit();
                    return true;
                }


                // submit form

            }
        });
    };
    
    return {
        //main function to initiate template pages
        init: function () {
        	runValidator1();
        }
    };
}();


function getBusinessInfo() {
    //get status
    var status = $('input:radio[name=status]:checked').val();
    //add status to form
    $("#status").val(status);

    //get catalog type
    var catalog_type = $('input:radio[name=catalog_type]:checked').val();
    //add catalog type to form
    $("#catalog_type").val(catalog_type);
    
    
}

function handleSaveBusiness(){
    //save button
    $('#save-button').click(function() {
        if (handlePDFUpdate()){
            //get all input
            getBusinessInfo();
            $("#business_form").submit();
        }
        
    });
}

function handelMenuBusiness(){
    $('#business').addClass("active open");
    $('#business-list').addClass("active");
}



