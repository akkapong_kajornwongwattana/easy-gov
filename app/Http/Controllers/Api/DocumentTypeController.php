<?php namespace App\Http\Controllers\Api;

use App\Repositories\DocumentTypeRepository;
use App\Http\Controllers\ApiController;
use App;
use Input;

class DocumentTypeController extends ApiController {

    protected $documentTypeRepo;
    protected $myValidate;
    protected $myResponse;


    function __construct(DocumentTypeRepository $documentTypeRepo)
    {
        parent::__construct();

        $this->documentTypeRepo = $documentTypeRepo;
        $this->myValidate = App::make('App\Services\MyValidate');
        $this->myResponse = App::make('App\Services\MyResponse');
    }


    //Method for get document type
    public function getDocumenttype()
    {
        $input = Input::all();

        //--- Validate Section ---//
        /* Rules Validation*/

        $rules = [
        ];

        /* Input Default */
        $setInputDefault = [
        ];

        $params = $this->myValidate->validator($input, $rules, $setInputDefault);
        if (isset($params['error'])) {
          return $this->myValidate->validateError($params['error']);
        }

        // for create condition for query data
        // $conds = $this->createCondition($input);
        //--- Process Section ---//
        $result = $this->documentTypeRepo->getDocumentType([], [], []);
        //--- Output Section ---//
        return $this->myResponse->output($result);
    }

   
}