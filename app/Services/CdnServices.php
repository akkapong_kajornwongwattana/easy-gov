<?php namespace App\Services;
/**
 * @codeCoverageIgnore
 */

use App\Services\CurlServices;
use App\Services\Keygen;
use Config;
use CurlFile;
class CdnServices extends CurlServices
{
    protected $keyGen;
    private $_privateKey;
    private $_serviceName;
    private $_serviceId;
    private $_key;
    private $uploadPath;

    public function __construct(Keygen $keyGen)
    {
        parent::__construct();

        $this->setUrl(Config::get('api.cdn_api.url'));

        $this->keyGen = $keyGen;

        $this->_privateKey  = Config::get('api.cdn_api.private_key');
        $this->_serviceName = Config::get('api.cdn_api.service_name');
        $this->_serviceId   = Config::get('api.cdn_api.service_id');

        $this->uploadPath   = public_path().'/uploads/';
    }

    public function getUploadPath()
    {
        return config('config.base_url').'uploads/';
    }
    
    private function creatFileParam($fileName)
    {
        $filename   = $_FILES[$fileName]['name'];
        $filedata   = $_FILES[$fileName]['tmp_name'];
        $filesize   = $_FILES[$fileName]['size'];
        $filetype   = $_FILES[$fileName]['type'];

        $uploadfile = new CurlFile($filedata, $filetype, $filename);

        $inputfile = [
            'file' => $uploadfile
        ];

        return [$inputfile, $filesize];
    }

    // public function upload($fileName, $formatArray=true)
    // {
    //     $fileInfo = $this->creatFileParam($fileName);
    //     $response = $this->callUploadFile('postFile', $this->createUriForUpload('apis/upload'), $fileInfo[0], $fileInfo[1], $formatArray);

    //     return $response;

    // }

    private function getFileName($fileName)
    {
        $date = date("Y-m-d");

        return $date."_".basename($_FILES[$fileName]["name"]);
    }

    public function upload($fileName, $floder)
    {
        $uploadResult = [
                "status" => [
                    "code" => 290
                ],
                "data" => [
                    "short_url" => ""
                ]
            ];
        $target_dir    = $this->uploadPath;
        
        if (!empty($floder)) {
            $target_dir .= $floder."/";

            if (!file_exists($target_dir)) {
                //create folder
                mkdir($target_dir, 0755);
            }
        }
        
        $tmpFileName   = $this->getFileName($fileName);
        $target_file   = $target_dir . $tmpFileName;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if (move_uploaded_file($_FILES[$fileName]["tmp_name"], $target_file)) {
            $uploadResult["status"]["code"]    = 200;
            $uploadResult["data"]["short_url"] = $tmpFileName; 
        } else {
            $uploadResult["status"]["code"] = 290;
        }
        return $uploadResult;
    }

    private function _uploadFileData($name, $floder)
    {
        $short_url = "";
        $uploadResult = $this->upload($name, $floder);
        if ($uploadResult["status"]["code"] == 200) {
            $short_url = $uploadResult["data"]["short_url"];
        }
        
        return $short_url;
    }

    public function manageUpload($param, $keyNew, $keyOld, $floder="")
    {
        //define output
        $output = "";
        if (isset($param[$keyNew])) {
            $output = $this->_uploadFileData($keyNew, $floder);
        } else if (!empty($param[$keyOld])) {
            $output = $param[$keyOld];
        }
        return $output;
    }
}