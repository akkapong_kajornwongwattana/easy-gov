<?php namespace App\Http\Controllers\Html\CRUD\SuperAdmin;

use App\Http\Controllers\Html\CRUD\SuperAdminHtmlController;
use App\Repositories\ModuleRepository;
use Config;
use App;
use Input;

class ModuleController extends SuperAdminHtmlController {

    private $userAcl;
    private $myValidate;
    private $moduleRepo;
    private $menuRepo;
    private $myResponse;
    private $canAccessMethod;

    private $module      = "ModuleController";
    private $moduleName  = "module";
    
    private $mapping = [
        "list"   => "module.list",
        "create" => "module.form",
        "update" => "module.form",
        "view"   => "module.form"
    ];
    private $derimetorView = ".";
    private $derimetorRedirect = "/";

    public function __construct(ModuleRepository $moduleRepo)
    {
        parent::__construct();

        $this->myValidate = App::make('App\Services\MyValidate');
        $this->myResponse = App::make('App\Services\MyResponse');
        $this->menuRepo   = App::make('App\Repositories\MenuRepository');

        //check permission
        $this->canAccessMethod    = $this->canAccess();
        
        $this->moduleRepo = $moduleRepo;
    }

    //------- Interface method --------//
    public function getList()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        //can use this method
        //get Module List
        $modules = $this->moduleRepo->getModule([], [], []);
        //return to view
        return $this->getOutput($this->mapping["list"], ["modules" => $modules, "module_name" => $this->module]);

    }

    public function getCreate()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        //can use this method
        //get menu list
        $menuList = $this->menuRepo->getMenuList();
        return $this->getOutput($this->mapping["create"], ["data" => ["menuList" => $menuList], "module_name" => $this->module]);

    }

    public function postModule()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $inputs = Input::all();

        if (!$inputs["id"]) {
            //create
            $result = $this->moduleRepo->addModule($inputs);
        } else {
            //update
            $result = $this->moduleRepo->updateModule($inputs);
        }

        if (!$result['success']) {
            $result["error"]       = array();
            $result["error"][]     = $result['message'];
            //Add old input
            $result["data"]        = $inputs;
            //add module name
            $result["module_name"] = $this->module;
        }
        //redirect page
        return $this->changeToPage(str_replace($this->derimetorView, $this->derimetorRedirect, $this->mapping["list"]), $this->mapping["create"], $result);
    }

    public function getView($id)
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        //can use this method
        //get Module
        $modules = $this->moduleRepo->getModule(["id"], ["="], ["id" => $id]);
        if (!empty($modules)) {
            $modules = $modules[0];
        }
        //get menu list
        $menuList            = $this->menuRepo->getMenuList();
        //add menuList
        $modules["menuList"] = $menuList;
        //return to view
        return $this->getOutput($this->mapping["view"], ["data" => $modules, "module_name" => $this->module]);
    }

    public function getUpdate($id)
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }
        //can use this method
        //update Module
        $modules = $this->moduleRepo->getModule(["id"], ["="], ["id" => $id]);
        if (!empty($modules)) {
            $modules = $modules[0];
        }
        //get menu list
        $menuList            = $this->menuRepo->getMenuList();
        //add menuList
        $modules["menuList"] = $menuList;
        //return to view
        return $this->getOutput($this->mapping["update"], ["data" => $modules, "module_name" => $this->module]);
    }

    public function deleteDelete()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $inputs = Input::all();
        $id     = $inputs["id"];
        
        $result = $this->moduleRepo->deleteModule($id);
        //return to view
        return $this->myResponse->getOutput('json', $result);

    }
    //------ End Interface method ------//

}