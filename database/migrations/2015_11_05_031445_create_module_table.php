<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('module', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('module_name', 255);
			$table->text('description');
			$table->tinyInteger('has_create');
			$table->tinyInteger('has_read');
			$table->tinyInteger('has_update');
			$table->tinyInteger('has_delete');
			$table->integer('menu_id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('module');
	}

}
