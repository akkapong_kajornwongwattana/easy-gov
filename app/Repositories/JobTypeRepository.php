<?php namespace App\Repositories;
/**
 * @codeCoverageIgnore
 */

use App\Models\JobType;
use App;
use DB;
use Config;

class JobTypeRepository {

    protected $jobType;
    protected $myService;
    protected $messages;
    

    public function __construct(JobType $jobType)
    {
        $this->jobType   = $jobType;
        
        $this->myService = App::make('App\Services\MyServices');
        
        //get message
        $this->messages  = Config::get('message');
    }

    //For get job type from condition 
    public function getJobType($keys, $operators, $params)
    {
        //create param for add condition
        $conditions        = $this->myService->createCondition($keys, $operators, $params);
        //add condition
        $jobTypeCollection = $this->myService->addConditiontoCollection($conditions, $this->jobType);
        $output            = $jobTypeCollection->get()->toArray();
        
        return $output;
    }

    //check duplicate module
    private function duplicateJobType($param)
    {
        //define parameters
        $keys               = ["name"];
        $operators          = ["="];
        
        $isDuplicate        = $this->myService->checkDuplicate($this->jobType, $keys, $operators, $param);
        
        return $isDuplicate;
    }

    public function addJobType($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        //check duplicate
        $isDups = $this->duplicateJobType($param);

        if (!$isDups[0]) {

            // add data to database
            $jobType               = new JobType;
            // add data to collection object
            $jobType->name         = $param["name"];
            
            if (!$jobType->save()) {
                $result["success"] = false;
                $result["message"] = $this->messages["add_error"];
            }

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["duplicate"];
        }
        return $result;
    }

    private function checkDupForUpdate($param)
    {
        $isDuplicate = false;
        //check duplicate
        $duplicates = $this->duplicateJobType($param);
        if ($duplicates[0]) {
            $tmp = $duplicates[1];
            //check self
            if ((int)$tmp["id"] != (int)$param["id"]) {
                $isDuplicate = true;
            }
        }
        return $isDuplicate;
    }

    public function updateJobType($param)
    {
        $result = [
            "success" => true,
            "message" => ""
            ];
        if (!empty($param["id"])) {
            //get record
            $jobType     = $this->jobType->find($param['id']);
            
            //check duplicate
            $isDuplicate = $this->checkDupForUpdate($param);

            if (!$isDuplicate){
                
                $jobType->name  = $param["name"];

                if (!$jobType->save()) {
                    $result["success"] = false;
                    $result["message"] = $this->messages["update_error"];
                }
                
            } else {
                $result["success"] = false;
                $result["message"] = $this->messages["duplicate"];
            }
            

        } else {
            $result["success"] = false;
            $result["message"] = $this->messages["update_error"];
        }
        return $result;
    }

    public function deleteJobType($id)
    {
        $output = [
            "status" => 200,
            "message" => ""
        ];

        $jobType = $this->jobType->find($id);

        if (!$jobType->delete()) {
                
            $output["status"]  = 290;
            $output["message"] = $this->messages["delete_error"];

        }

        return $output;
    }

}