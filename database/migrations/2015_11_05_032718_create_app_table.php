<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('app', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->string('company_name', 255);
			$table->string('company_type', 255);
			$table->date('project_start_date')->default(date("Y-m-d"));
			$table->string('contact_name', 255);
			$table->string('contact_email', 255);
			$table->string('contact_phone', 255);
			$table->integer('status');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('app');
	}

}
