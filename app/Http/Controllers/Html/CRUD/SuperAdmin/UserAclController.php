<?php namespace App\Http\Controllers\Html\CRUD\SuperAdmin;

use App\Http\Controllers\Html\CRUD\SuperAdminHtmlController;
use App\Repositories\UserAclRepository;
use App\Repositories\ModuleRepository;
use Config;
use App;
use Input;

class UserAclController extends SuperAdminHtmlController{

    protected $userAclRepo;
    protected $moduleRepo;

    //private $userAcl;
    private $myValidate;
    private $myResponse;
    private $myServices;
    private $canAccessMethod;

    private $module      = "UserAclController";
    private $moduleName  = "permission";
    private $mapping = [
        "list"   => "permission.list",
        "create" => "permission.form",
        "update" => "permission.form",
        "view"   => "permission.form"
    ];

    private $derimetorView     = ".";
    private $derimetorRedirect = "/";

    public function __construct(UserAclRepository $userAclRepo, ModuleRepository $moduleRepo)
    {
        parent::__construct();

        $this->myValidate = App::make('App\Services\MyValidate');
        $this->myResponse = App::make('App\Services\MyResponse');
        $this->myServices = App::make('App\Services\MyServices');
        
        //check permission
        $this->canAccessMethod    = $this->canAccess();
        
        $this->userAclRepo = $userAclRepo;
        $this->moduleRepo  = $moduleRepo;
    }

    //------- Interface method --------//
    public function getList()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $inputs   = Input::all();
        //get user id
        $user_id = $inputs["user_id"];
        if (!empty($user_id)) {
            //get user acl list
            $userAcls = $this->userAclRepo->getUserAcl(["user_id"], ["="], $inputs);
            //return to view
            return $this->getOutput($this->mapping["list"], ["userAcls" => $userAcls, "module_name" => $this->module, "user_id" => $user_id]);
        } else {
            //TODO
            //return error page => This method need user id
            return $this->getOutputWithErrormsg("error", ["module_name" => $this->module], Config::get("message.permission.require_user_id"));
        }
    }

    public function getCreate()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $inputs = Input::all();
        //get user id
        $user_id = $inputs["user_id"];
        if (!empty($user_id)) {
            //get module
            $modules = $this->userAclRepo->getModuleForUserAcl("", $user_id);
            //can use this method
            return $this->getOutput($this->mapping["create"], ["data" => ["modules" => $modules, "user_id" => $user_id], "module_name" => $this->module]);
        } else {
            //return error page => This method need user id
            return $this->getOutputWithErrormsg("error", ["module_name" => $this->module], Config::get("message.permission.require_user_id"));
        }
    }

    public function postPermission()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $inputs = Input::all();

        if (empty($inputs["id"])) {
            //create
            $result = $this->userAclRepo->addUserAcl($inputs);
        } else {
            //update
            $result = $this->userAclRepo->updateUserAcl($inputs);
        }

        if (!$result['success']) {
            //NOT SUCCESS
            $result["error"]       = array();
            $result["error"][]     = $result['message'];
            //get modules
            $modules               = $this->userAclRepo->getModuleForUserAcl($inputs["id"], $inputs["user_id"]);
            //add modules to inputs
            $inputs["modules"]     = $modules;
            //Add old input
            $result["data"]        = $inputs;
            //add module name
            $result["module_name"] = $this->module;
        } 
        //redirect page
        return $this->changeToPage(str_replace($this->derimetorView, $this->derimetorRedirect, $this->mapping["list"])."?user_id=".$inputs["user_id"], $this->mapping["create"], $result);
    }

    public function getView($id)
    {
       
        
    }
    
    public function getUpdate($id)
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $inputs  = Input::all();
        //get user id
        $user_id = $inputs["user_id"];
        //update Module
        $users   = $this->userAclRepo->getUserAcl(["id"], ["="], ["id" => $id]);
        if (!empty($users)) {
            $users = $users[0];
        }
        //get modules
        $modules          = $this->userAclRepo->getModuleForUserAcl($id, $user_id);
        //add modules to users
        $users["modules"] = $modules;
        //add user id to users
        $users["user_id"] = $user_id;

        //return to view
        return $this->getOutput($this->mapping["update"], ["data" => $users, "module_name" => $this->module]);
    }

    public function deleteDelete()
    {
        //check permission
        if (!$this->canAccessMethod) {
            //return 
            return $this->getOutputWithErrormsg("nopermission", ["module_name" => $this->module], Config::get("message.no_permission").$this->moduleName);
        }

        $inputs = Input::all();
        $id     = $inputs["id"];
        
        $result = $this->userAclRepo->deleteUserAcl($id);
        //return to view
        return $this->myResponse->getOutput('json', $result);
    }
    //------ End Interface method ------//

}
